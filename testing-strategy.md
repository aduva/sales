# Testing strategy:

1. Models:
.* Check for attributes validation
.* Check for correctness of additional methods if present

2. Controllers CRUDs
.* Check for index method returning list with default page and pageSize parameters
.* Check for index method returning list with given page and pageSize parameters
.* Check for index method returning list with given q parameter
.* Check for index method returning list with given q, page and pageSize parameters
.* Check for save method
.* Check for update method
.* Check for delete method

3. Views
.* Check for correct view rendering by some parameters
