package app.config;

import app.services.PubsubServiceModule;
import com.google.inject.*;
import org.javalite.activeweb.AppContext;
import org.javalite.activeweb.Bootstrap;
import app.services.PubsubService;


/**
 * @author Askhat Shakenov
 */
public class AppBootstrap extends Bootstrap {
    public void init(AppContext context) {
    	context.set("env", "dev");

    	Injector injector = Guice.createInjector(new PubsubServiceModule());
		setInjector(injector);

		PubsubService pub = injector.getInstance(PubsubService.class);
		pub.publisher().register(new app.controllers.WarehouseMovementHandler());
    }
}
