package app.config;

import org.javalite.activeweb.AbstractRouteConfig;
import org.javalite.activeweb.AppContext;
import app.controllers.*;

/**
 * @author Askhat Shakenov
 */

public class RouteConfig extends AbstractRouteConfig {
    public void init(AppContext appContext) {
    	route("/{controller}/{id}/{action}");
    }
}