package app.utils;

import freemarker.template.SimpleScalar;

import org.javalite.activeweb.freemarker.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Map;

import static org.javalite.common.Util.split;


public class MsgTag extends FreeMarkerTag {

    @Override
    protected void render(Map params, String body, Writer writer) throws Exception {
        if (params.containsKey("key")) {
            String key = params.get("key").toString();
            String localeString = "ru_RU";
            
            if(params.containsKey("locale")) {
                localeString = params.get("locale").toString();
            }

            String language, country;
            Locale locale;
            if(localeString.contains("_")) {
                language = split(localeString, '_')[0];
                country = split(localeString, '_')[1];
                locale = new Locale(language, country);
            } else {
                language = localeString;
                locale = new Locale(language);
            }

            String msg = Msg.message(key, locale, getParamsArray(params));
            
            if(params.containsKey("menushort"))
                msg = msg.substring(0,3).toLowerCase();
            writer.write(msg);

        } else {
             writer.write("<span style=\"display:none\">you failed to supply key for this message tag</span>");
        }
    }


    private String[] getParamsArray(Map params) {

        int index = 0;
        List<String> paramList = new ArrayList<String>();
        for (String paramName = "param"; params.containsKey(paramName + index); index++){
            String param = ((SimpleScalar) params.get(paramName + index)).getAsString();
            paramList.add(param);
        }
        return paramList.toArray(new String[]{});
    }
}
