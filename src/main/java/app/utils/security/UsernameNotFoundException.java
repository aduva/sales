package app.utils;

public class UsernameNotFoundException extends LoginException {

	public UsernameNotFoundException() {
    }

    public UsernameNotFoundException(String message) {
        super(message);
    }

    public UsernameNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }

}