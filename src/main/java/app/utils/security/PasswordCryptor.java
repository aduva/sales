package app.utils;

public interface PasswordCryptor {
	public boolean match(String plainPassword, String encryptedPassword);
	public String encrypt(String plainPassword);
}