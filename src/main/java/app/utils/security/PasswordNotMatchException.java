package app.utils;

public class PasswordNotMatchException extends LoginException {

	public PasswordNotMatchException() {
    }

    public PasswordNotMatchException(String message) {
        super(message);
    }

    public PasswordNotMatchException(String message, Throwable throwable) {
        super(message, throwable);
    }

}