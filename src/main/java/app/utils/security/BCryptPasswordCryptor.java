package app.utils;
import org.mindrot.jbcrypt.BCrypt;

public class BCryptPasswordCryptor implements PasswordCryptor {
	public boolean match(String plainPassword, String encryptedPassword) {
		return BCrypt.checkpw(plainPassword, encryptedPassword);
	}

	public String encrypt(String plainPassword) {
		return BCrypt.hashpw(plainPassword, BCrypt.gensalt());
	}
}