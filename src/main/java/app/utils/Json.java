package app.utils;

import org.codehaus.jackson.map.ObjectMapper;
import java.io.IOException; 
import java.util.Map;
import java.util.List;

public class Json {
	public static Map toMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
	
		try {
			return mapper.readValue(json, Map.class);
		} catch (IOException e) { throw new RuntimeException(e); }
	}
	
	public static Map[] toMaps(String json) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, Map[].class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static String fromMap(Map map) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(map);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static String fromList(List<Map> list) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(list);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}