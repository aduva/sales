package app.utils;

import app.models.*;
import app.services.*;
import com.google.inject.Inject;
import freemarker.template.SimpleScalar;
import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import org.javalite.activeweb.freemarker.*;
import static org.javalite.common.Util.split;


public class IsAuthenticatedTag extends Tag {

    @Override
    protected void render(Map params, String body, Writer writer) throws Exception {

        Subject subject = subject();

        if(subject == null || !subject.isAuthenticated()) {
            writer.write("");
            return;
        } else {
            writer.write(body);
        }
    }
}
