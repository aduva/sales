package app.utils;

import app.models.*;
import app.services.*;
import com.google.inject.Inject;
import freemarker.template.SimpleScalar;
import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import org.javalite.activeweb.freemarker.*;
import static org.javalite.common.Util.split;


public class UsernameTag extends Tag {

    @Override
    protected void render(Map params, String body, Writer writer) throws Exception {

        Subject subject = subject();

        if(subject == null || !subject.isAuthenticated()) {
            writer.write("");
            return;
        }

        if (params.containsKey("key")) {
            String key = params.get("key").toString();
            Profile p = subject.profile();
            
            if(p == null) {
                writer.write(subject.username());
                return;
            }
            
            String firstName = p.getString("first_name");
            String lastName = p.getString("last_name");

            if(key.equalsIgnoreCase("fullname")) {
                writer.write(firstName + " " + lastName);
            } else if(key.equalsIgnoreCase("lastName")) {
                writer.write(lastName);
            } else if(key.equalsIgnoreCase("firstName")) {
                writer.write(firstName);
            }

        } else {
            writer.write(subject.username());
        }
    }
}
