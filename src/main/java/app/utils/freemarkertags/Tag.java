package app.utils;

import app.models.*;
import app.services.*;
import org.javalite.activeweb.freemarker.FreeMarkerTag;
import org.javalite.activeweb.SessionFacade;

public abstract class Tag extends FreeMarkerTag {

    public static final String SESSION_KEY_SUBJECT = "subject";

    public Group group() {
        return subject().group();
    }

    public Long groupId() {
        Group g = group();
        return g != null ? g.getLongId() : null;
    }

    public Long userId() {
        return subject().userId();
    }

     // public Subject subject() {
    //     String username = null;
    //     try {
    //         username = cookieValue("last_login");
    //     } catch(NullPointerException e) {
    //         return null;
    //     }
    //     return username == null ? null : subject(username);
    // }

    public Subject subject(String username) {
        return (Subject) session(username.concat(":").concat(app.controllers.Controller.SESSION_KEY_SUBJECT));
    }

    public Subject subject() {
        return (Subject) sessionValue(app.controllers.Controller.SESSION_KEY_SUBJECT);
    }

    private SessionFacade sessionValue() {
        return new SessionFacade();
    }

    private Object sessionValue(String name){
        Object val = session().get(name);
        return val == null ? null : val;
    }
}
