package app.utils;

import freemarker.template.SimpleScalar;

import org.javalite.activeweb.freemarker.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Map;

import static org.javalite.common.Util.split;


public class Msg {

    private static final String BUNDLE = "activeweb_messages";

    public static String message(String key, Locale locale, Object... params) {
        return getMessage(key, locale, params);
    }

    public static String message(String key, Object... params) {
        return getMessage(key, null, params);
    }

    private static String getMessage(String key, Locale locale, Object... params){
        MessageFormat mf = new MessageFormat("");
        try{
            if(locale == null){
                mf.applyPattern(ResourceBundle.getBundle(BUNDLE, new UTF8Control()).getString(key));
            }else{
                mf.applyPattern(ResourceBundle.getBundle(BUNDLE, locale, new UTF8Control()).getString(key));
            }
        }catch(Exception e){
            mf.applyPattern(key);
        }
        return mf.format(params);
    }
}