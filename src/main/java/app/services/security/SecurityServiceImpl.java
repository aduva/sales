package app.services;

import org.javalite.activeweb.SessionFacade;
import app.models.User;
import app.models.Group;
import app.utils.*;
import java.io.Serializable;
import java.util.*;
import org.javalite.activeweb.Cookie;
import org.javalite.activeweb.RequestUtils;

public class SecurityServiceImpl implements SecurityService {    

	private static final String SESSION_KEY_SUBJECT = "subject";
	private static final String SESSION_KEY_GROUP = "user-current-group";
	private static final String SESSION_KEY_GROUPS = "user-groups";
	// private static String SESSION_KEY_LOGIN_TIME = "user-logged-in";

    public Subject subject() {
        String cv = RequestUtils.cookieValue("username");
        return (Subject) session(cv + ":" + SESSION_KEY_SUBJECT);
    }

    public Long userId() {
        Subject s = subject();
        return s != null ? s.userId() : null;
    }

    public void login(String username, String password, boolean rememberMe) throws UsernameNotFoundException, PasswordNotMatchException {
		User user = (User) User.findFirst("username = ?", username);
		if(user == null)
			throw new UsernameNotFoundException();

		BCryptPasswordCryptor matcher = new BCryptPasswordCryptor();
		if(!matcher.match(password, user.getString("password")))
			throw new PasswordNotMatchException();
        
        Subject subject = subject();
        Group g = null;
        
        if(subject == null) {
            List<Group> groups = (List<Group>) user.getAll(Group.class);
            g = groups.get(0);
            subject = new Subject(user, true, rememberMe);
            subject.setCurrentGroup(g);
            session(RequestUtils.cookieValue("username") + ":" + SESSION_KEY_SUBJECT, subject);
        } else {
            subject.isAuthenticated(true);
        }
    }

    public void logout() {
    	subject().isAuthenticated(false);
    	if(!subject().rememberMe())
    		session().remove(RequestUtils.cookieValue("username") + ":" + SESSION_KEY_SUBJECT);
    }

    private SessionFacade session(){
        return new SessionFacade();
    }
    
    private void session(String name, Serializable value){
        session().put(name, value);
    }

    private Object session(String name){
        Object val = session().get(name);
        return val == null ? null : val;
    }

}