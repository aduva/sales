package app.services;

import com.google.inject.AbstractModule;

public class SecurityServiceModule extends AbstractModule {
    protected void configure() {
        bind(SecurityService.class).to(SecurityServiceImpl.class).asEagerSingleton();
    }
}