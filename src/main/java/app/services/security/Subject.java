package app.services;

import app.models.User;
import app.models.Profile;
import app.models.Role;
import app.models.Group;
import app.models.GroupsUsers;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.javalite.activejdbc.DB;

public class Subject implements Serializable {
	private User user;
	private boolean isAuthenticated;
	private boolean rememberMe;
	private Group group;

	public Subject(User user, boolean isAuthenticated, boolean rememberMe) {
		this.user = user;
		this.isAuthenticated = isAuthenticated;
		this.rememberMe = rememberMe;
	}

	public String username() {
		return this.user.getString("username");
	}

	public Long userId() {
		return this.user.getLongId();
	}

	public boolean hasGroup(String... rolename) {
		return false;
	}

	public boolean hasRole(String rolename) {
		DB db = new DB();
		
		try {
			db.open();
		  // your code here
			Role r = Role.first("rolename = ?", rolename);
			if(r == null)
				return false;

		// GroupsUsers gu = GroupsUsers.first(
		// 	"group_id = ? and user_id = ? and role_id = ?", 
		// 	this.group.getLongId(), this.user.getLongId(), r.getLongId()
		// );

		// if(gu == null)
		// 	return false;
		} finally {
			db.close();
		}

		return true;
	}

	public boolean hasPermission(String... rolename) {
		return true;
	}

	public List<Map> groups() {
		return null;
	}

	public List<Map> roles() {
		return null;
	}

	public List<Map> permissions() {
		return null;
	}

	public boolean isAuthenticated() {
		return this.isAuthenticated;
	}

	public void isAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	public boolean rememberMe() {
		return this.rememberMe;
	}

	public void setCurrentGroup(Group group) {
		this.group = group;
	}

	public Group getGroup() {
		return group();
	}

	public Group group() {
		return this.group;
	}

	public Profile profile() {
		return (Profile) this.user.get("profile");
	}
}