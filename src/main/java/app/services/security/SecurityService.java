package app.services;
import app.utils.*;

public interface SecurityService {
    Subject subject();
    void login(String username, String password, boolean rememberMe) throws UsernameNotFoundException, PasswordNotMatchException;
    void logout();
    Long userId();
}