package app.services;

import org.javalite.activeweb.SessionFacade;
import app.models.User;
import app.models.Group;
import app.utils.*;
import java.io.Serializable;
import java.util.*;
import org.javalite.activeweb.Cookie;
import org.javalite.activeweb.RequestUtils;

public class PubsubServiceImpl implements PubsubService {    

	private Publisher publisher;

    public Publisher publisher() {
        if(publisher == null)
            this.publisher = new Publisher();
        return this.publisher;
    }

    public void publish(String topic, Object message) {
        publisher().publish(topic, message);
    }

}