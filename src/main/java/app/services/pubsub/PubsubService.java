package app.services;
import app.utils.*;
import java.util.Map;

public interface PubsubService {
    Publisher publisher();
    void publish(String topic, Object message);
}