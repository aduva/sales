package app.services;

import app.models.User;
import app.models.Group;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import static com.mycila.event.Topic.match;
import static com.mycila.event.Topic.only;
import static com.mycila.event.Topic.topic;

import com.mycila.event.*;

public class Publisher implements Serializable {
	private Dispatcher dispatcher;

	public Publisher() {
		this.dispatcher = Dispatchers.synchronousSafe(ErrorHandlers.rethrow());
	}

	public void publish(String topic, Object message) {
		this.dispatcher.publish(topic(topic), message);
	}

	public void register(Object o) {
		MycilaEvent.with(this.dispatcher).register(o);
	}
}