package app.services;

import com.google.inject.AbstractModule;

public class PubsubServiceModule extends AbstractModule {
    protected void configure() {
        bind(PubsubService.class).to(PubsubServiceImpl.class).asEagerSingleton();
    }
}