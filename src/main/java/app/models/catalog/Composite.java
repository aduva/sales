package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */

public class Composite extends Model {
    static {
        validatePresenceOf("product_id");
    }
}
