package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.*;
/**
 * @author Askhat Shakenov
 */

public class CategoriesProducts extends Model {
    static {
        validatePresenceOf("product_id", "category_id");
    }
}
