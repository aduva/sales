package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */
public class SetProducts extends Model {
    static {
        validatePresenceOf("composite_id", "product_id", "amount");
    }
}
