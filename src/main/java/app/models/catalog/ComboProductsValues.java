package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */
public class ComboProductsValues extends Model {
    static {
        validatePresenceOf("combo_product_id", "attribute_value_id");
    }
}
