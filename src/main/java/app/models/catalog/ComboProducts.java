package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */
public class ComboProducts extends Model {
    static {
        validatePresenceOf("product_id", "composite_id");
    }
}
