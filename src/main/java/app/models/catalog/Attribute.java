package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */
public class Attribute extends Model {
    static {
        validatePresenceOf("title", "group_id");
    }
}
