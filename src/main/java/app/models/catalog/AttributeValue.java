package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;

/**
 * @author Askhat Shakenov
 */
public class AttributeValue extends Model {
    static {
        validatePresenceOf("attribute_id", "title");
    }

    public String text;
}
