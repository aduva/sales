package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;

/**
 * @author Askhat Shakenov
 */
public class FeatureValue extends Model {
    static {
        validatePresenceOf("feature_id", "title");
    }

    public String text;
}
