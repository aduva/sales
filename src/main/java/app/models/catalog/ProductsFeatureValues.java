package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */
public class ProductsFeatureValues extends Model {
    static {
        validatePresenceOf("product_id", "feature_value_id");
    }
}
