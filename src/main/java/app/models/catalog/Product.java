package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
/**
 * @author Askhat Shakenov
 */
@BelongsTo(parent = Product.class, foreignKeyName = "parent_id")
public class Product extends Model {
    static {
        validatePresenceOf("title", "user_id", "group_id");
    }
}
