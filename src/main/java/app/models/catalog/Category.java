package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */

public class Category extends Model {
    static {
        validatePresenceOf("group_id", "title", "url");
    }
}
