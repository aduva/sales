package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */

public class Client extends Model {
	static {
        validatePresenceOf("group_id", "profile_id");
    }
}
