package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
/**
 * @author Askhat Shakenov
 */

public class OrdersProducts extends Model {
    static {
        validatePresenceOf("order_id", "product_id", "amount");
    }
}
