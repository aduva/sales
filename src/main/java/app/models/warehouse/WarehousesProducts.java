package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
/**
 * @author Askhat Shakenov
 */

public class WarehousesProducts extends Model {
    static {
        validatePresenceOf("warehouse_id", "product_id", "stock");
    }
}
