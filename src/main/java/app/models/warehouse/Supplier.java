package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
/**
 * @author Askhat Shakenov
 */

public class Supplier extends Model {
    static {
        validatePresenceOf("title", "group_id");
    }
}
