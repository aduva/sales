package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.*;
/**
 * @author Askhat Shakenov
 */

public class ProductsSuppliers extends Model {
    static {
        validatePresenceOf("product_id", "supplier_id");
    }
}
