package app.models;

import org.javalite.activejdbc.Model;
/**
 * @author Askhat Shakenov
 */

public class WarehouseMovement extends Model {
    static {
        validatePresenceOf("warehouse_id", "product_id", "amount", "group_id", "user_id", "manage_type_code");
    }

}
