package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */

public class Profile extends Model {
	static {
        validatePresenceOf("first_name");
    }
}
