package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */

public class Contact extends Model {
	static {
        validatePresenceOf("profile_id", "title", "value");
    }
}
