package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.*;

/**
 * @author Askhat Shakenov
 */
public class Role extends Model {
    static {
        validatePresenceOf("level", "title");
    }
}
