package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.*;

/**
 * @author Askhat Shakenov
 */
public class Group extends Model {
    static {
        validatePresenceOf("groupname", "title");
    }
}
