package app.models;

import org.javalite.activejdbc.Model;
/**
 * @author Askhat Shakenov
 */
public class RolesPermissions extends Model {
    static {
        validatePresenceOf("role_id", "permission", "is_granted");
    }
}
