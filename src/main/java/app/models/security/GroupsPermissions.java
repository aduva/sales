package app.models;

import org.javalite.activejdbc.Model;
/**
 * @author Askhat Shakenov
 */
public class GroupsPermissions extends Model {
    static {
        validatePresenceOf("group_id", "permission", "is_granted");
    }
}
