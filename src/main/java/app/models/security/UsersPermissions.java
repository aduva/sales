package app.models;

import org.javalite.activejdbc.Model;
/**
 * @author Askhat Shakenov
 */
public class UsersPermissions extends Model {
    static {
        validatePresenceOf("user_id", "permission", "is_granted");
    }
}
