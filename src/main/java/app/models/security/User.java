package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.*;
import app.utils.BCryptPasswordCryptor;

/**
 * @author Askhat Shakenov
 */
public class User extends Model {
    static {
        validatePresenceOf("username", "password");
    }

    public static User newUser(String username, String plainPassword) {
    	BCryptPasswordCryptor cryptor = new BCryptPasswordCryptor();
    	String encryptedPassword = cryptor.encrypt(plainPassword);

    	User u = new User();
    	u.set("username", username, "password", encryptedPassword);
    	return u;
    }

    public void beforeCreate() {
    	// if(this.getBoolean("password_"))

    }
}
