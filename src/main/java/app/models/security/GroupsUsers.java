package app.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.CompositePK;
/**
 * @author Askhat Shakenov
 */

@CompositePK({"user_id", "group_id", "role_id"})
public class GroupsUsers extends Model {
    static {
        validatePresenceOf("user_id", "group_id", "role_id");
    }
}
