package app.models;

import org.javalite.activejdbc.Model;

/**
 * @author Askhat Shakenov
 */
public class Tax extends Model {
    static {
        validatePresenceOf("title", "rate");
    }
}
