package app.controllers;

import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.*;
import org.javalite.activejdbc.*;
import app.models.*;

import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class LangsController extends Controller {
	public static Map<String, String> langs = new HashMap<String, String>(2);

    static {
        langs.put("ru_RU", "Русский");
        langs.put("kk_KK", "Қазақша");
    }

    public void setLang() {
    	if(!blank("lang")) {
            String lang = param("lang");
            if(langs.get(lang) != null)
                session("locale", lang);
        }

        redirect(HomeController.class);
    }
}
