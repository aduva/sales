package app.controllers;

import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.*;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.Json;

import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class TaxesController extends Controller {
	public int page = 1, pageSize = 10;
	public String q = "";

    public void index() {
    	try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Paginator p = new Paginator(Tax.class, pageSize, "group_id = ? and title ilike ?", group().getLongId(), "%"+q+"%").orderBy("id");
        if(xhr()) {

            Map<String, Object> result = new HashMap<String, Object>();
            result.put("items", p.getPage(page).toMaps());
            result.put("page", page);
            String json = app.utils.Json.fromMap(result);

            respond(json).contentType("application/json").status(200);

        } else {
            view("items", p.getPage(page));
            view("pageCount", p.pageCount());
            view("getCount", p.getCount());
            view("page", page);
            view("pageSize", pageSize);
            view("from", (page-1) * pageSize);

            int to = ((page-1) * pageSize) + pageSize;
            view("to", p.getCount() < to ? p.getCount() : to);
            view("next", p.hasNext() ? page+1 : page);
            view("prev", p.hasPrevious() ? page-1 : page);
        }
    }

    @POST
    public void save() {
        Tax item = new Tax();

        if(param("is_default").equals("on")) {
            Tax.updateAll("is_default = false and group_id = ?", groupId());
        }

        item.set(
            "title", param("title"), 
            "rate", Double.parseDouble(param("rate")),
            "group_id", groupId(), 
            "user_id", userId(),
            "is_default", param("is_default").equals("on")
        );

        if(!item.save()) {
            flash("message", "Something went wrong, please  fill out all fields");
            flash("errors", item.errors());
            flash("params", params1st());
            redirect(TaxesController.class, "add");
        } else {
            flash("message", "New tax was added: " + item.get("title"));
            redirect(TaxesController.class);
        }
    }

    public void add() {
        if(xhr())
            render().noLayout();
    }

    public void show() {
        Tax item = (Tax)Tax.findById(id());
        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(item == null) {
            response.put("message", "Tax not found with provided id: ".concat(getId()));
            status = 400;
        } else {
            response.put("item", item);
        }

        if(xhr()) {
            response.put("item", item.toMap());
            respond(Json.fromMap(response)).contentType("application/json").status(status);
            return;
        }
    }
}
