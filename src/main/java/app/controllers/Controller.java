package app.controllers;

import org.javalite.activeweb.AppController;
import app.services.Subject;
import app.models.Group;
/**
 * @author Askhat Shakenov
 */
public class Controller extends AppController {
    public static final String SESSION_KEY_SUBJECT = "subject";
    public static final int OK = 200;
    public static final int BAD = 400;

    public Long id() {
        return Long.valueOf(super.getId());
    }

    public Group group() {
    	return subject().group();
    }

    public Long groupId() {
    	Group g = group();
    	return g != null ? g.getLongId() : null;
    }

    public Long userId() {
        return subject().userId();
    }

    // public Subject subject() {
    //     String username = null;
    //     try {
    //         username = cookieValue("last_login");
    //     } catch(NullPointerException e) {
    //         return null;
    //     }
    //     return username == null ? null : subject(username);
    // }

    public Subject subject(String username) {
        return (Subject) session(username.concat(":").concat(SESSION_KEY_SUBJECT));
    }

    public Subject subject() {
        return (Subject) session(SESSION_KEY_SUBJECT);
    }
}
