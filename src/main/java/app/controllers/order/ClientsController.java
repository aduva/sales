package app.controllers;

import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.DELETE;
import org.javalite.activeweb.annotations.GET;
import org.javalite.activeweb.annotations.POST;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.Json;
import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class ClientsController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
    }

    public void list() {
        try {
            if(params1st().get("pageIndex") != null)
                page = Integer.parseInt(params1st().get("pageIndex"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Group g = group();

        StringBuffer sb = new StringBuffer();
        sb.append("select c.id, concat(p.first_name, ' ', p.last_name) clientname, ct.value phone from clients c ")
            .append("join profiles p on c.profile_id = p.id ")
            .append("join contacts ct on ct.profile_id = p.id ")
            .append("where (p.first_name ilike ? or ct.title = 'PHONE' and ct.value ilike ?) ")
            .append("and c.group_id = ? ")
            .append("order by c.id, clientname ")
            .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", "%"+q+"%", g.getLongId(), pageSize, (page - 1) * pageSize);

        StringBuffer countsb = new StringBuffer();
        countsb.append("p.id = c.profile_id and p.id = ct.profile_id and c.group_id = ? ");
        
        long count = Base.count("profiles p, clients c, contacts ct", countsb.toString(), g.getLongId());
        boolean hasNext = count > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("total", count);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @POST
    public void save() {
        Map<String, Object> response = new HashMap<String, Object>();
        int status = OK;

        if(blank("phoneCode") || blank("phone") || blank("first_name")) {
            status = BAD;
            response.put("message", "Пожалуйста, заполните все обязательные поля");
            view("response");
            if(xhr()) {
                respond(Json.fromMap(response)).contentType("application/json").status(status);
            }

            return;
        }

        Contact contact = Contact.first("title = 'PHONE' and value = ?", param("phoneCode").concat(param("phone")));
        if(contact != null) {
            status = BAD;
            response.put("message", "Клиент с таким номером уже существует");
            view(response);
            if(xhr()) {
                respond(Json.fromMap(response)).contentType("application/json").status(status);
            }

            return;   
        }

        Profile profile = Profile.createIt(
            "first_name", param("first_name"),
            "last_name", param("last_name")
        );

        contact.createIt(
            "title", "PHONE",
            "value", param("phoneCode").concat(param("phone")),
            "profile_id", profile.getLongId()
        );

        Client client = Client.createIt(
            "profile_id", profile.getLongId(),
            "group_id", groupId()
        );

        response.put("message", "Клиент " + param("first_name") + " " + param("phoneCode").concat(param("phone")) + " успешно добавлен в базу клиентов.");

        Map<String, Object> clientMap = new HashMap<String, Object>(5);
        clientMap.put("first_name", param("first_name"));
        clientMap.put("last_name", param("last_name"));
        clientMap.put("clientname", param("first_name") + " " + param("last_name"));
        clientMap.put("phone", param("phoneCode").concat(param("phone")));
        clientMap.put("id", client.getLongId());
        
        response.put("item", clientMap);
        
        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        } else {
            view(response);
        }
    }
}
