package app.controllers;

import org.javalite.activeweb.AppController;
import com.google.inject.Inject;
import org.javalite.activeweb.annotations.*;    
import org.javalite.activejdbc.*;
import org.joda.time.DateTime;
import app.models.*;
import app.utils.*;
import app.services.*;
import java.util.*;
import java.sql.Timestamp;

/**
 * @author Askhat Shakenov
 */
public class OrdersController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
        
    }

    public void cashierList() {
        String groupname = param("groupname");
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        StringBuffer sb = new StringBuffer();
        sb.append("select o.id, o.user_id, o.client_id, concat(cp.first_name,' ', cp.last_name) clientname, ")
          .append("up.first_name username, ct.value phone, o.sum, o.created_at, o.due_date, o.status_code from orders o ")
          .append("join clients c on c.id = o.client_id ")
          .append("join users u on u.id = o.user_id ")
          .append("join profiles cp on cp.id = c.profile_id ")
          .append("join profiles up on up.id = u.profile_id ")
          .append("join contacts ct on cp.id = ct.profile_id and ct.title = 'PHONE' ")
          .append("where o.group_id = ? and (cp.first_name ilike ? or cp.last_name ilike ? or ct.value ilike ?) ")
          .append("order by o.status_code, cp.first_name, o.id ");

        List<Map> itemsPage = Base.findAll(sb.toString(), groupId(), "%"+q+"%", "%"+q+"%", "%"+q+"%");
        
        boolean hasNext = Order.count("group_id = ?", groupId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>(4);
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

    public void itemsList() {
        String groupname = param("groupname");
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(!blank("q"))
            q = params1st().get("q");           

        Long orderId = Long.valueOf(param("order_id"));

        StringBuffer sb = new StringBuffer();
        sb.append("select p.title, p.artcode, p.pricewithtax, op.amount, op.sum, vals.*, ids.* ")
          .append("from crosstab(' ")
            .append("select p.id, f.title, fv.title  ")
            .append("from products p  ")
            .append("join products_feature_values pfv on pfv.product_id = p.id  ")
            .append("join feature_values fv on fv.id = pfv.feature_value_id  ")
            .append("join features f on f.id = pfv.feature_id  ")
            .append("order by p.title, p.id, f.id ")
          .append("') AS vals(product_id bigint, width varchar, color varchar), crosstab(' ")
            .append("select p.id, f.id, fv.id  ")
            .append("from products p  ")
            .append("join products_feature_values pfv on pfv.product_id = p.id  ")
            .append("join feature_values fv on fv.id = pfv.feature_value_id  ")
            .append("join features f on f.id = pfv.feature_id  ")
            .append("--where p.id = op.product_id  ")
            .append("order by p.title, p.id, f.id ")
          .append("') AS ids(product_id bigint, width_id bigint, color_id bigint), ")
        .append("products p ")
        .append("join orders_products op on op.product_id = p.id and op.order_id = ? ")
        .append("where p.id = vals.product_id and p.id = ids.product_id ")
        .append("order by p.title, p.artcode, p.id");

        List<Map> itemsPage = Base.findAll(sb.toString(), orderId);
        
        // boolean hasNext = OrdersProducts.count("order_id = ?", orderId);

        Map<String, Object> response = new HashMap<String, Object>(4);
        response.put("items", itemsPage);
        // response.put("page", page);
        // response.put("hasNext", hasNext);
        // response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

    @GET
    public void add() {
        render("show");
    }


    public void show() {
        Order item = (Order) Order.findById(id());

        if(item == null) {
            view("messages", "page not found");
            return;
        }

        String groupname = param("groupname");
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        StringBuffer sb = new StringBuffer();
        sb.append("select p.id, p.title, op.amount amount, op.pricewithtax, op.sum from products p ")
          .append("join orders_products op on op.product_id = p.id and op.order_id = ? ")
          .append("where p.title ilike ? ")
          .append("order by 1 ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), item.getLongId(), "%"+q+"%", pageSize, (page - 1) * pageSize);
        
        boolean hasNext = OrdersProducts.count("order_id = ?", item.getLongId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("item", item);
        result.put("items", itemsPage);
        result.put("page", page);
        result.put("hasNext", hasNext);
        result.put("pageSize", pageSize);
        view(result);
    }

    @POST
    public void save() {
        Order item = new Order();
        item.set(
            "user_id", userId(), 
            "group_id", groupId(), 
            "sum", Double.valueOf(param("sum")),
            "client_id", Long.valueOf(param("client_id")),
            "status_code", "OPEN"
        );

        if(blank("due_date"))
            item.set("due_date", new Timestamp(new DateTime().withTimeAtStartOfDay().plusDays(1).minusSeconds(1).toDate().getTime()));

        System.out.println(">>>>>>>>> params >>>>>> " + params1st());
        System.out.println(">>>>>>>>> due date >>>>>> " + item.get("due_date"));

        Map<String, Object> response = new HashMap(4);
        int status = OK;

        List<String> values = params("products_length");
        System.out.println(">>>>>>>>> params prods >>>>>> " + values);

        for(int i = 0; i < Integer.parseInt(param("products_length")); i++) {
            System.out.println(">>>>>>> prods >>>>>>" + param("products[" + i + "][title]"));
        }

        // for(String id : values) {
        //     AttributeValue av = AttributeValue.findById(Long.valueOf(id));
        //     if(av == null)
        //         continue;

        //     ComboProductsValues.createIt(
        //         "combo_product_id", combo.getLongId(),
        //         "attribute_value_id", av.getLongId(),
        //         "user_id", userId()
        //     );
        // }

        if(!item.save()) {
            response.put("message", "Упс, произошла ошибка.");
            response.put("subMessage", "Проверьте введенные данные и попробуйте снова.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = BAD;
            if(!xhr()) {
                flash(response);
                redirect(OrdersController.class, "add");
                return;
            }
        } else {

            response.put("alert", "success");
            response.put("message", "Заказ cформирован успешно!");
            response.put("subMessage", "Просьба перейти на кассу для совершения оплаты.");
            response.put("item", item);

            for(int i = 0; i < Integer.parseInt(param("products_length")); i++) {
                System.out.println(">>>>>>> prods >>>>>>" + param("products[" + i + "][title]"));
                Product p = Product.findById(Long.valueOf(param("products[" + i + "][id]")));
                if(p == null)
                    continue;
                OrdersProducts.createIt(
                    "order_id", item.getLongId(),
                    "product_id", p.getLongId(),
                    "pricewithtax", p.getDouble("pricewithtax"),
                    "amount", blank("products[" + i + "][amount]") ? 1 : Double.valueOf(param("products[" + i + "][amount]")),
                    "sum", Double.valueOf(param("products[" + i + "][sum]"))
                );
            }
            
            if(!xhr()) {
                flash(response);
                view("item", item);
                redirect(OrdersController.class, "show", item.getLongId());
                return;
            }
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);

    }

    @PUT
    public void payment() {
        Order item = (Order) Order.findById(id());

        Map<String, Object> response = new HashMap(5);
        int status = OK;

        if(blank("amount") || Double.valueOf(param("amount")) < item.getDouble("sum")) {
            response.put("message", "Упс, произошла ошибка.");
            response.put("subMessage", "Проверьте введенные данные и попробуйте снова.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = BAD;
            if(!xhr()) {
                flash(response);
                redirect(OrdersController.class, "add");
                return;
            }
        }

        OrdersPayments.createIt(
            "order_id", item.getLongId(),
            "user_id", userId(), 
            "type_code", "CASH",
            "amount", Double.valueOf(param("amount"))
        );

        item.set("status_code", "PAID");
        item.save();

        System.out.println(">>>>>>>>> params >>>>>> " + params1st());
        System.out.println(">>>>>>>>> due date >>>>>> " + item.get("due_date"));
        
        response.put("alert", "success");
        response.put("message", "Оплата произведена успешно!");
        response.put("subMessage", "Просьба перейти туда, где выдают товар. Где-то там... ;)");
        response.put("item", item);
            
        if(!xhr()) {
            flash(response);
            view("item", item);
            redirect(OrdersController.class, "show", item.getLongId());
            return;
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    @PUT
    public void stock() {
        Order item = (Order) Order.findById(id());

        Map<String, Object> response = new HashMap(5);
        int status = OK;

        item.set("status_code", "CLOSED");
        item.save();
        
        response.put("alert", "success");
        response.put("message", "Все прошло успешно, запасы списаны, заказ закрыт!");
        response.put("subMessage", "Просьба сказать \"спасибо\" и улыбнуться клиенту! ;)");
        response.put("item", item);
            
        if(!xhr()) {
            flash(response);
            view("item", item);
            redirect(OrdersController.class, "show", item.getLongId());
            return;
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    @PUT
    public void changeDueDate() {
        Order item = (Order) Order.findById(id());
        if(item == null) {
            flash("message", "Error: Order not found");
            redirect(OrdersController.class, "index");
        }
        item.set("due_date", new Timestamp(new DateTime().withTimeAtStartOfDay().plusDays(2).minusSeconds(1).toDate().getTime()));
        
        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
         
            response.put("message", "Order was updated: " + item.getLongId());
            response.put("alert", "success");
        }

        flash(response);
        redirect(OrdersController.class, "show", item.getLongId());
    }

    @POST
    public void addProduct() {

        if(blank("id") || blank("product_id")) {
            flash("message", "Error: Order and product IDs (id, product_id) have to be provided");
            redirect(OrdersController.class, "index");
        }

        Order item = (Order) Order.findById(id());
        Product p = (Product) Product.findById(Long.valueOf(param("product_id")));
        if(item == null || p == null) {
            flash("message", "Error: Order not found");
            redirect(OrdersController.class, "index");
        }

        OrdersProducts op = OrdersProducts.findFirst("order_id = ? and product_id = ?", item.getLongId(), p.getLongId());
        if(op == null) {
            op = new OrdersProducts();
            op.set(
                "order_id", item.getLongId(),
                "product_id", p.getLongId(),
                "pricewithtax", p.getDouble("pricewithtax")
            );
        }

        op.set(
            "amount", blank("amount") ? 1 : Double.valueOf(param("amount")),
            "sum", Double.valueOf(param("amount")) * op.getDouble("pricewithtax")
        );

        op.save();
        Map<String, Object> response = new HashMap(3);
        response.put("message", "Product was added: " + p.getString("title"));
        response.put("alert", "success");
        response.put("item", op);
        flash(response);
        redirect(OrdersController.class, "show", item.getLongId());
    }
}
