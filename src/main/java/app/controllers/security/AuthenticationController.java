package app.controllers;

import app.controllers.*;
import app.models.*;
import app.services.*;
import app.utils.*;
import java.util.*;
import org.javalite.activejdbc.*;
import org.javalite.activeweb.annotations.DELETE;
import org.javalite.activeweb.annotations.POST;
import org.javalite.activeweb.AppController;
import org.javalite.activeweb.Cookie;

/**
 * @author Askhat Shakenov
 */
public class AuthenticationController extends Controller {

    public void login() {
    	String groupname = param("g");
        String rolename = param("rid");

        Group group = Group.findFirst("groupname = ?", groupname);
        if(blank("g") && blank("rid"))
            return;

        if(!blank("g") && blank("rid")) {
            List<Role> roles = Role.find("group_id = ?", group.getLongId());
            view("roles", roles);
            // return;
        }

        if(!blank("g") && !blank("rid")) {
            List<User> users = group.get(User.class, "role_id = ?", Long.valueOf(param("rid")));
            view("users", users);
        }
    }

    @POST
    public void doLogin() {
    	Map<String, Object> response = new HashMap<String, Object>(2);
    	int status = 200;

        String username = param("username");
        String password = param("password");
        boolean rememberMe = !blank("remember_me");

        User user = (User) User.findFirst("username = ?", username);
        if(user == null) {
            flash("message", "auth.username.error");
            redirect(AuthenticationController.class, "login");
            return;
        }

        BCryptPasswordCryptor matcher = new BCryptPasswordCryptor();
        if(!matcher.match(password, user.getString("password"))) {
            flash("message", "auth.password.error");
            redirect(AuthenticationController.class, "login");
            return;
        }
        
        Subject subject = subject(username);
        Group g = null;
        
        if(subject == null) {
            List<Group> groups = (List<Group>) user.getAll(Group.class);
            g = groups.get(0);
            subject = new Subject(user, true, rememberMe);
            subject.setCurrentGroup(g);
            session(SESSION_KEY_SUBJECT, subject);
            // session(username.concat(":").concat(SESSION_KEY_SUBJECT), subject);
        } else {
            subject.isAuthenticated(true);
        }

        Cookie cookie = new Cookie("last_login", username);
        // cookie.setSecure(true);
        sendCookie(cookie);

        flash("message", "auth.login.success");
        String path = (String) session("redirectPath");
        session("redirectPath", null);

        if(path == null)
            redirect("/");
        else 
            redirect(path);
    }

    public void logout() {
    	subject().isAuthenticated(false);
        if(!subject().rememberMe())
            session().remove(subject().username().concat(":").concat(SESSION_KEY_SUBJECT));
    	redirect(AuthenticationController.class, "login");
    }

    // public void setGroup() {
    // 	Group g = Group.findFirst("groupname = ?", param("groupname"));
    // 	if(g != null)
    // 		session(SESSION_KEY_GROUP, g);
    // 	redirect(HomeController.class);
    // }

    public String getLayout() {
    	return "/authentication/layout";
    }

    public void create() {
        if(Group.count() == 0) {
            Group group = Group.createIt("groupname", "ys", "title", "Yerke Sylkym");
            User u = User.newUser("askhat", "123456");
            u.saveIt();

            User u2 = User.newUser("azoda", "123456");
            u2.saveIt();
            Role role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());
            Role role2 = Role.createIt("title", "Account", "level", 10, "group_id", group.getLongId());
            Role role3 = Role.createIt("title", "Cashier", "level", 20, "group_id", group.getLongId());
            Role role4 = Role.createIt("title", "Stockman", "level", 20, "group_id", group.getLongId());
            Role role5 = Role.createIt("title", "Salesman", "level", 20, "group_id", group.getLongId());

            GroupsUsers ur1 = new GroupsUsers();
            ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
            ur1.save();

            GroupsUsers ur2 = new GroupsUsers();
            ur2.set("group_id", group.getLongId(), "user_id", u2.getLongId(), "role_id", role2.getLongId());
            ur2.save();

            Profile p = new Profile();
            p.set("first_name", "Askhat", "last_name", "Shakenov", "user_id", u.getLongId());
            p.saveIt();

            Profile p2 = new Profile();
            p2.set("first_name", "Azoda", "last_name", "Shakenova", "user_id", u2.getLongId());
            p2.saveIt();
        }

        redirect(AuthenticationController.class,"login");
    }
}
