package app.controllers;

import app.models.*;
import java.util.*;
import org.javalite.activejdbc.Base;
import org.javalite.activeweb.annotations.*;
import org.javalite.activeweb.AppController;

/**
 * @author Askhat Shakenov
 */
public class UsersController extends Controller {
	private int page = 1;
    private int pageSize = 10;
    private String q = "";

    public void index() {
    	try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Group g = group();

        StringBuffer sb = new StringBuffer();
        sb.append("select u.username, u.created_at ")
          .append("from users u, groups_users gu where u.username ilike ? ")
          .append("and gu.user_id = u.id and gu.group_id = ? ")
          .append("order by u.username ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", g.getLongId(), pageSize, (page - 1) * pageSize);
        
        boolean hasNext = User.count() > (page - 1) * pageSize + pageSize;

        // Paginator p = new Paginator(Product.class, pageSize, "title ilike ?", "%"+q+"%").orderBy("id");
        // if(xhr()) {
        //     Map<String, Object> result = new HashMap<String, Object>();
        //     result.put("items", itemsPage);
        //     result.put("page", page);
        //     result.put("hasNext", hasNext);
        //     String json = app.utils.Json.fromMap(result);

        //     respond(json).contentType("application/json").status(200);
        //     System.out.println(">>>>>>>>>>>>  response json");
        //     return;
        // }

        view("items", itemsPage);
        view("page", page);
        view("pageSize", pageSize);
        view("hasNext", hasNext);
    }

    public void show() {
        //this is to protect from URL hacking
        User item = (User) User.findFirst("username = ?", param("username"));

        if(item != null) {
            view("item", item);
        } else {
            view("messages", "page not found");
        }
    }

    @POST
    public void save() {
        Group g = group();

        if(g == null || !blank("role_id")) {
            redirect(HomeController.class, "index");
        }

        if(!blank("role_id") && g != null) {
            User item = User.newUser(param("username"), param("password"));

            Map<String, Object> response = new HashMap(4);
            int status = 200;

            if(!item.save()) {
                flash("message", "Пожалуйста, заполните все необходимые поля.");
                flash("errors", item.errors());
                flash("params", params1st());
                flash("alert", "danger");
            } else {
                flash("alert", "success");
                flash("message", "New user was added: " + item.get("username"));
                
                GroupsUsers.createIt("user_id", item.getLongId(), "group_id", g.getLongId(), "role_id", Long.valueOf(param("role_id")));
                
                view("item", item);
            }

            redirect(UsersController.class, "show", item.get("username"));
        } else {
            redirect(HomeController.class, "index");
        }
        
    }

    @PUT
    public void update() {
        User item = (User)User.findFirst("username = ?", param("username"));
        item.set("password", param("password"));

        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
            
            response.put("message", "User was updated: " + item.get("username"));
            response.put("alert", "success");
        }

        flash(response);
        redirect(UsersController.class, "show", item.getString("username"));
    }

    @DELETE
    public void delete() {
        User item = (User)User.findFirst("username = ?", param("username"));
        item.delete();
        flash("message", "User: '" + param("username") + "' was deleted");
        redirect(UsersController.class);
    }
}
