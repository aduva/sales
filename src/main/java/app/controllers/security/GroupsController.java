package app.controllers;

import app.models.*;
import java.util.*;
import org.javalite.activejdbc.Base;
import org.javalite.activeweb.annotations.*;
import org.javalite.activeweb.AppController;

/**
 * @author Askhat Shakenov
 */
public class GroupsController extends AppController {
	private int page = 1;
    private int pageSize = 10;
    private String q = "";

    public void index() {
    	try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        StringBuffer sb = new StringBuffer();
        sb.append("select g.groupname, g.created_at ")
          .append("from groups g where g.groupname ilike ? or g.title ilike ?")
          .append("order by g.groupname ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", "%"+q+"%", pageSize, (page - 1) * pageSize);
        
        boolean hasNext = Group.count() > (page - 1) * pageSize + pageSize;

        // Paginator p = new Paginator(Product.class, pageSize, "title ilike ?", "%"+q+"%").orderBy("id");
        // if(xhr()) {
        //     Map<String, Object> result = new HashMap<String, Object>();
        //     result.put("items", itemsPage);
        //     result.put("page", page);
        //     result.put("hasNext", hasNext);
        //     String json = app.utils.Json.fromMap(result);

        //     respond(json).contentType("application/json").status(200);
        //     System.out.println(">>>>>>>>>>>>  response json");
        //     return;
        // }

        view("items", itemsPage);
        view("page", page);
        view("pageSize", pageSize);
        view("hasNext", hasNext);
    }

    public void show() {
        //this is to protect from URL hacking
        Group item = (Group) Group.findFirst("groupname = ?", param("groupname"));

        if(item != null) {
            view("item", item);
        } else {
            view("messages", "page not found");
        }
    }

    @POST
    public void save() {
        Group item = new Group();
        item.set("groupname", param("groupname"), "title", param("title"));

        Map<String, Object> response = new HashMap(4);
        int status = 200;

        if(!item.save()) {
            flash("message", "Пожалуйста, заполните все необходимые поля.");
            flash("errors", item.errors());
            flash("params", params1st());
            flash("alert", "danger");
            status = 400;
        } else {
            flash("alert", "success");
            flash("message", "New group was added: " + item.get("groupname"));
            
            view("item", item);
        }

        redirect(GroupsController.class, "show", item.get("groupname"));
    }

    @PUT
    public void update() {
        Group item = (Group)Group.findFirst("groupname = ?", param("groupname"));
        item.set("title", param("title"));

        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
            
            response.put("message", "Group was updated: " + item.get("groupname"));
            response.put("alert", "success");
        }

        flash(response);
        redirect(GroupsController.class, "show", item.getString("groupname"));
    }

    @DELETE
    public void delete() {
        Group item = (Group)Group.findFirst("groupname = ?", param("groupname"));
        item.delete();
        flash("message", "Group: '" + param("groupname") + "' was deleted");
        redirect(GroupsController.class);
    }
}
