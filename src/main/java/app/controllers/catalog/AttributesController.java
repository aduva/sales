package app.controllers;

import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.DELETE;
import org.javalite.activeweb.annotations.POST;
import org.javalite.activeweb.annotations.GET;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.*;
import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class AttributesController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Group g = group();

        StringBuffer sb = new StringBuffer();
        sb.append("select * ")
          .append("from attributes a where a.title ilike ? and a.group_id = ?")
          .append("order by a.id ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", g.getLongId(), pageSize, (page - 1) * pageSize);
        
        boolean hasNext = Attribute.count("title ilike ? and group_id = ?", "%"+q+"%", g.getLongId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    public void show() {
        Attribute item = (Attribute) Attribute.findById(Long.valueOf(getId()));

        if(item != null){
            view("item", item);
            view("values", item.getAll(AttributeValue.class));
        }else{
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }
    }
    
    @POST
    public void create() {
        Attribute item = new Attribute();
        item.fromMap(params1st());
        item.set("user_id", userId(), "group_id", group().getLongId());
        if(!item.save()) {
            flash("message", "Something went wrong, please  fill out all fields");
            flash("errors", item.errors());
            flash("params", params1st());
            redirect(AttributesController.class, "new_form");
        }else{
            flash("message", "New attribute was added: " + item.get("title"));
            redirect(AttributesController.class);
        }
    }

    @POST
    public void addValue() {
        Attribute item = (Attribute) Attribute.findById( Long.valueOf(param("attribute_id")) );
        if(item != null) {
            AttributeValue val = new AttributeValue();
            val.set("title", param("title"));
            val.set("attribute_id", item.get("id"));
            val.set("user_id", userId());
            if(!val.save()) {
                flash("message", "Something went wrong, please  fill out all fields");
                flash("errors", val.errors());
                flash("params", params1st());
                redirect(AttributesController.class, "new_value_form");
            } else {
                flash("message", "New attribute value was added: " + val.get("title"));
                redirect(AttributesController.class, "show", item.get("id"));
            }
        }
    }

    @DELETE
    public void delete(){
        Attribute item = (Attribute) Attribute.findById(Long.valueOf(getId()));
        String title = item.getString("title");
        item.delete(true);
        flash("message", "Attribute: '" + title + "' was deleted");
        redirect(AttributesController.class);
    }

    public void newForm() {}

    public void newValueForm() {
        Attribute item = (Attribute) Attribute.findById(Long.valueOf(getId()));

        if(item != null){
            view("attribute", item.get("id"));
        } else {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }
    }

    @DELETE
    public void deleteValue(){
        AttributeValue item = (AttributeValue) AttributeValue.findById(Long.valueOf(getId()));
        String title = item.getString("title");
        Long id = item.getLong("attribute_id");
        item.delete();
        flash("message", "Attribute Value: '" + title + "' was deleted");
        redirect(AttributesController.class, "show", id);
    }

    public void attributeValues() {
        Attribute item = (Attribute) Attribute.findById(id());
        if(item != null) {
            List<Map> items = item.getAll(AttributeValue.class).toMaps();
            
            if(!xhr()) {
                view("items", items);
                render().noLayout();
            } else {
                for(Map map : items) {
                    // map.put("attribute")
                    map.put("text", map.get("title"));
                }
                respond(Json.fromList(items)).contentType("application/json").status(200);
            }

        } else {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }
    }
}
