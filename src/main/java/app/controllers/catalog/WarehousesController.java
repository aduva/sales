package app.controllers;

import com.google.inject.Inject;
import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.*;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.*;
import app.services.*;
import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class WarehousesController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    @Inject
    private PubsubService pub;

    public void index() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        StringBuffer sb = new StringBuffer();
        sb.append("select * ")
          .append("from warehouses w where w.title ilike ? and w.group_id = ? ")
          .append("order by w.id ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", groupId(), pageSize, (page - 1) * pageSize);
        
        boolean hasNext = Warehouse.count("group_id = ?", groupId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

    public void show() {
        Warehouse item = (Warehouse) Warehouse.findById(id());
        if(item == null) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

       view("item", item);
    }

    public void add() {

    }

    @POST
    public void save() {
        Warehouse item = new Warehouse();
        item.set("title", param("title"), "artcode", param("artcode"), "user_id", userId(), "group_id", groupId());

        Map<String, Object> response = new HashMap(4);
        int status = 200;

        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
            if(!xhr()) {
                flash(response);
                redirect(WarehousesController.class, "add");
            }
        } else {

            response.put("alert", "success");
            response.put("message", "New Warehouse was added: " + item.get("title"));
            
            response.put("item", item);
            
            if(!xhr()) {
                flash(response);
                view("item", item);
                redirect(WarehousesController.class, "show", item.getLongId());
            }
        }
    }

    @POST
    public void update() {
        Warehouse item = (Warehouse) Warehouse.findById(id());
        if(item == null) {
            flash("message", "Error: Warehouse not found");
            redirect(WarehousesController.class, "index");
        }
        item.set("title", param("title"), "artcode", param("artcode"));
        
        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
         
            response.put("message", "Warehouse was updated: " + item.get("title"));
            response.put("alert", "success");
        }

        flash(response);
        redirect("/warehouses/" + item.getLongId() + "/show");
    }

    @GET
    public void management() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        StringBuffer sb = new StringBuffer();
        List<Map> itemsPage = null;
        if(blank("pid")) {
            sb.append("select p.id, p.title, p.artcode, ")
              .append("sum(coalesce(wp.stock, 0)) stock, ")
              .append("count(cp.id) combos from products p ")
              .append("left join warehouses_products wp on wp.product_id = p.id ")
              .append("left join composites c on c.product_id = p.id ")
              .append("left join combo_products cp on cp.composite_id = c.id ")
              .append("where p.type_code not in('COMBO', 'P_COMBO') ")
              .append("and p.title ilike ? and p.group_id = ? ")
              .append("group by p.id, c.id order by p.id ")
              .append("limit ? offset ?");
            itemsPage = Base.findAll(sb.toString(), "%"+q+"%", groupId(), pageSize, (page - 1) * pageSize);
        } else {
            sb.append("select p.id, p.title, p.artcode, p.type_code, sum(coalesce(wp.stock, 0)) as stock, 0 combos from products p ")
              .append("join combo_products cp on cp.product_id = p.id ")
              .append("join composites c on c.product_id = ? and cp.composite_id = c.id ")
              .append("left join warehouses_products wp on wp.product_id = p.id ")
              .append("where p.title ilike ? and p.group_id = ? ")
              .append("group by p.id order by p.id ")
              .append("limit ? offset ?");
            itemsPage = Base.findAll(sb.toString(), Long.valueOf(param("pid")), "%"+q+"%", groupId(), pageSize, (page - 1) * pageSize);
        }

        boolean hasNext = Warehouse.count("group_id = ?", groupId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

    public void manage() {
        Product product = (Product) Product.findById(id());
        List<Warehouse> warehouses = Warehouse.find("group_id = ?", groupId());

        view("product", product);
        view("warehouses", warehouses);
    }

    @POST
    public void updateStock() {
        Product p = (Product) Product.findById(id());
        Warehouse source = (Warehouse) Warehouse.findById(Long.valueOf(param("source")));

        if(source == null || p == null) {
            flash("message", "Error: Warehouse or product is null");
            redirect(WarehousesController.class, "manage", p.getLongId());
            return;
        }

        Double amount = Double.valueOf(param("amount"));

        WarehousesProducts wp = (WarehousesProducts) WarehousesProducts.findFirst("warehouse_id = ? and product_id = ?", source.getLongId(), p.getLongId());

        if(wp == null) {
            wp = new WarehousesProducts();
            wp.set("warehouse_id", source.getLongId(), "product_id", p.getLongId(), "stock", 0);
        }

        ArrayList<Map> list = new ArrayList();

        if(param("manage_type_code").equalsIgnoreCase("DECREASE")) {
            wp.set("stock", wp.getDouble("stock") - amount);

        } else if(param("manage_type_code").equalsIgnoreCase("INCREASE")) {
            wp.set("stock", wp.getDouble("stock") + amount);

        } else if(param("manage_type_code").equalsIgnoreCase("TRANSFER")) {
            Warehouse target = (Warehouse) Warehouse.findById(Long.valueOf(param("target")));

            WarehousesProducts tp = (WarehousesProducts) WarehousesProducts.findFirst(
                "warehouse_id = ? and product_id = ?", target.getLongId(), p.getLongId());
            if(tp == null) {
                tp = new WarehousesProducts();
                tp.set("warehouse_id", target.getLongId(), "product_id", p.getLongId(), "stock", 0);
            }

            wp.set("stock", wp.getDouble("stock") - amount);
            tp.set("stock", tp.getDouble("stock") + amount);
            tp.save();
            WarehouseMovement.createIt(
                "manage_type_code", blank("target") ? param("manage_type_code") : "TRANSFER_TO",
                "amount", amount,
                "product_id", p.getLongId(),
                "warehouse_id", source.getLongId(),
                "user_id", userId(),
                "group_id", groupId(),
                "manage_subject_id", blank("target") ? null : Long.valueOf(param("target"))
            );
        }

        wp.save();

        WarehouseMovement.createIt(
            "manage_type_code", blank("target") ? param("manage_type_code") : "TRANSFER_TO",
            "amount", amount,
            "product_id", p.getLongId(),
            "warehouse_id", source.getLongId(),
            "user_id", userId(),
            "group_id", groupId(),
            "manage_subject_id", blank("target") ? null : Long.valueOf(param("target"))
        );
        
        redirect(WarehousesController.class, "management");
    }

    @POST
    public void changeStockAmount() {
        Warehouse item = (Warehouse) Warehouse.findById(id());
        Product p = (Product) Product.findById(Long.valueOf(param("product_id")));

        if(item == null || p == null) {
            flash("message", "Error: Warehouse or product is null");
            redirect(WarehousesController.class, "show", item.getLongId());
            return;
        }

        WarehousesProducts wp = (WarehousesProducts) WarehousesProducts.findFirst("warehouse_id = ? and product_id = ?", id(), p.getLongId());
        if(wp == null) {
            wp = new WarehousesProducts();
            wp.set("warehouse_id", id(), "product_id", p.getLongId(), "stock", 0);
        }

        wp.set("stock", Double.valueOf(param("stock_amount")));
        wp.save();
        
        redirect(WarehousesController.class, "show", item.getLongId());
    }

    @POST
    public void increaseStockAmount() {
        Warehouse item = (Warehouse) Warehouse.findById(id());
        Product p = (Product) Product.findById(Long.valueOf(param("product_id")));

        if(item == null || p == null) {
            flash("message", "Error: Warehouse or product is null");
            redirect(WarehousesController.class, "show", item.getLongId());
            return;
        }

        WarehousesProducts wp = (WarehousesProducts) WarehousesProducts.findFirst("warehouse_id = ? and product_id = ?", id(), p.getLongId());
        if(wp == null) {
            wp = new WarehousesProducts();
            wp.set("warehouse_id", id(), "product_id", p.getLongId(), "stock", 0);
        }

        double stock = wp.getDouble("stock") + Double.valueOf(param("stock_amount"));
        wp.set("stock", stock);
        wp.save();
        
        redirect(WarehousesController.class, "show", item.getLongId());
    }

    @POST
    public void decreaseStockAmount() {
        Warehouse item = (Warehouse) Warehouse.findById(id());
        Product p = (Product) Product.findById(Long.valueOf(param("product_id")));

        if(item == null || p == null) {
            flash("message", "Error: Warehouse or product is null");
            redirect(WarehousesController.class, "show", item.getLongId());
            return;
        }

        WarehousesProducts wp = (WarehousesProducts) WarehousesProducts.findFirst("warehouse_id = ? and product_id = ?", id(), p.getLongId());
        if(wp == null) {
            wp = new WarehousesProducts();
            wp.set("warehouse_id", id(), "product_id", p.getLongId(), "stock", 0);
        }

        double stock = wp.getDouble("stock") - Double.valueOf(param("stock_amount"));
        wp.set("stock", stock);
        wp.save();
        
        redirect(WarehousesController.class, "show", item.getLongId());
    }

    @GET
    public void movement() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");
        
        StringBuffer sb = new StringBuffer();
        sb.append("select mw.id, p.title, p.artcode, mw.amount, mw.manage_type_code, w.title warehouse_title, mw.created_at ")
          .append("from warehouse_movements mw ")
          .append("join products p on p.id = mw.product_id ")
          .append("join warehouses w on w.id = mw.warehouse_id ")
          .append("order by mw.id ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), pageSize, (page - 1) * pageSize);

        boolean hasNext = Warehouse.count("group_id = ?", groupId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

}
