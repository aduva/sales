package app.controllers;

import com.google.inject.Inject;
import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.*;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.*;
import java.util.*;
import app.services.*;

/**
 * @author Askhat Shakenov
 */
public class ProductsController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
        String groupname = param("groupname");
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Group g = group();

        StringBuffer sb = new StringBuffer();
        sb.append("select * ")
          .append("from products p where (p.artcode ilike ? or p.title ilike ?) and p.group_id = ?")
          .append("order by p.id ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", "%"+q+"%", g.getLongId(), pageSize, (page - 1) * pageSize);
        
        boolean hasNext = Product.count("group_id = ?", g.getLongId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    public void show() {
        Product item = (Product) Product.findById(id());

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        if(item != null) {
            response.put("item", item);
        } else {
            response.put("messages", "page not found");
        }

        view(response);

        if(xhr()) {
            if(item != null) {
                response.put("item", item.toJson(false));
            }
            
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @GET
    public void add() {
        render("show");
    }

    @POST
    public void save() {
        System.out.println();
        System.out.println("============= " + params1st());
        System.out.println();
        Product item = new Product();
        item.set("title", param("title"), "artcode", param("artcode"), "type_code", param("type_code"), "user_id", userId(), "group_id", groupId());

        Map<String, Object> response = new HashMap(4);
        int status = 200;

        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
            if(!xhr()) {
                flash(response);
                redirect(ProductsController.class, "add");
                return;
            }
        } else {

            response.put("alert", "success");
            response.put("message", "New product was added: " + item.get("title"));
            item.set("cost", null, "pricepretax", null, "pricewithtax", null, "tax_id", null, "priceperunit", null, "unitname", null);
            response.put("item", item.toMap());
            
            if(!xhr()) {
                flash(response);
                view("item", item);
                redirect(ProductsController.class, "show", item.getLongId());
                return;
            }
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    @PUT
    public void update() {
        Product item = (Product)Product.findById(id());
        item.set(
            "title", param("title"), 
            "artcode", param("artcode"),
            "type_code", param("type_code")
        );
        

        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
            response.put("message", "Product was updated: " + item.get("title"));
            response.put("alert", "success");
        }
        
        if(!xhr()) {
            flash(response);
            redirect(ProductsController.class, "show", item.getLong("id"));
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    @GET
    public void warehouses() {
        Product item = Product.findById(id());
        StringBuffer sb = new StringBuffer();
        
        sb.append("select w.id warehouse_id, w.title, w.artcode, wp.stock, wp.id warehouse_product_id from warehouses w ")
           .append("join warehouses_products wp on wp.warehouse_id = w.id ")
           .append("join products p on wp.product_id = p.id and p.id = ? ")
           .append("group by 1, wp.stock, wp.id order by 1");
        
        List<Map> items = Base.findAll(sb.toString(), item.getLongId());

        Map<String, Object> response = new HashMap(1);
        int status = 200;
        if(!xhr()) {
            response.put("items", items);
            view(response);
            render("_forms/warehouses").noLayout();
        } else {
            response.put("items", Json.fromList(items));
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @POST
    public void addWarehouse() {
        Map map = params1st();
        
        Product item = Product.findById(Long.valueOf((String) map.get("product_id")));
        Warehouse warehouse = Warehouse.findById(id());
        
        WarehousesProducts warehousesProducts = WarehousesProducts.findFirst(
            "product_id = ? and warehouse_id = ?",
            item.getLongId(), warehouse.getLongId()
        );

        if(warehousesProducts == null) {
            warehousesProducts = WarehousesProducts.createIt(
                "product_id", item.getLongId(),
                "warehouse_id", warehouse.getLongId(),
                "stock", 0
            );
        }

        Map<String, Object> response = new HashMap(3);
        if(!xhr()) {
            response.put("item", item);
            view(response);
            render("_forms/warehouses").noLayout();
        } else {
            Map itemMap = new HashMap(6);
            itemMap.put("warehouse_product_id", warehousesProducts.getLongId());
            itemMap.put("product_id", item.getLongId());
            itemMap.put("id", warehouse.getLongId());
            itemMap.put("stock", 0);
            itemMap.put("title", warehouse.getString("title"));
            itemMap.put("artcode", warehouse.getString("artcode"));
            response.put("item", itemMap);
            
            response.put("message", "products.warehouse.add");
            response.put("alert", "success");
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

    @DELETE
    public void deleteWarehouse() {
        WarehousesProducts item = WarehousesProducts.findById(id());
        Map<String, Object> response = new HashMap(2);
        if(item.delete()) {
            response.put("message", "products.warehouse.delete");
            response.put("alert", "success");

             if(!xhr()) {
                flash(response);
                render("_forms/warehouses").noLayout();
            } else {
                respond(Json.fromMap(response)).contentType("application/json").status(OK);
            }
        } else {
            response.put("message", "error.products.warehouse.delete");
            response.put("alert", "error");
        }
    }

    @GET
    public void combos() {
        Product item = Product.findById(id());
        StringBuffer sb = new StringBuffer();
        
        sb.append("select p.id, p.artcode, p.pricewithtax, p.priceperunit, string_agg(concat(a.title, ': ', av.title), ', ') as title, case p.id when c.product_id then true else false end as is_default from combo_products cp ")
            .append("join composites c on c.id=cp.composite_id and c.id = (select composite_id from combo_products where product_id = ?) ")
            .append("join combo_products_values cpv on cpv.combo_product_id = cp.id ")
            .append("join attribute_values av on av.id = cpv.attribute_value_id ")
            .append("join attributes a on a.id = av.attribute_id ")
            .append("join products p on cp.product_id = p.id ")
            .append("group by 1, c.product_id order by 1");
        
        List<Map> items = Base.findAll(sb.toString(), item.getLongId());

        Map<String, Object> response = new HashMap(1);
        int status = 200;
        if(!xhr()) {
            response.put("items", items);
            view(response);
            render().noLayout();
        } else {
            response.put("items", Json.fromList(items));
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @GET
    public void comboProductsValues() {
        Product item = Product.findById(id());
        StringBuffer sb = new StringBuffer();
        
        sb.append("select cpv.id, cpv.attribute_value_id, a.title as attribute, av.title from products p ")
            .append("join combo_products cp on cp.product_id = p.id ")
            .append("join combo_products_values cpv on cp.id = cpv.combo_product_id ")
            .append("join attribute_values av on av.id = cpv.attribute_value_id ")
            .append("join attributes a on a.id = av.attribute_id ")
            .append("where p.id = ?");
        
        List<Map> items = Base.findAll(sb.toString(), item.getLongId());

        Map<String, Object> response = new HashMap(1);
        int status = 200;
        if(!xhr()) {
            response.put("items", items);
            view(response);
        } else {
            response.put("items", Json.fromList(items));
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @DELETE
    public void deleteAttributeValue() {
        ComboProductsValues item = ComboProductsValues.findById(id());
        
        Map<String, Object> response = new HashMap(2);
        int status = 200;
        if(item == null) {
            response.put("message", "Ошибка удаления. Атрибут не найден.");
            response.put("alert", "danger");
            status = 400;
        } else {
            item.delete(true);
            response.put("message", "Атрибут удален успешно");
            response.put("alert", "success");
        }

        // if(xhr())
            respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    @POST
    public void addCombos() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        Map map = params1st();

        Product parent = Product.findById(Long.valueOf((String) map.get("product_id")));
        String typeCode = parent.getString("type_code");
        
        if(typeCode.equals("COMBO") || typeCode.equals("P_COMBO")) {
            flash("message", "Error: Combo can't have combo");
            render("show");
            return;
        }

        Product item = null;
        Composite composite = Composite.findFirst("product_id = ?", parent.getLongId());
        if(composite == null) {
            composite = Composite.createIt(
                "product_id", parent.getLongId(),
                "user_id", userId()
            );
            item = parent;
        } else {
            String comboTypeCode = "COMBO";
            if(typeCode.equals("PARTIAL"))
                comboTypeCode = "P_COMBO";

            item = Product.createIt(
                "artcode", param( "artcode" ),
                "pricewithtax", Double.parseDouble( param( "pricewithtax" ) ),
                "priceperunit", Double.parseDouble( param( "priceperunit" ) ),
                "user_id", userId(),
                "group_id", parent.getLong("group_id"),
                "title", parent.getString("title"),
                "type_code", comboTypeCode
            );
        }

        ComboProducts combo = ComboProducts.createIt(
            "product_id", item.getLongId(),
            "composite_id", composite.getLongId()
        );

        List<String> values = params("attribute_value_id[]");

        for(String id : values) {
            AttributeValue av = AttributeValue.findById(Long.valueOf(id));
            if(av == null)
                continue;

            ComboProductsValues.createIt(
                "combo_product_id", combo.getLongId(),
                "attribute_value_id", av.getLongId(),
                "user_id", userId()
            );
        }

        Map<String, Object> response = new HashMap(2);
        int status = 200;

        response.put("alert", "success");
        response.put("message", "Combo added");
        
        StringBuffer sb = new StringBuffer();
        
        sb.append("select p.id, p.artcode, p.pricewithtax, p.priceperunit, p.type_code, ")
            .append("string_agg(concat(a.title, ': ', av.title), ', ') as title, case p.id when c.product_id then true else false end as is_default from combo_products cp ")
            .append("join composites c on c.id=cp.composite_id and c.id = ? ")
            .append("join combo_products_values cpv on cpv.combo_product_id = cp.id and cp.id = ? ")
            .append("join attribute_values av on av.id = cpv.attribute_value_id ")
            .append("join attributes a on a.id = av.attribute_id ")
            .append("join products p on cp.product_id = p.id ")
            .append("group by 1, c.product_id order by 1");
        
        List<Map> items = Base.findAll(sb.toString(), composite.getLongId(), combo.getLongId());
        response.put("item", items.get(0));

        if(!xhr()) {
            view(response);
            render("_forms/combinations").noLayout();
        } else {
            response.put("items", Json.fromList(items));
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @PUT
    public void editCombo() {
        Map map = params1st();

        Map<String, Object> response = new HashMap(2);
        int status = 200;

        Product item = Product.findById(id());
        
        if(!blank("artcode")) {
            item.set("artcode", param( "artcode" ));            
        }

        if(!blank("priceperunit")) {
            item.set("priceperunit", Double.valueOf( param( "priceperunit" )));
        }

        if(!blank("pricewithtax")) {
            item.set("pricewithtax", Double.valueOf( param( "pricewithtax" )));
        }
        
        if(!item.save()) {
            status = 400;
            response.put("alert", "error");
            response.put("message", "Combo edit error");
            respond(Json.fromMap(response)).contentType("application/json").status(status);
            return;
        }

        ComboProducts combo = ComboProducts.findFirst(
            "product_id = ?", item.getLongId()
        );

        if(!blank("is_default")) {
            Composite composite = Composite.findFirst(
                "id = ?", combo.getLong("composite_id")
            );

            if(Boolean.valueOf(param("is_default")) && !composite.getLong("product_id").equals(item.getLongId())) {
                // combo.set("product_id", composite.getLong("product_id"));
                // combo.save();
                composite.set("product_id", item.getLongId());
                composite.save();
            }
        }

        List<String> values = params("attribute_value_id[]");

        for(String id : values) {
            AttributeValue av = AttributeValue.findById(Long.valueOf(id));
            if(av == null)
                continue;

            ComboProductsValues cpv = ComboProductsValues.findFirst(
                "combo_product_id = ? and attribute_value_id = ?", 
                combo.getLongId(), av.getLongId()
            );

            if(cpv != null) {
                continue;   
            }

            ComboProductsValues.createIt(
                "combo_product_id", combo.getLongId(),
                "attribute_value_id", av.getLongId(),
                "user_id", userId()
            );
        }

        response.put("alert", "success");
        response.put("message", "Combo edited");
        
        StringBuffer sb = new StringBuffer();
        
        sb.append("select p.id, p.artcode, p.pricewithtax, p.priceperunit, string_agg(concat(a.title, ': ', av.title), ', ') as title, case p.id when c.product_id then true else false end as is_default from combo_products cp ")
            .append("join composites c on c.id=cp.composite_id ")
            .append("join combo_products_values cpv on cpv.combo_product_id = cp.id and cp.id = ? ")
            .append("join attribute_values av on av.id = cpv.attribute_value_id ")
            .append("join attributes a on a.id = av.attribute_id ")
            .append("join products p on cp.product_id = p.id ")
            .append("group by 1, c.product_id order by 1");
        
        List<Map> items = Base.findAll(sb.toString(), combo.getLongId());
        response.put("item", items.get(0));

        if(!xhr()) {
            view(response);
            render("_forms/combinations").noLayout();
        } else {
            // response.put("items", Json.from(items));
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    
    @GET
    public void sets() {
        Product item = Product.findById(id());
        StringBuffer sb = new StringBuffer();
        
        sb.append("select p.id product_id, sp.id set_product_id, c.id composite_id, p.artcode, p.pricewithtax, p.priceperunit, p.title, sp.amount from set_products sp ")
           .append("join composites c on c.id=sp.composite_id and c.product_id = ? ")
           .append("join products p on sp.product_id = p.id ")
           .append("group by 1, 2, 3, c.product_id, sp.amount order by 1");
        
        List<Map> items = Base.findAll(sb.toString(), item.getLongId());

        Map<String, Object> response = new HashMap(1);
        int status = 200;
        if(!xhr()) {
            response.put("items", items);
            view(response);
            render("_forms/composites").noLayout();
        } else {
            response.put("items", Json.fromList(items));
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    @POST
    public void addSet() {
        Map map = params1st();
        
        // String username = param("username");

        Product item = Product.findById(id());
        Product set = Product.findById(Long.valueOf(param("product_id")));
        String typeCode = item.getString("type_code");
        
        if(typeCode.equals("PARTIAL") || typeCode.equals("COMBO") || typeCode.equals("P_COMBO")) {
            flash("message", "Error: Partial and combo can't have products in set");
            render("show");
            return;
        }

        Composite composite = Composite.findFirst("product_id = ?", item.getLongId());
        if(composite == null) {
            composite = Composite.createIt(
                "product_id", item.getLongId(),
                "user_id", userId()
            );
        }

        SetProducts setProduct = SetProducts.createIt(
            "product_id", set.getLongId(),
            "composite_id", composite.getLongId(),
            "amount", Double.valueOf(param("amount"))
        );

        Map<String, Object> response = new HashMap(3);
        if(!xhr()) {
            response.put("item", item);
            view(response);
            render("_forms/composites").noLayout();
        } else {
            Map itemMap = new HashMap(5);
            itemMap.put("set_product_id", setProduct.getLongId());
            itemMap.put("product_id", set.getLongId());
            itemMap.put("amount", setProduct.getDouble("amount"));
            itemMap.put("title", set.getString("title"));
            itemMap.put("artcode", set.getString("artcode"));
            itemMap.put("priceperunit", set.getDouble("priceperunit"));
            response.put("item", itemMap);
            
            response.put("message", "products.set.add");
            response.put("alert", "success");
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        }
    }

    @PUT
    public void editSet() {
        Map map = params1st();

        SetProducts set = SetProducts.findById(Long.valueOf(param("set_product_id")));
        
        Product item = Product.findById(Long.valueOf(param("product_id")));
        String typeCode = item.getString("type_code");

        Map<String, Object> response = new HashMap(3);
        
        set.set(
            "product_id", item.getLongId(),
            "amount", Double.valueOf(param("amount"))
        );

        

        if(!set.save()) {
            response.put("message", "error.products.set.edit");
            response.put("alert", "error");
        } else {
            if(!xhr()) {
                response.put("item", item);
                view(response);
                render("_forms/composites").noLayout();
            } else {
                Map itemMap = new HashMap(5);
                itemMap.put("set_product_id", set.getLongId());
                itemMap.put("product_id", item.getLongId());
                itemMap.put("amount", set.getDouble("amount"));
                itemMap.put("title", item.getString("title"));
                itemMap.put("artcode", item.getString("artcode"));
                itemMap.put("priceperunit", item.getDouble("priceperunit"));
                response.put("item", itemMap);
                
                response.put("message", "products.set.edit");
                response.put("alert", "success");
                respond(Json.fromMap(response)).contentType("application/json").status(OK);
            }
        }
    }

    @DELETE
    public void deleteSet() {
        SetProducts set = SetProducts.findById(id());
        Map<String, Object> response = new HashMap(2);
        if(set.delete()) {
            response.put("message", "products.set.delete");
            response.put("alert", "success");

             if(!xhr()) {
                flash(response);
                render("_forms/composites").noLayout();
            } else {
                respond(Json.fromMap(response)).contentType("application/json").status(OK);
            }
        } else {
            response.put("message", "error.products.set.delete");
            response.put("alert", "error");
        }
    }

    @PUT
    public void prices() {
        Product item = (Product)Product.findById(id());
        item.set(
            "cost", blank("cost") ? null : Double.parseDouble(param("cost")), 
            "pricepretax", blank("pricepretax") ? null : Double.parseDouble(param("pricepretax")),
            "pricewithtax", blank("pricewithtax") ? null : Double.parseDouble(param("pricewithtax")),
            "tax_id", blank("tax_id") ? null : Long.valueOf(param("tax_id")),
            "priceperunit", blank("priceperunit") ? null :  Double.parseDouble(param("priceperunit")),
            "unitname", blank("unitname") ? null : param("unitname")
        );

        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
            response.put("message", "Product prices were updated: " + item.get("title"));
            response.put("alert", "success");
        }

        
        if(!xhr()) {
            flash(response);
            redirect(ProductsController.class, "show", item.getLong("id"));
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }
}
