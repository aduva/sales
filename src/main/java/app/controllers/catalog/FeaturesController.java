package app.controllers;

import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.DELETE;
import org.javalite.activeweb.annotations.POST;
import org.javalite.activeweb.annotations.GET;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.*;
import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class FeaturesController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Group g = group();

        StringBuffer sb = new StringBuffer();
        sb.append("select * ")
          .append("from features a where (a.title ilike ? or a.url = ?) and a.group_id = ?")
          .append("order by a.id ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", q, g.getLongId(), pageSize, (page - 1) * pageSize);
        
        boolean hasNext = Attribute.count("title ilike ? and group_id = ?", "%"+q+"%", g.getLongId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    public void show() {
        Feature item = (Feature) Feature.findById(Long.valueOf(getId()));

        if(item != null){
            view("item", item);
            view("values", item.getAll(FeatureValue.class));
        }else{
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }
    }

    public void showValue() {
        FeatureValue item = (FeatureValue) FeatureValue.findById(id());

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        if(item != null) {
            response.put("item", item);
        } else {
            response.put("messages", "page not found");
        }

        view(response);

        if(xhr()) {
            if(item != null) {
                response.put("item", item.toJson(false));
            }
            
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }
    
    @POST
    public void create() {
        Feature item = new Feature();
        item.fromMap(params1st());
        item.set("user_id", userId(), "group_id", groupId());
        if(!item.save()) {
            flash("message", "Something went wrong, please  fill out all fields");
            flash("errors", item.errors());
            flash("params", params1st());
            redirect(FeaturesController.class, "new_form");
        }else{
            flash("message", "New feature was added: " + item.get("title"));
            redirect(FeaturesController.class);
        }
    }

    @POST
    public void addValue() {
        Feature item = (Feature) Feature.findById( Long.valueOf(param("feature_id")) );
        if(item != null) {
            FeatureValue val = new FeatureValue();
            val.set("title", param("title"));
            val.set("feature_id", item.get("id"));
            val.set("user_id", userId());
            if(!val.save()) {
                flash("message", "Something went wrong, please  fill out all fields");
                flash("errors", val.errors());
                flash("params", params1st());
                redirect(FeaturesController.class, "new_value_form");
            } else {
                flash("message", "New feature value was added: " + val.get("title"));
                redirect(FeaturesController.class, "show", item.get("id"));
            }
        }
    }

    @DELETE
    public void delete(){
        Feature item = (Feature) Feature.findById(Long.valueOf(getId()));
        String title = item.getString("title");
        item.delete(true);
        flash("message", "Feature: '" + title + "' was deleted");
        redirect(FeaturesController.class);
    }

    public void newForm() {}

    public void newValueForm() {
        Feature item = (Feature) Feature.findById(Long.valueOf(getId()));

        if(item != null){
            view("feature", item.get("id"));
        } else {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }
    }

    @DELETE
    public void deleteValue(){
        FeatureValue item = (FeatureValue) FeatureValue.findById(Long.valueOf(getId()));
        String title = item.getString("title");
        Long id = item.getLong("feature_id");
        item.delete();
        flash("message", "Feature Value: '" + title + "' was deleted");
        redirect(FeaturesController.class, "show", id);
    }

    public void featureValues() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        StringBuffer count = new StringBuffer();
        count
          .append("feature_values fv, features f");

        StringBuffer count2 = new StringBuffer();
        count2
          .append("f.id = fv.feature_id and f.group_id = ?");

        if(!blank("id")) {
            count2.append(" and f.id = ").append(param("id"));
        } else if(!blank("url")) {
            count2.append(" and f.url = \'").append(param("url")).append("\'");
        } else if(!blank("q")) {
            count2.append(" and f.title ilike \'%").append(param("q")).append("%\'");
        }

        StringBuffer select = new StringBuffer();
        select
            .append("select fv.* from ")
            .append(count).append(" where ").append(count2)
            .append(" order by fv.id ").append("limit ? offset ?");

        List<Map> itemsPage = Base.findAll(select.toString(), groupId(), pageSize, (page - 1) * pageSize);
        
        boolean hasNext = Base.count(count.toString(), count2.toString(), groupId()) > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        
        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(OK);
        } else{
            render("show_values");
        }
    }
}
