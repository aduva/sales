package app.controllers;

import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.DELETE;
import org.javalite.activeweb.annotations.GET;
import org.javalite.activeweb.annotations.POST;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.Json;
import java.util.*;

/**
 * @author Askhat Shakenov
 */
public class CategoriesController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
    }

    public void list() {
        try {
            if(params1st().get("page") != null)
                page = Integer.parseInt(params1st().get("page"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Paginator p = new Paginator(
            Category.class, pageSize, "group_id = ? and title like ?",
            group().getLongId(), "%"+q+"%").orderBy("title, id"
        );

        boolean hasNext = p.getCount() > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        response.put("items", p.getPage(page).toMaps());
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("total", p.getCount());
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    public void show() {
        Category item = (Category) Category.findById(id());

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        if(item != null) {
            response.put("item", item);
        } else {
            response.put("messages", "page not found");
        }

        if(xhr()) {
            if(item != null) {
                response.put("item", item.toJson(false));
            }
            
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        } else {
            view(response);
        }
    }

    public void single() {
        Category item = (Category) Category.findById(id());
        
        Map<String, Object> response = new HashMap(2);
        int status = 200;
        if(item == null) {
            response.put("alert", "error");
            response.put("message", "Category not found with provided id: ".concat(getId()));
            status = 400;
        } else {
            response.put("alert", "success");
            response.put("item", item.toJson(false));        
        }
        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }
    
    @POST
    public void create() {
        Category item = new Category();
        item.fromMap(params1st());
        item.set("user_id", userId(), "group_id", group().getLongId());

        if(!item.save()) {
            flash("message", "Something went wrong, please  fill out all fields");
            flash("errors", item.errors());
            flash("params", params1st());
            redirect(CategoriesController.class, "add");
        } else {
            flash("message", "New category was added: " + item.get("title"));
            redirect(CategoriesController.class);
        }
    }

    @DELETE
    public void delete() {
        Category item = (Category)Category.findById(Long.valueOf(getId()));
        String title = item.getString("title");
        item.delete();
        flash("message", "Category: '" + title + "' was deleted");
        redirect(CategoriesController.class);
    }

    public void add() {
        
    }
}
