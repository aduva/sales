package app.controllers;

import com.google.inject.Inject;
import org.javalite.activeweb.AppController;
import org.javalite.activeweb.annotations.*;
import org.javalite.activejdbc.*;
import app.models.*;
import app.utils.*;
import java.util.*;
import app.services.*;

/**
 * @author Askhat Shakenov
 */
public class GoodsController extends Controller {                
    public int page = 1;
    public int pageSize = 10;
    public String q = "";

    public void index() {
    }

    public void create() {
        render("index");
    }

    public void list() {
        System.out.println(">>>>>>>>>>>>>>>>> " + params1st() + "\n\n\n");
        try {
            if(params1st().get("pageIndex") != null)
                page = Integer.parseInt(params1st().get("pageIndex"));
            if(params1st().get("pageSize") != null)
                pageSize = Integer.parseInt(params1st().get("pageSize"));
        } catch(NumberFormatException e) {
            view("message", "are you trying to hack the URdddL?");
            render("/system/404");
        }

        if(params1st().get("q") != null)
            q = params1st().get("q");

        Group g = group();

        StringBuffer sb = new StringBuffer();
        sb.append("select vals.*, ids.*, p.id, p.title, p.artcode, p.pricewithtax, p.is_disabled, p.description, ")
            .append("c.title as category, c.id as category_id ")
            .append("from crosstab('")
            .append("select p.id, f.title, fv.title ")
            .append("from products p ")
            .append("join products_feature_values pfv on pfv.product_id = p.id ")
            .append("join feature_values fv on fv.id = pfv.feature_value_id ")
            .append("join features f on f.id = pfv.feature_id ")
            .append("where p.group_id = ").append(g.getLongId()).append(" ")
            .append("order by p.title, p.id, f.id")
            .append("') AS vals(product_id bigint, width varchar, color varchar), crosstab('")
            .append("select p.id, f.id, fv.id ")
            .append("from products p ")
            .append("join products_feature_values pfv on pfv.product_id = p.id ")
            .append("join feature_values fv on fv.id = pfv.feature_value_id ")
            .append("join features f on f.id = pfv.feature_id ")
            .append("where p.group_id = ").append(g.getLongId()).append(" ")
            .append("order by p.title, p.id, f.id")
            .append("') AS ids(product_id bigint, width_id bigint, color_id bigint), ")
            .append("products p, categories c, categories_products cp ")
            .append("where c.id = cp.category_id and p.id = cp.product_id ")
            .append("and vals.product_id = p.id and ids.product_id = p.id ");

        if(!blank("category_id")) {
            sb.append("and c.id = ").append(param("category_id")).append(" ");
        }

        if(!blank("weight_id")) {
            sb.append("and ids.weight_id = ").append(param("weight_id")).append(" ");
        }

        if(!blank("priceTo") && !blank("priceFrom")) {
            sb.append("and p.pricewithtax >= ")
                .append(param("priceFrom"))
                .append(" and p.pricewithtax <= ")
                .append(param("priceTo")).append(" ");
        }

        sb.append("and (p.artcode ilike ? or p.title ilike ? or c.title ilike ?) and p.group_id = ? ")
          .append("order by p.title, p.id, c.id ")
          .append("limit ? offset ?");
        List<Map> itemsPage = Base.findAll(sb.toString(), "%"+q+"%", "%"+q+"%", "%"+q+"%", g.getLongId(), pageSize, (page - 1) * pageSize);

        StringBuffer countsb = new StringBuffer();
        countsb
            .append("c.id = cp.category_id and p.id = cp.product_id and p.group_id = ? ");
        if(!blank("category_id"))
            countsb.append("and c.id = ").append(param("category_id"));

        long count = Base.count("products p, categories c, categories_products cp", countsb.toString(), g.getLongId());
        boolean hasNext = count > (page - 1) * pageSize + pageSize;

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        response.put("items", itemsPage);
        response.put("page", page);
        response.put("hasNext", hasNext);
        response.put("total", count);
        response.put("pageSize", pageSize);
        view(response);

        if(xhr()) {
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        }
    }

    public void show() {
        Product item = (Product) Product.findById(id());

        Map<String, Object> response = new HashMap<String, Object>();
        int status = 200;

        if(item != null) {
            ProductsFeatureValues v = ProductsFeatureValues.first(
                "product_id = ?", item.getLongId()
            );
            FeatureValue w = FeatureValue.findById(v.getLong("feature_value_id"));
            response.put("weight", w.getString("title"));

            response.put("item", item);

        } else {
            response.put("messages", "page not found");
        }
        System.out.println(">>>>>>>>>>>>> " + response);
        view(response);

        if(xhr()) {
            if(item != null) {
                response.put("item", item.toJson(false));
            }
            
            respond(Json.fromMap(response)).contentType("application/json").status(status);
        } else {
            render("index");
        }
    }

    @GET
    public void add() {
        render("show");
    }

    @POST
    public void save() {

        Tax tax = Tax.first("is_default = true and group_id = ?", groupId());
        Double pricewithtax = Double.valueOf(param("pricewithtax"));
        Double pricepretax = getPricePreTax(pricewithtax, tax);

        Product item = new Product();
        item.set(
            "title", param("title"), 
            "artcode", param("artcode"), 
            "type_code", "DEFAULT", 
            "user_id", userId(), 
            "group_id", groupId(),
            "pricepretax", pricepretax,
            "pricewithtax", pricewithtax,
            "priceperunit", pricewithtax,
            "tax_id", tax.getLongId(),
            "cost", null, 
            "unitname", null,
            "is_disabled", false
        );

        Map<String, Object> response = new HashMap(4);
        int status = OK;

        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = BAD;
            if(!xhr()) {
                flash(response);
                redirect(GoodsController.class, "add");
                return;
            }
        } else {

            response.put("alert", "success");
            response.put("message", "New food was added: " + item.get("title"));
            response.put("item", item.toMap());

            FeatureValue w = FeatureValue.findById(Long.valueOf(param("weight_id")));
            ProductsFeatureValues.createIt(
                "product_id", item.getLongId(),
                "feature_value_id", w.getLongId(),
                "feature_id", w.getLong("feature_id")
            );
            
            if(!xhr()) {
                flash(response);
                view("item", item);
                redirect(GoodsController.class, "show", item.getLongId());
                return;
            }
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    private Double getPricePreTax(Double pricewithtax, Tax t) {
        if(t != null) {
            return Math.rint(pricewithtax * 100 / (100 + t.getInteger("rate")));
        }

        return pricewithtax;
    }

    @PUT
    public void update() {

        Product item = (Product)Product.findById(id());

        Tax tax = null;
        if(item.getLong("tax_id") == null) {
            tax = Tax.first("is_default = true and group_id = ?", groupId());
        } else {
            tax = Tax.findById(item.getLong("tax_id"));
        }


        Double pricewithtax = Double.valueOf(param("pricewithtax"));
        Double pricepretax = getPricePreTax(pricewithtax, tax);

        item.set(
            "title", param("title"), 
            "category_id", Long.valueOf(param("category_id")),
            "pricepretax", pricepretax,
            "pricewithtax", pricewithtax,
            "priceperunit", pricewithtax
        );
        

        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Пожалуйста, заполните все необходимые поля.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
            FeatureValue w = FeatureValue.findById(Long.valueOf(param("weight_id")));
            ProductsFeatureValues v = ProductsFeatureValues.first(
                "product_id = ? and feature_id = ?", item.getLongId(), w.getLong("feature_id")
            );

            if(v != null) {
                v.set("feature_value_id", w.getLongId());
                v.save();
            }

            response.put("message", "Product was updated: " + item.get("title"));
            response.put("alert", "success");
            response.put("item", item.toMap());
        }
        
        if(!xhr()) {
            flash(response);
            redirect(ProductsController.class, "show", item.getLong("id"));
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    @PUT
    public void toggleDisable() {

        Product item = (Product)Product.findById(id());
        if(item.getBoolean("is_disabled") == null) {
            item.set("is_disabled", true);
        } else {
            item.set("is_disabled", !item.getBoolean("is_disabled"));
        }
        


        Map<String, Object> response = new HashMap(4);
        int status = 200;
        if(!item.save()) {
            response.put("message", "Упс, что-то пошло не так.");
            response.put("errors", item.errors());
            response.put("params", params1st());
            response.put("alert", "danger");
            status = 400;
        } else {
            response.put("message", "Product was updated: " + item.get("title"));
            response.put("alert", "success");
            response.put("item", item.toMap());
        }
        
        if(!xhr()) {
            flash(response);
            redirect(ProductsController.class, "show", item.getLong("id"));
        }

        respond(Json.fromMap(response)).contentType("application/json").status(status);
    }

    protected String getLayout() {
        return "goods/layout";
    }
}
