package app.filters;

import org.javalite.activeweb.AbstractControllerConfig;
import org.javalite.activeweb.AppContext;
import org.javalite.activeweb.controller_filters.HttpSupportFilter;
import java.util.*;
import app.services.SecurityService;
import app.services.Subject;
import com.google.inject.Inject;
import app.models.*;

public class AuthenticatedFilter extends HttpSupportFilter {

    public void before() {
    	Subject subject = subject();

    	if(subject == null || !subject.isAuthenticated()) {
            session("redirectPath", path());
        	redirect(app.controllers.AuthenticationController.class, "login");
        }
    }

    // public Subject subject() {
    //     String username = null;
    //     try {
    //         username = cookieValue("last_login");
    //     } catch(NullPointerException e) {
    //         return null;
    //     }
    //     return username == null ? null : subject(username);
    // }

    public Subject subject(String username) {
        return (Subject) session(username.concat(":").concat(app.controllers.Controller.SESSION_KEY_SUBJECT));
    }

    public Subject subject() {
        return (Subject) session(app.controllers.Controller.SESSION_KEY_SUBJECT);
    }
}