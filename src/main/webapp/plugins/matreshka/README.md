# Matreshka v1.3.2

[![Matreshka Website](http://matreshka.io/img/mk5-logo_full-vert.svg)](http://matreshka.io)

[![Build Status](https://travis-ci.org/matreshkajs/matreshka.svg)](https://travis-ci.org/matreshkajs/matreshka)[![devDependency Status](https://david-dm.org/matreshkajs/matreshka/dev-status.svg)](https://david-dm.org/matreshkajs/matreshka#info=devDependencies)[![Issue Stats](http://issuestats.com/github/matreshkajs/matreshka/badge/pr)](http://issuestats.com/github/matreshkajs/matreshka)[![Issue Stats](http://issuestats.com/github/matreshkajs/matreshka/badge/issue)](http://issuestats.com/github/matreshkajs/matreshka)

[![Join the chat at https://gitter.im/finom/matreshka](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/finom/matreshka)

### [Download](https://github.com/finom/matreshka/releases)



```html
<script src="matreshka.min.js"></script>
<input type="text" class="my-input">
<script>
var app = new Matreshka();
app.bindNode('x', '.my-input');
app.x = 'Two-way data binding in JS? O rly?';
</script>
```

Matreshka is small and powerful client-side JavaScript framework that allows you to build single page applications as simply as possible.

* Two-way data-bindings in JavaScript files.
* It's simple. Really. You don't need to learn mass of articles to get started.
* Custom architecture. You can choose any way how you build an application.
* [It's fast!](http://mathieuancelin.github.io/js-repaint-perfs/matreshka/index.html)

#### Sponsoring by [Shooju](http://shooju.com)

-----------------------------------

## Resources
- [Vote for new features](https://trello.com/b/E5KcQESk/matreshka-js-features)
- [The website](http://matreshka.io)
- [Release History](https://github.com/matreshkajs/matreshka/releases)
- [JSDoc files for IDE](https://github.com/matreshkajs/matreshka.io/tree/master/en/jsdoc)
- [TodoMVC](https://github.com/matreshkajs/matreshka_todomvc)
- [Issues](https://github.com/matreshkajs/matreshka/issues)
- [Forum](http://matreshka.io/forum)
- [Twitter](https://twitter.com/matreshkajs)
- [Donate some Bitcoins](https://www.coinbase.com/finom)

**License:** [MIT License](https://raw.github.com/finom/matreshka/master/LICENSE)
