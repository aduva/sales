/*
 This file is a collection of unobtrusive JS that binds to link_to generated anchors typical for Ajax calls.

 editor: Askhat Shakenov
 */

$(document).ready(function() {
    $('a[data-link]').bind('click', function() {
        var anchor = $(this);

        var opts = {
            destination: anchor.attr("data-destination"),
            formId: anchor.attr("data-form"),
            href: anchor.attr("href"),
            _method: anchor.attr("data-method"),
            before: anchor.attr("data-before"),
            after: anchor.attr("data-after"),
            beforeArg: anchor.attr("data-before-arg"),
            afterArg: anchor.attr("data-after-arg"),
            error: anchor.attr("data-error"),
            confirmMessage: anchor.attr("data-confirm")
        }

        aw(opts);
    });
});

function aw(opts) {
    if(opts.confirmMessage != null ){
        if(!confirm(opts.confirmMessage)){
            return false;
        }
    }

    //not Ajax
    if(opts.destination == null && opts.before == null && opts.after == null && (opts._method == null || opts._method.toLowerCase() == "get")){
        return true;
    }

    if (opts._method == null) {
        opts._method = "get";
    }
    var type;
    if (opts._method.toLowerCase() == "get") {
        type = "get";
    } else if (opts._method.toLowerCase() == "post"
            || opts._method.toLowerCase() == "put"
            || opts._method.toLowerCase() == "delete") {
        type = "post";
    }

    var data = "opts._method=" + opts._method;
    if (opts.formId != null) {
        data += "&" + $("#" + opts.formId).serialize();
    }

    if(opts.before != null){
        eval(opts.before)(opts.beforeArg);
    }


    $.ajax({ url: opts.href, data: data, type:type,
        success: function(data) {
            if (opts.after != null)
                eval(opts.after)(opts.afterArg, data);

            if (opts.destination != null)
                $("#" + opts.destination).html(data);
        },
        error: function(xhr, status, errorThrown) {
            if(opts.error != null){
                eval(opts.error)(xhr.status, xhr.responseText );
            }
        }
    });

    return false;
}