var ProductForms = Class({
	initInfoForm: function(data) {
		console.log(forms);
	},
	initPricesForm: function(data) {
		this.pricesForm = new PricesForm(data);
	},
	initCategoriesForm: function(data) {
		this.categoriesForm = new CategoriesForm(data);
	},
	forms: {}
});

ProductForms.set('forms.CategoriesForm', Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.set('id', data.id)
			.set('category', {id: data.category.id, title: data.category.title})
			.bindNode({
				sandbox: '#form-categories'
			}).bindNode('category', ':sandbox .category-title', {
				setValue: function(v) {
					this.innerHTML = v.title;
				}
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('change:category', function() {
				console.log('this.title = this.category.title;');
			}).linkProps('isValid', 'category id', function( category, id ) {
				return category !== undefined && id !== undefined;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			})
		;
	},
	submitForm: function() {
		$.ajax({url: '/products/category', data: { category_id: this.category.id, id: this.id }, method: 'put',
			success: function(data) {
				$('body').pgNotification({
                    style: 'bar',
                    message: data.message,
                    position: 'top',
                    timeout: 3000,
                    type: 'success'
                }).show();
			},
			error: function(data) {
				$('body').pgNotification({
                    style: 'bar',
                    message: data.message,
                    position: 'top',
                    timeout: 3000,
                    type: 'error'
                }).show();
			}
		});
		return this;
	}
});


ProductForms.forms.PricesForm = Class({
	'extends': MK.Object,
	constructor: function(data) {
		
		this.jset(data)
			.bindNode({
				sandbox: '#form-prices',
				cost: ':sandbox input#cost',
				pricepretax: ':sandbox input#pricePreTax',
				pricewithtax: ':sandbox input#priceWithTax',
				priceperunit: ':sandbox input#pricePerUnit',
				unitname: ':sandbox input#unitName'
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('change:pricepretax', function() {
				if(this.taxRate !== undefined)
					this.set('pricewithtax', (parseFloat(this.pricepretax) + parseFloat(this.pricepretax) * parseFloat(this.taxRate.rate) / 100).toFixed(2), {silent: true, forceHTML: true});
				else
					this.set('pricewithtax', parseFloat(this.pricepretax), {silent: true, forceHTML: true});

			}).on('change:pricewithtax', function() {
				if(this.taxRate !== undefined) {
					var p = parseFloat(parseFloat(this.taxRate.rate) / 100);
						p += 1;
					this.set('pricepretax', (parseFloat(this.pricewithtax) / p).toFixed(2), {silent: true, forceHTML: true});
				} else {
					this.set('pricepretax', parseFloat(this.pricewithtax), {silent: true, forceHTML: true});
				}

			}).on('change:taxRate', function() {
				if(this.pricePreTax !== undefined) {
					this.set('pricewithtax', (parseFloat(this.pricepretax) + parseFloat(this.pricepretax) * parseFloat(this.taxRate.rate) / 100).toFixed(2), {silent: true, forceHTML: true});
				} else if(this.priceWithTax !== undefined) {
					var p = parseFloat(parseFloat(this.taxRate.rate) / 100);
						p += 1;
					this.set('pricepretax', (parseFloat(this.pricewithtax) / p).toFixed(2), {silent: true, forceHTML: true});
				}

			}).linkProps('isValid', 'cost pricepretax pricewithtax taxRate', function( cost, pricepretax, pricewithtax, taxRate ) {
				return cost && pricepretax && pricewithtax && taxRate;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			})
		;
	},
	submitForm: function() {
		if(this.isValid) {
			this.jset('tax_id', this.taxRate.id);
		}

		$.ajax({url: '/products/prices', data: this.toJSON(), method: 'put',
			success: function(data) {
				$('body').pgNotification({
                    style: 'bar',
                    message: data.message,
                    position: 'top',
                    timeout: 3000,
                    type: 'success'
                }).show();
			},
			error: function(data) {
				$('body').pgNotification({
                    style: 'bar',
                    message: data.message,
                    position: 'top',
                    timeout: 3000,
                    type: 'error'
                }).show();
			}
		});
		return this;
	}
});
ProductForms.forms.InfoForm = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-info',
				title: ':sandbox input#title',
				artcode: ':sandbox input#artcode',
				type_code: ':sandbox input[name=type_code]'
			}).linkProps('isValid', 'title artcode', function( title, artcode ) {
				return title.length >= 4 && artcode.length >= 5;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			}).linkProps('isComposite', 'type_code', function(type_code) {
				return type_code === 'COMPOSITE';
			}).bindNode('isComposite', ':sandbox #composite-control', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				// console.log(cadd);
				this.submitForm();
			}).on('change:isComposite', function() {
				if(this.isComposite && infoForm.composites === undefined) {
					console.log('initing composites');
					infoForm.composites = new Composites(data);
				}
			})
		;
	},
	submitForm: function() {
		if(this.isValid){
			this.jset('composites', this.composites);
			this.jset('compositesLength', this.composites === undefined ? 0 : this.composites.length);
		}

		var method = $(this.sandbox).attr('method');

		$.ajax({url: $(this.sandbox).attr('action'), data: this.toJSON(), method: method,
			success: function(data) {
				$('body').pgNotification({
                    style: 'bar',
                    message: data.message,
                    position: 'top',
                    timeout: 3000,
                    type: 'success'
                }).show();
				console.log(data);
                if(method === 'post') {
                	$('#currentBreadcrumb').attr('href', '/products/show/'+data.id);
                	$('#currentBreadcrumb').html(data.title);
                	$('a[href=#savefirst]').each(function() {
                		console.log('ti', $(this).attr('data-href'));
                		$(this).attr('href', $(this).attr('data-href'));
                		$(this).tab('show');
                	});
                	$(document).attr('title', 'Sales - Товар: ' + data.title);
                	window.history.pushState({state: {'controller': 'products', 'action': 'show', 'id': data.id}}, "", '/products/show/' + data.id);
                }
			},
			error: function(data) {
				$('body').pgNotification({
                    style: 'bar',
                    message: data.message,
                    position: 'top',
                    timeout: 3000,
                    type: 'error'
                }).show();
			}
		});

		return this;
	}
});

ProductForms.forms.Composite = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						qty: ':sandbox .qty',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					})
				;
			})
		;
	}
});

ProductForms.forms.Composites = Class({
	'extends': MK.Array,
	itemRenderer: '#composite-template',
	Model: Composite,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#composite-control',
				table: ':sandbox #composite-table',
				cadd: ':sandbox #composite-add',
				qty: ':sandbox #composite-qty',
				container: ':sandbox tbody'
			}).on( '@readytodie', function( composite ) {
				this.pull( composite );
			}).on('click::cadd', function(e) {
				e.preventDefault();
				
				for( var i = 0; i < this.length; i++ ) {
					if( this[i].id === this.data.id ) {
						this[i].qty = this.qty;
						return;
					}
				}

				this.push({
					id: this.data.id,
					title: this.data.title,
					qty: this.qty
				});
			}).linkProps('isEmpty', 'length', function(length) {
				return length <= 0;
			}).bindNode('isEmpty', ':sandbox #alertrow', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			})
			.recreate(data);
		;
	}
});