<div class="header">
  <!-- START MOBILE CONTROLS -->
  <div class="container-fluid relative">
    <!-- LEFT SIDE -->
    <div class="pull-left full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="header-inner">
        <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
          <span class="icon-set menu-hambuger"></span>
        </a>
      </div>
      <!-- END ACTION BAR -->
    </div>
    <div class="pull-center hidden-md hidden-lg">
      <div class="header-inner">
        <div class="brand inline">
          <img src="/img/logo.png" alt="logo" data-src="/img/logo.png" data-src-retina="/img/logo_2x.png" width="78" height="22">
        </div>
      </div>
    </div>
    <!-- RIGHT SIDE -->
    <div class="pull-right full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="header-inner">
        <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
          <span class="icon-set menu-hambuger-plus"></span>
        </a>
      </div>
      <!-- END ACTION BAR -->
    </div>
  </div>
  <!-- END MOBILE CONTROLS -->
  
  <!-- <div class=" pull-right">
    <div class="header-inner">
      <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
    </div>
  </div> -->
  <div class=" pull-right">
    <!-- START User Info-->
    <div class="visible-lg visible-md m-t-10">
      <div class="pull-right">
        <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
          <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            <span class="semi-bold no-style">Askhat Shakenov</span>
          </div>
          <span class="thumbnail-wrapper d32 circular inline m-t-5">
            <img src="${context_path}/img/profiles/avatar.jpg" alt="" data-src="${context_path}/img/profiles/avatar.jpg" data-src-retina="${context_path}/img/profiles/avatar_small2x.jpg" width="32" height="32"></span>
        </a>
      </div>
    </div>
  </div>
  <div class="hidden-xs hidden-sm header-search">
    <i class="hint-text pg-search"></i>
    <input id="product-search" class="transparent " placeholder="Введите данные для поиска..." autocomplete="off" spellcheck="false">
  </div>
</div>