/* Sidebar */
.sidebar {
  text-align: left;
  background: #2c2d38;
  z-index: 1000;
  position: fixed;
  width: 25em;
  max-width: 100%;
  height: 100%;
  height: 100vh;
  top: 0;
  left: 0;
  padding: 2em 1em;
}

.sidebar h1 {
  margin: 0.75em 0 0;
  font-size: 2em;
  line-height: 1;
  font-family: "Playfair Display", serif;
}

.sidebar h1 span {
  display: block;
}

/* Menu button for toggling the sidebar (appears on small screen) */
.menu-toggle {
  position: fixed;
  z-index: 100;
  display: block;
  width: 25px;
  height: 25px;
  top: 14px;
  left: 14px;
  cursor: pointer;
  background: none;
  border: none;
  display: none;
  margin: -2px 15px 0 0;
}

.menu-toggle span {
  position: absolute;
  top: 50%;
  left: 0;
  display: block;
  width: 100%;
  height: 2px;
  background-color: #81c483;
  font-size: 0px;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.menu-toggle span:before,
.menu-toggle span:after {
  position: absolute;
  left: 0;
  width: 100%;
  height: 100%;
  background: #81c483;
  content: '';
}

.menu-toggle span:before {
  -webkit-transform: translateY(-7px);
  transform: translateY(-7px);
}

.menu-toggle span:after {
  -webkit-transform: translateY(7px);
  transform: translateY(7px);
}

/* close button (for content and sidebar) */
.close-button {
  position: absolute;
  border: none;
  background: none;
  margin: 0;
  z-index: 100;
  top: 0;
  right: 0;
  font-size: 18px;
  color: #ddd;
  cursor: pointer;
  pointer-events: none;
  padding: 20px 30px;
  opacity: 0;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-transition: opacity 0.3s;
  transition: opacity 0.3s;
}

.close-button span {
  display: none;
}

.close-button:hover {
  color: #7b7b7b;
}

/* show class for content close button */
.close-button--show {
  opacity: 1;
  pointer-events: auto;
}

/* small screen changes for sidebar (it becomes an off-canvas menu) */
@media screen and (max-width: 599px) {
  .sidebar {
    -webkit-transform: translate3d(-100%, 0, 0);
    transform: translate3d(-100%, 0, 0);
    -webkit-transition: -webkit-transform 0.3s;
    transition: -webkit-transform 0.3s;
  }

  .sidebar.sidebar--open {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }

  .sidebar.sidebar--open ~ .main {
    pointer-events: none;
  }

  .top-bar {
    padding: 22px 15px 10px 60px;
  }

  .menu-toggle {
    display: inline-block;
  }

  .sidebar .close-button {
    opacity: 1;
    pointer-events: auto;
  }
}