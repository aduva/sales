<@content for="sidebar">
<nav class="page-sidebar bg-master-lighter b-r b-grey" data-pages="sidebar">
  <div id="client-info" class="panel hidden bg-primary no-border no-margin">
    <div class="panel-heading">
      <div class="pull-left">
        <span class="icon-thumbnail bg-master-light pull-left text-master clientname-short"></span>
        <div class="pull-left">
          <p class="hint-text all-caps font-sf no-margin overflow-ellipsis phone"></p>
          <h5 class="no-margin overflow-ellipsis text-master-light clientname"></h5>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-controls">
        <ul>
          <li><a href="#" class="portlet-close client-change text-master-light"><i class="portlet-icon portlet-icon-close text-master-light"></i></a>
          </li>
        </ul>
      </div>
    </div>
    <div class="clearfix"></div>
      
    <div class="p-l-20 p-r-20 p-t-5 p-b-10 ">
      <p class="pull-left no-margin hint-text">Бонусов 0</p>
      <div class="clearfix"></div>
    </div>
  </div>
  <div id="order-client-search" class="b-b b-grey full-width padding-25">
    <div class="input-group transparent no-border full-width">
      <input id="client-search" class="no-border product-search bg-transparent" placeholder="Имя или номер клиента" autocomplete="off" spellcheck="false">
    </div>
  </div>

  <div class="order-products panel no-margin">
    <div class="p-l-20 p-r-20 p-t-5 p-b-0 b-b b-grey">
      <div class="row no-padding">
        <div class="col-sm-5">
          <h2 class="text-success no-margin">сумма</h2>
          <p class="no-margin">
            <input class="form-control transparent p-l-0 hint-text" type="text" placeholder="Ввести промо">
          </p>
        </div>
        <h3 class="pull-right col-sm-7 text-right semi-bold"><span class="order-sum kzt">0</span></h3>
        
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="auto-overflow products-cart">
      <table class="table table-condensed">
        <tbody></tbody>
      </table>
    </div>
    <div class="padding-5 full-width pull-bottom">
       <a href="#" class="btn btn-lg btn-block btn-compose btn-primary fs-16 font-sf all-caps text-white finish-btn disabled">Оплатить <span class="order-sum kzt">0</span></a>
    </div>
  </div>
  
</nav>
</@content>

<@content for="header">
<div class="header">
  <!-- START MOBILE CONTROLS -->
  <div class="container-fluid relative">
    <!-- LEFT SIDE -->
    <div class="pull-left full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="header-inner">
        <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
          <span class="icon-set menu-hambuger"></span>
        </a>
      </div>
      <!-- END ACTION BAR -->
    </div>
    <div class="pull-center hidden-md hidden-lg">
      <div class="header-inner">
        <div class="brand inline">
          <img src="/img/logo.png" alt="logo" data-src="/img/logo.png" data-src-retina="/img/logo_2x.png" width="78" height="22">
        </div>
      </div>
    </div>
    <!-- RIGHT SIDE -->
    <div class="pull-right full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="header-inner">
        <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
          <span class="icon-set menu-hambuger-plus"></span>
        </a>
      </div>
      <!-- END ACTION BAR -->
    </div>
  </div>
  <!-- END MOBILE CONTROLS -->
  
  <!-- <div class=" pull-right">
    <div class="header-inner">
      <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
    </div>
  </div> -->
  <div class=" pull-right">
    <!-- START User Info-->
    <div class="visible-lg visible-md m-t-10">
      <div class="pull-right">
        <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
          <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            <span class="semi-bold no-style">Askhat Shakenov</span>
          </div>
          <span class="thumbnail-wrapper d32 circular inline m-t-5">
            <img src="${context_path}/img/profiles/avatar.jpg" alt="" data-src="${context_path}/img/profiles/avatar.jpg" data-src-retina="${context_path}/img/profiles/avatar_small2x.jpg" width="32" height="32"></span>
        </a>
      </div>
    </div>
  </div>
  <div class="hidden-xs hidden-sm header-search">
    <i class="hint-text pg-search"></i>
    <input id="product-search" class="transparent " placeholder="Введите данные для поиска..." autocomplete="off" spellcheck="false">
  </div>
</div>
</@content>


<!-- CATALOG -->
<div class="flexbox grid" id="products-list">
  <div class="no-result hidden">
    <h1>Ничего не найдено</h1>
  </div>
  <div class="clearfix"></div>
</div>

<!-- <script type="text/html" id="cart-item-template">
<div class="row p-t-20 p-b-20 bg-white m-t-5 m-b-5 m-l-5 m-r-5 b-b b-t b-r b-l b-rad-5 b-grey">

  <div class="col-sm-5">
    <h5 class="no-margin bold p-l-10 title"></h5>
    <p class="font-sf p-l-10 artcode"></p>
  </div>

  <div class="col-sm-3">
    <h5 class="no-margin p-l-10 pricewithtax"></h5>
    <p class="hint-text font-sf p-l-10 discount hidden"></p>
  </div>

  <div class="col-sm-3 b-l b-grey b-dashed">
    <h5 class="no-margin p-l-10 sum"></h5>
  </div>
  <div class="clearfix"></div>
</div>
</script> -->

<script type="text/html" id="cart-item-template">
<tr class="cart-item">
  <td class="font-sf fs-12 col-sm-6">
    <div class="pull-left">
      <div class="amount-input-area bg-success p-t-5 p-b-5 m-r-5">
        <input type="number" value="1" class="form-control semi-bold transparent amount amount-input no-margin" placeholder="Введите количество">
      </div>x
    </div>
    <div class="p-l-5 all-caps"><span class="title bold"></span><br>
    <span class="hint-text small artcode"></span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center fs-12 col-sm-3 b-r b-grey b-dashed">
    <span class="pricewithtax kzt fs-14 bold"></span><br>
    <span class="hint-text small discount hidden"></span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt"></span>
  </td>
 </tr>
</script>

<script type="text/html" id="client-template">
<div class="client">
  <div class="thumbnail-wrapper d32 circular bg-success text-white inline m-t-10">
    <div>
      <img width="32" height="32" src="/img/profiles/avatar.jpg" data-src="/img/profiles/avatar.jpg" data-src-retina="/img/profiles/avatar2x.jpg" alt="">
    </div>
  </div>
  <div class="p-l-10 inline p-t-5">
    <p class="m-b-5 clientname semi-bold"></p>
    <p class="hint-text phone"></p>
  </div>
</div>
</script>

<!-- <script type="text/html" id="item-template">
  <div class="product">
    <div class="product__info">
      <span class="product__meta color highlight">2009</span>
      <span class="product__meta width highlight">St-Julien, Bordeaux</span>
      
      <span class="product__title inline pull-left title all-caps"></span>
      <span class="product__meta inline artcode highlight"></span>
      <span class="product__price highlight inline pull-right m-t-5 pricewithtax kzt">$99.90</span>
      
    </div>
    <label class="action action--add"><i class="fa fa-shopping-cart"></i><i class="fa fa-check"></i></label>
  </div>
</script> -->

<script type="text/html" id="item-template">
<div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
  <div class="p-l-10 p-r-10 p-t-5 p-b-5">
    <div class="pull-left text-left">
      <h5 class="no-margin overflow-ellipsis title"></h5>
      <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
    </div>
    <h5 class="pull-right text-master pricewithtax kzt"></h5>
    <div class="clearfix"></div>
  </div>
  <div class="p-l-10 p-r-10 p-t-0 p-b-0">
    <div class="row">
      <div class="col-md-6 text-left">
        <p class="hint-text all-caps font-sf small no-margin color"></p>
        <p class="font-sf small no-margin width"></p>
      </div>
      
      <div class="col-md-6 text-right">
        <p class="small font-sf no-margin">Ост.: 556 м.</p>
      </div>
    </div>
  </div>
</div>
</script>
<script type="text/html" id="category-template">
  <li><a href="#" class="all-caps font-sf fs-11 active title"></a></li>
</script>

<@content for="styles">
<@render partial="component" />
</@content>

<@content for="js">
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>

<script>
<@render partial="controljs" />
$(document).on('ready', function() {
  app.init(); 
});
</script>
</@content>