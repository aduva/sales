<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Sales - Дэшборд</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="${context_path}/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="${context_path}/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="${context_path}/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="${context_path}/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="${context_path}/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="${context_path}/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="${context_path}/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="${context_path}/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="${context_path}/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="${context_path}/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="${context_path}/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="${context_path}/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="${context_path}/plugins/jquery-menuclipper/jquery.menuclipper.css" rel="stylesheet" type="text/css" />
    
    <link class="main-stylesheet" href="${context_path}/pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="${context_path}/css/main.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
    <link href="${context_path}/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
    <![endif]-->
    <style>
        <@yield to="styles"/>
            
    </style>
  </head>
  <body class="fixed-header">
    <@yield to="sidebar"/>

    <div class="page-container ">
      <@yield to="header"/>
    
      <div class="page-content-wrapper full-height">
        <div class="content full-height">
          <div class="container-fluid full-height container-fixed-lg padding-10">
            ${page_content}
          </div>
        </div>
      </div>
    </div>
    <@yield to="printarea"/>

    <!--START QUICKVIEW -->
    <div id="quickview" class="quickview-wrapper" data-pages="quickview">
     
      
      <!-- Tab panes -->

          <div class="view-port clearfix" id="chat">
            <div class="view bg-white">
              <!-- BEGIN View Header !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <!-- BEGIN Header Controler !-->
                  
                  <!-- END Header Controler !-->
                  <div class="view-heading">
                    Chat List
                    <div class="fs-11">Show All</div>
                  </div>
                  <!-- BEGIN Header Controler !-->
                  <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
                  <!-- END Header Controler !-->
                </div>
              </div>
              <!-- END View Header !-->
              <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                <div class="list-view-group-container">
                  <div class="list-view-group-header text-uppercase">
                    Пользователи</div>
                  <ul>
                    <!-- BEGIN Chat User List Item  !-->
                    <li class="chat-user-list clearfix">
                      <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class="" href="#">
                        <span class="col-xs-height col-middle">
                        <span class="thumbnail-wrapper d32 circular bg-success">
                            <img width="34" height="34" alt="" data-src-retina="${context_path}/img/profiles/1x.jpg" data-src="${context_path}/img/profiles/1.jpg" src="${context_path}/img/profiles/1x.jpg" class="col-top">
                        </span>
                        </span>
                        <p class="p-l-10 col-xs-height col-middle col-xs-12">
                          <span class="text-master">Аva Фlores</span>
                          <span class="block text-master hint-text fs-12">Hello there</span>
                        </p>
                      </a>
                    </li>
                    <!-- END Chat User List Item  !-->
                  </ul>
                </div>
                
              </div>
            </div>
            <!-- BEGIN Conversation View  !-->
            <div class="view chat-view bg-white clearfix">
              <!-- BEGIN Header  !-->
              <div class="navbar navbar-default">
                <div class="navbar-inner">
                  <a href="javascript:;" class="link text-master inline action p-l-10 p-r-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                    <i class="pg-arrow_left"></i>
                  </a>
                  <div class="view-heading">
                    John Smith
                    <div class="fs-11 hint-text">Последний вход 5 мин. назад</div>
                  </div>
                    
                </div>
              </div>
              <!-- END Header  !-->
              
              <div class=" bg-white clearfix p-l-10 p-r-10">
                <div class="row">
                  <div class="col-xs-1 p-t-15">
                    <span class="link"><i class="fa fa-key"></i></span>
                  </div>
                  <div class="col-xs-10 p-l-10">
                    <input type="text" class="form-control chat-input" placeholder="Введите пароль">
                  </div>
                </div>
              </div>
              <!-- END Chat Input  !-->
            </div>
            <!-- END Conversation View  !-->
          </div>
        </div>
      </div>
    </div>
    <!-- END QUICKVIEW-->
    <!-- START OVERLAY -->
    <div class="overlay hide padding-25" id="client-search-overlay">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-0">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close text-black fs-16">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid p-l-5 p-r-5">
          <!-- BEGIN Overlay Controls !-->
          <input id="overlay-search" class="no-border product-search bg-transparent" placeholder="" autocomplete="off" spellcheck="false">
          <br>
          <div class="inline-block m-l-10">
            <i><span class="fs-13 client-create-text">Нажмите Enter для создания нового клиента</span></i>
          </div>
          <div class="inline-block m-l-10">
            <i><span class="fs-13 type-more-text">Введите <span class="type-count-text">3 символа</span></span></i>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- BEGIN Overlay Search Results, This part is for demo purpose, you can add anything you like !-->
        <div class="container-fluid">
          <div class="search-results m-t-10">
            <p class="bold">Клиенты</p>
            <div class="grid" id="clients-list">
              <div class="no-result hidden">
                <h3>Ничего не найдено</h3>
              </div>
            </div>
          </div>
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>

    <div class="overlay hide p-t-60" id="client-create-overlay">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-0">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close cancel text-black fs-16">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid full-height">
          <!-- BEGIN Overlay Controls !-->
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="panel panel-transparent">
                <div class="panel-heading"><div class="panel-title font-sf">Добавить клиента</div></div>
                <div class="panel-body">
                  <form id="form-project" role="form" autocomplete="off">
                    <p class="font-sf">Основная информация</p>
                    <div class="form-group-attached">
                      <div class="row clearfix">
                        <div class="col-sm-3">
                          <div class="form-group form-group-default required">
                            <label class="font-sf label-lg">Код</label>
                            <input type="hidden" class="form-control input-lg" name="phoneCode" id="phoneCode" required>
                          </div>
                        </div>
                        <div class="col-sm-9">
                          <div class="form-group form-group-default">
                            <label class="font-sf label-lg">Номер телефона</label>
                            <input type="text" class="form-control input-lg" name="phone" id="phoneInput" required>
                          </div>
                        </div>
                      </div>
                      <div class="row clearfix">
                        <div class="col-sm-6">
                          <div class="form-group form-group-default required">
                            <label class="font-sf label-lg">Имя</label>
                            <input type="text" class="form-control input-lg" name="first_name" id="first_name" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group form-group-default">
                            <label class="font-sf label-lg">Фамилия</label>
                            <input type="text" class="form-control input-lg" name="last_name" id="last_name">
                          </div>
                        </div>
                      </div>
                      <div class="form-group form-group-default">
                        <label class="font-sf label-lg">День рождения</label>
                        <input type="text" class="form-control input-lg" name="birthday" id="birthday" placeholder="дд/мм/гг">
                      </div>
                    </div>
                    <div class="row m-t-15">
                      <div class="col-sm-6">
                        <a href="#" class="btn btn-lg btn-block btn-primary submit">Добавить</a>
                      </div>
                      <div class="col-sm-6">
                        <a href="#" class="btn btn-lg btn-block btn-default cancel">Отмена</a>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>

    <div class="overlay hide" id="order-finish-overlay">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-0">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close overlayClose cancel text-black fs-16  hidden">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid full-height">
          <!-- BEGIN Overlay Controls !-->
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="padding-30 m-t-50">
                <i id="icon" class="fa fa-spinner fa-pulse fa-4x fa-fw hint-text"></i>
                <h2 id="message">Подождите, идет обработка данных...</h2>
                <p id="sub-message"></p>
                <h5><span class="clientname semi-bold">Асхат Шакенов</span>&nbsp;<span class="phone">+77077803676</span><br>Сумма: <span class="order-sum kzt">65250</span></h5>
                <button id="done-btn" type="button" class="btn btn-lg btn-primary btn-cons btn-rounded overlayClose m-t-30 hidden"><i class="fa fa-check"></i> Готово</button>
              </div>
            </div>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>

    <div class="overlay hide" id="order-payment-overlay">
      <!-- BEGIN Overlay Content !-->
      <div class="overlay-content has-results m-t-0">
        <!-- BEGIN Overlay Header !-->
        <div class="container-fluid">
          <!-- BEGIN Overlay Logo !-->
          
          <!-- END Overlay Logo !-->
          <!-- BEGIN Overlay Close !-->
          <a href="#" class="close-icon-light overlay-close overlayClose cancel text-black fs-16  hidden">
            <i class="pg-close"></i>
          </a>
          <!-- END Overlay Close !-->
        </div>
        <!-- END Overlay Header !-->
        <div class="container-fluid full-height">
          <!-- BEGIN Overlay Controls !-->
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="padding-30 m-t-50">
                <i id="icon" class="fa fa-spinner fa-pulse fa-4x fa-fw hint-text"></i>
                <h2 id="message">Подождите, идет обработка данных...</h2>
                <p id="sub-message"></p>
                <h5>
                  <span class="clientname semi-bold">Асхат Шакенов</span>&nbsp;<span class="phone">+77077803676</span>
                  <br>Сумма: <span class="order-sum kzt">65250</span>
                  <br>Наличные: <span class="cash kzt">65250</span>
                  <br>Сдача: <span class="change kzt">65250</span>
                </h5>
                <button id="done-btn" type="button" class="btn btn-lg btn-primary btn-cons btn-rounded overlayClose m-t-30 hidden"><i class="fa fa-check"></i> Готово</button>
                <button id="print-btn" type="button" class="btn btn-lg btn-default btn-cons btn-rounded overlayClose m-t-30 hidden"><i class="fa fa-print"></i> Распечатать чек</button>
              </div>
            </div>
          </div>
          <!-- END Overlay Controls !-->
        </div>
        <!-- END Overlay Search Results !-->
      </div>
      <!-- END Overlay Content !-->
    </div>
    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    <script src="${context_path}/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="${context_path}/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="${context_path}/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="${context_path}/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="${context_path}/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="${context_path}/plugins/classie/classie.js"></script>
    <script type="text/javascript" src="${context_path}/plugins/moment/moment-with-locales.min.js"></script>
    <script src="${context_path}/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="${context_path}/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>

    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="${context_path}/pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="${context_path}/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <@yield to="js"/>
  </body>
</html>