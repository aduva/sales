<@content for="sidebar">
<nav class="page-sidebar bg-master-lighter b-r b-grey" data-pages="sidebar" id="orders-list">

  <div id="order-client-search" class="b-b b-grey full-width padding-5 p-l-25">
    <div class="input-group transparent no-border full-width">
      <input id="input-search" class="no-border text-master product-search bg-transparent" placeholder="Имя или номер клиента" autocomplete="off" spellcheck="false">
    </div>
  </div>

  <div class="order-products panel no-margin">
    <div class="auto-overflow orders-items">
      <ul class="nav nav-tabs nav-tabs-linetriangle bg-master-lightest hidden" role="tablist" data-init-reponsive-tabs="collapse">
        <li class="active"><a href="#tab2FollowUs" class="fs-10 small font-sf" data-toggle="tab" role="tab">Не оплаченные</a>
        </li>
        <li><a href="#tab2Inspire" class="fs-10 small font-sf" data-toggle="tab" role="tab">Оплаченные</a>
        </li>
        <li><a href="#tab2Inspire" class="fs-10 small font-sf" data-toggle="tab" role="tab">Закрытые</a>
        </li>
      </ul>
      <table class="table table-condensed">
        <div class="no-result padding-10 hidden noResult">
          <h3 class="text-master">Ничего не найдено</h3>
        </div>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
  
</nav>
</@content>

<@content for="header">
<div class="header">
  <!-- START MOBILE CONTROLS -->
  <div class="container-fluid relative">
    <!-- LEFT SIDE -->
    <div class="pull-left full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="header-inner">
        <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
          <span class="icon-set menu-hambuger"></span>
        </a>
      </div>
      <!-- END ACTION BAR -->
    </div>
    <div class="pull-center hidden-md hidden-lg">
      <div class="header-inner">
        <div class="brand inline">
          <img src="/img/logo.png" alt="logo" data-src="/img/logo.png" data-src-retina="/img/logo_2x.png" width="78" height="22">
        </div>
      </div>
    </div>
    <!-- RIGHT SIDE -->
    <div class="pull-right full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="header-inner">
        <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
          <span class="icon-set menu-hambuger-plus"></span>
        </a>
      </div>
      <!-- END ACTION BAR -->
    </div>
  </div>
  <!-- END MOBILE CONTROLS -->
  
  <!-- <div class=" pull-right">
    <div class="header-inner">
      <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
    </div>
  </div> -->
  <div class=" pull-right">
    <!-- START User Info-->
    <div class="visible-lg visible-md m-t-10">
      <div class="pull-right">
        <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
          <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            <span class="semi-bold no-style">Askhat Shakenov</span>
          </div>
          <span class="thumbnail-wrapper d32 circular inline m-t-5">
            <img src="${context_path}/img/profiles/avatar.jpg" alt="" data-src="${context_path}/img/profiles/avatar.jpg" data-src-retina="${context_path}/img/profiles/avatar_small2x.jpg" width="32" height="32"></span>
        </a>
      </div>
    </div>
  </div>
</div>
</@content>

<div class="panel" id="order-info">
  <div class="panel-body full-height">

  <!-- START BODY -->
    <div class="no-result padding-10 full-height noResult">
      <h1 class="text-master">Ничего не выбрано</h1>
    </div>
    <div class="row hidden">
      <div class="col-md-6 b-r b-dashed b-grey ">
        <div class="padding-5 m-t-0">
          <div class="m-b-10 row">
            <div class="col-sm-8">
              <span class="icon-thumbnail bg-master-light pull-left text-master clientname-short">A</span>
              <div class="pull-left">
                <p class=" all-caps font-sf no-margin overflow-ellipsis phone">+77077803676</p>
                <p class="no-margin fs-16 overflow-ellipsis bold clientname">Askhat</p>
                <p class="pull-left no-margin">Бонусов 0</p>
              </div>
            </div>
            <div class="col-sm-4 text-right">
              <a href="#" class="no-margin">@<span class="username"></span></a>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="auto-overflow products-cart">
            <table class="table table-condensed m-t-10">
              <tbody class="products-cart-container"></tbody>
            </table>
          </div>
          
          <div class="container-sm-height">
            <div class="row row-sm-height b-a b-grey">
              <div class="col-sm-12 text-right bg-primary col-sm-height col-middle padding-10">
                <h5 class="font-sf all-caps small no-margin hint-text text-white bold">Итого</h5>
                <h4 class="no-margin text-white sum order-sum kzt">0</h4>
              </div>
            </div>
          </div>

        </div>
      </div>
      <div class="col-md-6">
        <div class="padding-10 payment-info hidden">
          <div class="bg-master-lighter padding-30 b-rad-lg">
            <h3><i class="fa fa-check-circle-o"></i> Оплата принята</h3>
          </div>
        </div>
        <div class="padding-10 payment-info">
          <ul class="list-unstyled list-inline m-l-30">
            <li><a href="#" class="p-r-30 text-black">Наличные</a></li>
            <li><a href="#" class="p-r-30 text-black  hint-text">Терминал</a></li>
          </ul>
          <form role="form">
            <div class="bg-master-lighter padding-30 b-rad-lg">
              <h2 class="pull-left no-margin">Наличные</h2>
              
              <div class="clearfix"></div>
              <div class="form-group form-group-default required m-t-25 m-b-25">
                <label class="font-sf label-lg">Вносимая сумма</label>
                <input type="number" id="cash-input" autofocus class="form-control cash input-lg" placeholder="Введите сумму денег от покупателя" required>
              </div>
              
              <h3>Сдача: <span class="change kzt">0</span></h3>
              <br>  
              <button id="finish-btn" class="btn btn-primary btn-lg btn-block m-t-20 all-caps font-sf">Принять оплату</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  <!-- END BODY -->
  </div>
</div>

<script type="text/html" id="order-item-template">
<tr class="">
  <td class="font-sf fs-12 col-sm-6">
    <div class="all-caps"><span class="title bold">Вискоза</span><br>
    <span class="hint-text small artcode">VISK1</span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
    <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
  </td>
</tr>
</script>

<script type="text/html" id="order-template">
<tr class="order cart-item">
  <td class="font-sf fs-12 col-sm-6">
    <div class="all-caps"><span class="clientname bold">Askhat Shakenov</span><br>
    <span class="hint-text small phone">+77077803676</span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center fs-12 col-sm-3">
    <span class="label label-warning small fs-10 status">НЕ ОПЛАЧЕН</span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt">12564</span>
  </td>
 </tr>
</script>



<@content for="printarea">
<div class="no-padding no-margin pull-center hidden" id="print-area">
  <h3 class="no-padding text-center">Ерке Сылқым</h3>
  <p class="no-margin text-center">Ақтөбе, А. Молдағұлова к-сі 44<br>тел.: 41-88-47</p>
  <div class="auto-overflow products-cart">
    <table class="table table-condensed m-t-10">
      <tbody class="products-cart-container"></tbody>
    </table>
  </div>
</div>
</@content>

<@content for="styles">
<@render partial="component" />
</@content>

<@content for="js">
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>

<script>
<@render partial="cashierjs" />
$(document).on('ready', function() {
  moment.locale('ru');
  app.init(); 
});
</script>
</@content>