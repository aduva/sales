<nav class="page-sidebar bg-master-lighter b-r b-grey" data-pages="sidebar">
  <div id="client-info" class="panel hidden bg-primary no-border no-margin">
    <div class="panel-heading">
      <div class="pull-left">
        <span class="icon-thumbnail bg-master-light pull-left text-master clientname-short"></span>
        <div class="pull-left">
          <p class="hint-text all-caps font-sf no-margin overflow-ellipsis phone"></p>
          <h5 class="no-margin overflow-ellipsis text-master-light clientname"></h5>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-controls">
        <ul>
          <li><a href="#" class="portlet-close client-change text-master-light"><i class="portlet-icon portlet-icon-close text-master-light"></i></a>
          </li>
        </ul>
      </div>
    </div>
    <div class="clearfix"></div>
      
    <div class="p-l-20 p-r-20 p-t-5 p-b-10 ">
      <p class="pull-left no-margin hint-text">Бонусов 0</p>
      <div class="clearfix"></div>
    </div>
  </div>
  <div id="order-client-search" class="b-b b-grey full-width padding-25">
    <div class="input-group transparent no-border full-width">
      <input id="client-search" class="no-border product-search bg-transparent" placeholder="Имя или номер клиента" autocomplete="off" spellcheck="false">
    </div>
  </div>

  <div class="order-products panel no-margin">
    <div class="p-l-20 p-r-20 p-t-5 p-b-0 b-b b-grey">
      <div class="row no-padding">
        <div class="col-sm-5">
          <h2 class="text-success no-margin">сумма</h2>
          <p class="no-margin">
            <input class="form-control transparent p-l-0 hint-text" type="text" placeholder="Ввести промо">
          </p>
        </div>
        <h3 class="pull-right col-sm-7 text-right semi-bold"><span class="order-sum kzt">0</span></h3>
        
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="auto-overflow products-cart">
      <table class="table table-condensed">
        <tbody></tbody>
      </table>
    </div>
    <div class="padding-5 full-width pull-bottom">
       <a href="#" class="btn btn-lg btn-block btn-compose btn-primary fs-16 font-sf all-caps text-white finish-btn disabled">Оплатить <span class="order-sum kzt">0</span></a>
    </div>
  </div>
  
</nav>