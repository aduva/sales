<@content for="sidebar">
    <#include "../layouts/sidebar.ftl">
</@content>

<@content for="header">
    <#include "../layouts/header.ftl">
</@content>

<div class="panel full-height no-margin no-padding" id="order-info">
  <div class="panel-body no-margin full-height no-padding">
      
    <div class="split-view">
      
      <div class="split-sidebar">
        
        <div class="tab-content full-width full-height no-padding">
          <div class="tab-pane slide-left active full-height" id="cart-pane">
            <div class="panel no-border full-height no-margin widget-loader-circle todolist-widget">
              <div class="panel-body full-height">
                <div class="no-padding">
                  <div class="">
                    <span class="icon-thumbnail bg-master-light pull-left text-master">A</span>
                    <div class="pull-left">
                      <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis ">+77077803676</p>
                      <h5 class="no-margin overflow-ellipsis ">Askhat Shakenov</h5>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="m-t-5">
                    <p class="hint-text fade small pull-left">Бонусов 0</p>
                    <a href="#" class="pull-right text-master"><i class="fa fa-long-arrow-left"></i></a>
                    <div class="clearfix"></div>
                  </div>
                  <hr class="m-t-0">
                </div>

                <div class="auto-overflow products-cart">
                  <table class="table table-condensed">
                    <tbody>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <div class="input-amount-td inline">
                            <span class="label label-success">HELLO</span>
                          </div>
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                      <tr class="">
                        <td class="font-sf fs-12 col-sm-6">
                          <div class="all-caps"><span class="title bold">Вискоза</span><br>
                          <span class="hint-text small artcode">VISK1</span>
                          </div>
                        </td>
                        <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
                          <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
                        </td>
                        <td class="col-sm-3 p-l-0 p-r-0 text-center">
                          <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane slide-right full-height" id="select-client-pane">
            <div class="panel no-border  no-margin widget-loader-circle todolist-widget">
              <div class="panel-heading">
                <div class="panel-title">
                  <span class="font-sf fs-11 all-caps">Выбор клиента <i class="fa fa-chevron-right"></i>
                  </span>
                </div>
                <div class="panel-controls">
                  <ul>
                    <li>
                      <a href="#" class="portlet-refresh text-black" data-toggle="refresh"><i class="portlet-icon portlet-icon-refresh"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="panel-body">
               
                <div class="input-group transparent no-border">
                  <span class="input-group-addon no-border fs-16">
                    <i class="fa fa-search"></i>
                  </span>
                  <input id="client-search" class="no-border product-search bg-transparent" placeholder="Имя или номер клиента" autocomplete="off" spellcheck="false">
                </div>
                <p class="no-margin small">Еще 3 символа...</p>
                <p class="no-margin small">Нажмите Enter чтобы добавить нового клиента</p>
                <hr>

                <div class="client m-t-10 p-t-5 p-b-5 b-b b-grey full-width">
                  <div class="">
                    <span class="icon-thumbnail bg-master-light pull-left text-master">A</span>
                    <div class="pull-left">
                      <p class="hint-text all-caps font-fs  small no-margin overflow-ellipsis ">+77077803676</p>
                      <h5 class="no-margin overflow-ellipsis ">Askhat Shakenov</h5>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>

                <div class="client m-t-10 p-t-5 b-b b-grey full-width">
                  <div class="">
                    <span class="icon-thumbnail bg-master-light pull-left text-master">A</span>
                    <div class="pull-left">
                      <p class="hint-text all-caps font-fs  small no-margin overflow-ellipsis ">+77077803676</p>
                      <h5 class="no-margin overflow-ellipsis ">Askhat Shakenov</h5>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>

                <div class="client m-t-10 p-t-5 b-b b-grey full-width">
                  <div class="">
                    <span class="icon-thumbnail bg-master-light pull-left text-master">A</span>
                    <div class="pull-left">
                      <p class="hint-text all-caps font-fs  small no-margin overflow-ellipsis ">+77077803676</p>
                      <h5 class="no-margin overflow-ellipsis ">Askhat Shakenov</h5>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>

                <a href="#cart-pane" data-toggle="tab" class="btn btn-block font-sf all-cap m-t-30">Отмена</a>
              </div>
            </div>

          </div>
          <div class="tab-pane"></div>
          <div class="tab-pane"></div>
        </div>
      </div>

      <div class="split-grid flexbox">
        
        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<script type="text/html" id="order-item-template">
<tr class="">
  <td class="font-sf fs-12 col-sm-6">
    <div class="all-caps"><span class="title bold">Вискоза</span><br>
    <span class="hint-text small artcode">VISK1</span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
    <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
  </td>
</tr>
</script>

<script type="text/html" id="order-template">
<tr class="order cart-item">
  <td class="font-sf fs-12 col-sm-6">
    <div class="all-caps"><span class="clientname bold">Askhat Shakenov</span><br>
    <span class="hint-text small phone">+77077803676</span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center fs-12 col-sm-3">
    <span class="label label-warning small fs-10 status">НЕ ОПЛАЧЕН</span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt">12564</span>
  </td>
 </tr>
</script>



<@content for="printarea">
<div class="no-padding no-margin pull-center hidden" id="print-area">
  <h3 class="no-padding text-center">Ерке Сылқым</h3>
  <p class="no-margin text-center">Ақтөбе, А. Молдағұлова к-сі 44<br>тел.: 41-88-47</p>
  <div class="auto-overflow products-cart">
    <table class="table table-condensed m-t-10">
      <tbody class="products-cart-container"></tbody>
    </table>
  </div>
</div>
</@content>


<@content for="styles">
<@render partial="split" />
</@content>
<@content for="js">
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>
<script src="${context_path}/plugins/jquery-menuclipper/jquery.menuclipper.js"></script>

<script src="${context_path}/pages/js/pages.min.js"></script>
<script src="${context_path}/pages/js/pages.email.js" type="text/javascript"></script>

<script>
<@render partial="controljs" />
$(document).on('ready', function() {
  $('#long-arrow').on('click', function(e) {
    $('.cart').toggleClass('slideLeft');
    $('.clients').toggleClass('hidden');
  });
  


});
</script>
</@content>