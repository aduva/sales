body {
	/*background-color: #eef0f0;*/
}

#print-area {
	/*visibility: hidden;*/
	position: absolute;
	top:0;
	left:0;
	padding: 0;
	background-color: #eee;
	z-index: 3000;
}

@media print {
	@page {
		margin: 0.25cm;
	}
	body * {
		visibility: hidden;
		padding: 0 !important;
		margin: 0;
	}
	#print-area * {
		visibility: visible;
	}
	#print-area {
		visibility: visible;
		position: absolute;
		top: 0;
		left: 0;
		padding: 0;
	}
}

.table tbody tr {
    border-top: 1px solid rgba(230, 230, 230, 0.7);
}

.table tbody tr:first-child {
	border-top: 0px;
}

.table tbody tr td {
	border-bottom: 0px;
	border-top: 0px;
}

.form-group-default.transparent, .input-group.transparent, .form-control.transparent,
.input-group.transparent .select2-container .select2-choice, input.transparent {
	border-color: transparent;
	background-color: transparent;
}

.order-products {
	height: calc(100% - 103px) !important;
}

.products-cart {
	height: calc(100% - 178px);
	height: -webkit-calc(100% - 178px);
}

.kzt:after {
    content: "\20B8"
}

.discount:after {
	content: "%";
}

.discount:before {
	content: "-";
}

.inline-block {
	display: inline-block;
}

.navbar-nav li a {
	color: #777;
}

.navbar-nav li.active {
	background-color: #eee;
}

.product-search {
	font-weight: 700;
    font-size: 27px;
    height: 60px;
    letter-spacing: -1.925px;
    line-height: 60px;
    width: 100%;
    padding-left: 0 !important;
}

#client-search.product-search {
	color: grey;
    font-size: 27px;
    height: 50px;
    line-height: 50px;
}

div.cs-skin-slide.cs-active, .select2-drop-active {
	z-index: 1011;
}

.header-search {
	padding-left: 10px;
	padding-top: 5px;
	margin-top: 10px;
	width: 300px;
	font-size: 18px;
}

.header-search input {
	font-size: 18px;
	width: 250px;
}

@media only screen and (min-width: 980px) {
	body.menu-pin .page-sidebar {
	    width: 350px;
	}

	.page-content-wrapper .content {
	    padding-left: 350px !important;
	    /*padding-top: 60 !important;*/
	}

	.header-search {
		margin-left: 350px;
	}
}

@media (max-width: 991px) {
	.page-sidebar {
		width: 350px;
	}

	body.sidebar-open .page-container {
	  -webkit-transform: translate3d(350px, 0, 0);
	  transform: translate3d(350px, 0, 0);
	  -ms-transform: translate(350px, 0);
	  overflow: hidden;
	  position: fixed;
	}
	body.sidebar-open .push-on-sidebar-open {
	  -webkit-transform: translate3d(350px, 0, 0);
	  transform: translate3d(350px, 0, 0);
	  -ms-transform: translate(350px, 0);
	  overflow: hidden;
	}
}

.cart-item {
	border-left: 4px solid #e6e6e6;
}

.cart-item.selected {
	border-left: 4px solid #6d5cae;
}

.cart-item .amount-input-area {
	height: 25px;
	display: inline;
	border-radius: 3px;
}

.amount-input {
	display: inline;
	color: white;
	width: 40px;
	font-size: 12px;
	line-height: 18px;
	height: 25px;
	padding: 0;
	text-align: center;
}

.amount-input::-webkit-outer-spin-button, .amount-input::-webkit-inner-spin-button {
	-webkit-appearance: none;
	-moz-appearance: textfield;
	margin: 0;
}

.alert-list a {
	width: 100% !important;
}

.list-view li:hover {
  background-color: #3b4752;
}

.menu-items {
	padding-bottom: 145px !important;
}

.list-view-group-header, .list-view-fake-header {
	background-color: #2b303b;
	color: #fff;
	opacity: .7;
}

#setclient {
	margin-top: auto;
}

.order-finish a.btn {
	color: #fff;
}


/* product grid */
.grid {
	margin: 0 auto;
	padding: 0px;
	/*max-width: 1200px;*/
	width: 100%;
	text-align: center;
	overflow: hidden;
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

/* if flexbox is supported, let's use it to lay out the products */
.flexbox .grid {
	display: -ms-flexbox;
	display: -webkit-flex;
	display: flex;
	-webkit-flex-direction: row;
	-ms-flex-direction: row;
	flex-direction: row;
	-webkit-flex-wrap: wrap;
	-ms-flex-wrap: wrap;
	flex-wrap: wrap;
	-webkit-justify-content: center;
	-ms-flex-pack: center;
	justify-content: center;
	-webkit-align-content: stretch;
	-ms-flex-line-pack: stretch;
	align-content: stretch;
	-webkit-align-items: flex-start;
	-ms-flex-align: start;
	align-items: flex-start;
}

.no-result h1, .no-result h3{
    opacity: 0.5;
 }

/* product */
.product {
	width: 200px;
	min-height: 110px;
	margin: 5px;
	transition: all 0.3 ease;
}

.client {
	width: 100%;
	cursor: pointer;
}

.client:hover {
	background-color: #f0f0f0;
}

.product__selected {
	border-color: #5C5EDC;
}

.flexbox .product {
	display: block;
	-webkit-flex: none;
	-ms-flex: none;
	flex: none;
}

/* product info */
.product__info > span {
	display: block;
	padding: 1px 0;
}

/* since we'll be using the product info inside of the comparison, we'll hide the extra info for the grid view */
.grid .extra {
	display: none;
}

.product__image {
	display: block;
	margin: 0 auto;
	max-width: 100%;
	-webkit-transform-origin: 50% 100%;
	transform-origin: 50% 100%;
}

.product:hover {
	-webkit-box-shadow: 0 4px 5px 0px rgba(0,0,0,.05);
    box-shadow: 0 4px 5px 0px rgba(0,0,0,.05);
}

.product:hover .pricewithtax {
	-webkit-animation: swing 0.6s forwards;	
	animation: swing 0.6s forwards;	
}

/* https://daneden.github.io/animate.css/ */
@-webkit-keyframes swing {
	25% {
		-webkit-transform: rotate3d(0, 0, 1, 6deg);
		transform: rotate3d(0, 0, 1, 6deg);
	}

	50% {
		-webkit-transform: rotate3d(0, 0, 1, -4deg);
		transform: rotate3d(0, 0, 1, -4deg);
	}

	75% {
		-webkit-transform: rotate3d(0, 0, 1, 2deg);
		transform: rotate3d(0, 0, 1, 2deg);
	}

	100% {
		-webkit-transform: rotate3d(0, 0, 1, 0deg);
		transform: rotate3d(0, 0, 1, 0deg);
	}
}

@keyframes swing {
  25% {
		-webkit-transform: rotate3d(0, 0, 1, 6deg);
		transform: rotate3d(0, 0, 1, 6deg);
	}

	50% {
		-webkit-transform: rotate3d(0, 0, 1, -4deg);
		transform: rotate3d(0, 0, 1, -4deg);
	}

	75% {
		-webkit-transform: rotate3d(0, 0, 1, 2deg);
		transform: rotate3d(0, 0, 1, 2deg);
	}

	100% {
		-webkit-transform: rotate3d(0, 0, 1, 0deg);
		transform: rotate3d(0, 0, 1, 0deg);
	}
}


.product:hover {
	cursor: pointer;
}

.action--remove:hover,
.action--add:hover {
	color: #5C5EDC;
}

.product .fa-check,
.product .fa-shopping-cart {
	display: none;
}

.product:hover .fa-shopping-cart, 
.product__selected .fa-check {
	display: block;
	color: #5C5EDC;
}

.product__selected .fa-shopping-cart,
.product:hover.product__selected .fa-shopping-cart {
	display: none;
}



@media screen and (max-width: 59.688em) {
	.grid {
		padding: 2em 0.5em;
		font-size: 85%;
	}

	.product {
		margin: 0 0.5em 1em;
		min-width: 20em;
	}

	.flexbox .product {
		-webkit-flex: 1 0 10em;
		-ms-flex: 1 0 10em;
		flex: 1 0 10em;
	}
}