<@content for="sidebar">
    <#include "../layouts/sidebar.ftl">
</@content>

<@content for="header">
    <#include "../layouts/header.ftl">
</@content>

<div class="panel full-height no-margin no-padding" id="order-info">
  <div class="panel-body no-margin full-height no-padding">
      
    <div class="split-view">
      
      <div class="split-sidebar">
        <ul class="nav nav-tabs nav-tabs-fillup hidden" data-init-reponsive-tabs="dropdownfx">
          <li class="active">
            <a data-toggle="tab" href="#cart-pane"><span>Home</span></a>
          </li>
          <li>
            <a data-toggle="tab" href="#select-client-pane"><span>Profile</span></a>
          </li>
          <li>
            <a data-toggle="tab" href="#slide3"><span>Messages</span></a>
          </li>
        </ul>
        <div class="tab-content full-width full-height no-padding">
          <div class="tab-pane slide-left full-height" id="cart-pane">
            <div class="cart full-width full-height">
              <div class="cart-header clearfix">
                <div class="circle">
                  <a data-toggle="tab" href="#select-client-pane"><i class="fa fa-long-arrow-left"></i></a>
                </div>
                <div class="user-pic">
                  <img alt="Profile Image" width="33" height="33" data-src-retina="${context_path}/img/profiles/3x.jpg" data-src="${context_path}/img/profiles/3.jpg" src="${context_path}/img/profiles/3.jpg">
                </div>
                <h5>Jeff Curtis, <span class="phone semi-bold"> +77077803676</span> 
                </h5>
                <h6>Бонусов 0</h6>
                
              </div>
              <div class="cart-content no-padding">
                <ul class="list-unstyled no-margin no-padding cart-items">
                  <li class="item m-b-10 clearfix row completed">
                    <div class="font-sf fs-12 col-sm-6 p-t-10 full-height">
                      <div class="pull-left">
                        <div class="amount-input-area bg-success p-t-5 p-b-5 m-r-5">
                          <input type="number" value="1" class="form-control semi-bold transparent amount amount-input no-margin" placeholder="Введите количество">
                        </div>x
                      </div>
                      <div class="inline m-l-5 all-caps">
                        <p class="title no-margin bold">Вискоза</p>
                        <p class="hint-text small no-margin artcode">ВИСК025</p>
                      </div>
                    </div>
                    <div class="font-sf p-l-0 p-r-0 text-center fs-12 col-sm-3 b-r b-grey b-dashed p-t-10 full-height">
                      <span class="pricewithtax kzt fs-14 bold">1250</span><br>
                      <span class="hint-text small discount hidden"></span>
                    </div>
                    <div class="col-sm-3 p-l-0 p-r-0 text-center p-t-10 full-height">
                      <span class="font-sf fs-14 text-primary bold sum kzt">12500</span>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="cart-footer no-margin clearfix">
                <div class="pull-left all-caps font-sf">Итого <span class="kzt sum">5,345</span></div>
                <div class="pull-right"><a href="#" class="all-caps font-sf">Продолжить <i class="fa fa-long-arrow-right"></i></a></div>
              </div>
            </div>
          </div>
          <div class="tab-pane slide-right full-height active" id="select-client-pane">
            <div class="cart full-width full-height">
              <div class="cart-header clearfix">
                <div class="circle">
                  <a data-toggle="tab" href="#cart-pane"><i class="fa fa-long-arrow-left"></i></a>
                </div>
                <div class="input-group transparent no-border">
                  <input id="client-search" class="no-border product-search bg-transparent" placeholder="Имя или номер клиента" autocomplete="off" spellcheck="false">
                </div>
              </div>
              <div class="cart-content no-padding">
              <p class=" italic">Hello my friend, want some coffee?</p>
                <ul class="list-unstyled no-margin no-padding cart-items">
                  <li class="item m-b-10 clearfix row completed padding-15">
                    <div class="user-pic">
                      <img alt="Profile Image" width="33" height="33" data-src-retina="${context_path}/img/profiles/3x.jpg" data-src="${context_path}/img/profiles/3.jpg" src="${context_path}/img/profiles/3.jpg">
                    </div>
                    <h5>Jeff Curtis, <span class="phone semi-bold"> +77077803676</span> 
                    </h5>
                    <h6>Бонусов 0</h6>
                  </li>
                </ul>
              </div>
              <div class="cart-footer no-margin clearfix">
                <div class="pull-left all-caps font-sf">Итого <span class="kzt sum">5,345</span></div>
                <div class="pull-right"><a href="#" class="all-caps font-sf">Продолжить <i class="fa fa-long-arrow-right"></i></a></div>
              </div>
            </div>
          </div>
          <div class="tab-pane"></div>
          <div class="tab-pane"></div>
        </div>
      </div>

      <div class="split-grid flexbox">
        
        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

        <div class="panel product m-r-5 m-t-5 m-b-5 bg-white padding-5 b-r b-l b-t b-b b-grey">
          <div class="p-l-10 p-r-10 p-t-5 p-b-5">
            <div class="pull-left text-left">
              <h5 class="no-margin overflow-ellipsis title"></h5>
              <p class="hint-text all-caps font-sf  small no-margin overflow-ellipsis artcode"></p>
            </div>
            <h5 class="pull-right text-master pricewithtax kzt"></h5>
            <div class="clearfix"></div>
          </div>
          <div class="p-l-10 p-r-10 p-t-0 p-b-0">
            <div class="row">
              <div class="col-md-6 text-left">
                <p class="hint-text all-caps font-sf small no-margin color"></p>
                <p class="font-sf small no-margin width"></p>
              </div>
              
              <div class="col-md-6 text-right">
                <p class="small font-sf no-margin">Ост.: 556 м.</p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>

<script type="text/html" id="order-item-template">
<tr class="">
  <td class="font-sf fs-12 col-sm-6">
    <div class="all-caps"><span class="title bold">Вискоза</span><br>
    <span class="hint-text small artcode">VISK1</span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center semi-bold fs-14 col-sm-3 b-r b-grey b-dashed">
    <span class="amount">1</span> x <span class="pricewithtax kzt">1258</span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt">1580</span>
  </td>
</tr>
</script>

<script type="text/html" id="order-template">
<tr class="order cart-item">
  <td class="font-sf fs-12 col-sm-6">
    <div class="all-caps"><span class="clientname bold">Askhat Shakenov</span><br>
    <span class="hint-text small phone">+77077803676</span>
    </div>
  </td>
  <td class="font-sf p-l-0 p-r-0 text-center fs-12 col-sm-3">
    <span class="label label-warning small fs-10 status">НЕ ОПЛАЧЕН</span>
  </td>
  <td class="col-sm-3 p-l-0 p-r-0 text-center">
    <span class="font-sf fs-14 text-primary bold sum kzt">12564</span>
  </td>
 </tr>
</script>



<@content for="printarea">
<div class="no-padding no-margin pull-center hidden" id="print-area">
  <h3 class="no-padding text-center">Ерке Сылқым</h3>
  <p class="no-margin text-center">Ақтөбе, А. Молдағұлова к-сі 44<br>тел.: 41-88-47</p>
  <div class="auto-overflow products-cart">
    <table class="table table-condensed m-t-10">
      <tbody class="products-cart-container"></tbody>
    </table>
  </div>
</div>
</@content>


<@content for="styles">
<@render partial="split" />
</@content>
<@content for="js">
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>
<script src="${context_path}/plugins/jquery-menuclipper/jquery.menuclipper.js"></script>

<script src="${context_path}/pages/js/pages.min.js"></script>
<script src="${context_path}/pages/js/pages.email.js" type="text/javascript"></script>

<script>
<@render partial="controljs" />
$(document).on('ready', function() {
  $('#long-arrow').on('click', function(e) {
    $('.cart').toggleClass('slideLeft');
    $('.clients').toggleClass('hidden');
  });
  


});
</script>
</@content>