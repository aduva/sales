.panel#order-info {
    border: 1px solid #eee;
}
.split-view .split-sidebar {
    width: 360px;
    height: 100%;
    float: left;
    border-right: 1px solid #eee;
}



.kzt:after {
    content: " \20B8"
}


#client-search.product-search {
    width: 100%;
    padding-left: 0 !important;
    font-weight: 300;
    letter-spacing: -0.925px;
    color: grey;
    font-size: 23px;
    height: 27px;
    line-height: 30px;
}


.split-view .split-grid {
    position: relative;
    overflow: auto;
    height: 100%

    margin: 0 auto;
    padding: 0px;
    /*max-width: 1200px;*/
    /*width: 100%;*/
    text-align: center;
    overflow: hidden;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

.split-view .split-grid.flexbox {
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-align-content: stretch;
    -ms-flex-line-pack: stretch;
    align-content: stretch;
    -webkit-align-items: flex-start;
    -ms-flex-align: start;
    align-items: flex-start;
}

.product {
    width: 200px;
    min-height: 110px;
    margin: 5px;
    transition: all 0.3 ease;
}

.client:hover {
    cursor: pointer;
    background-color: #eee;
    transition: all 0.3 ease;
}

.input-amount-td {
    transform: -100px 0 0;
    display: inline;
    width: 100px;
    padding: 0px !important;
    transition: all 0.3 ease;
}

tr:hover .input-amount-td {
    transform: 100px 0 0;
    width: 100px;
    /*padding: 0px !important;*/
    /*transition: all 0.3 ease;*/
}

.products-cart {
    height: calc(100% - 178px);
    height: -webkit-calc(100% - 178px);
}

.split-view .split-sidebar .split-header {
    /*float: left;*/
    width: 100%;
    min-height: 50px;
    overflow-y: auto;
    /*position: relative;*/
    -webkit-overflow-scrolling: touch;
    border-right: 1px solid #e6e6e6;
    -webkit-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

.split-view .split-sidebar .split-list {
    height: calc(100% - 60px);
}