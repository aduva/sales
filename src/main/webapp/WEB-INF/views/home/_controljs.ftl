window.app = {
	init: function() {
		console.log('init app');
		var _this = this;
		_this.loadProducts();
		// _this.initCategories();
		_this.initSelections();
		_this.initSearchClient();
		_this.initOrder();

    $("#birthday").mask("99/99/99");
    $("#phoneInput").mask("999 999 9999");
	},
	initOrder: function() {
		app.forms.order = new app.forms.Order();
	},
	// initCategories: function() {
	// 	$.ajax({ url: '/categories/list', method: 'get',
 //      success: function(data) {
 //        var items = data.items;
	// 			app.arrays.categories = new app.arrays.Categories(items);
 //      }
 //    });
	// },
	loadProducts: function() {
		$.ajax({ url: '/goods/list', data: app.filter, method: 'get',
      success: function(data) {
        var items = data.items;
        if(app.arrays.items === undefined) {
					app.arrays.items = new app.arrays.Items(items);
        } else {
					app.arrays.items.recreate(items);
        }
      }
    });
	},
	pullFromCart: function(item) {
		app.arrays.cartItems.each(function(i, index) {
			if(i.id === item.id) {
				app.arrays.cartItems.pull(index);
			}
		});
	},
	pushToCart: function(item) {
		if(app.arrays.cartItems === undefined)
			app.arrays.cartItems = new app.arrays.CartItems({sum: 0, arr:[]});

		var _item = {
			amount: 1,
			sum: item.pricewithtax,
			pricewithtax: item.pricewithtax,
			id: item.id,
			title: item.title,
			artcode: item.artcode
		};
		app.arrays.cartItems.push(_item);	
	},
	initSelections: function() {
		$.ajax({ dataType: 'json', url: "/js/phonecodes.json", 
			success: function(data) {
				app.selects.selectPhoneCode = $("#phoneCode").select2({
				  data: data,
				  formatSelection: function(item) {
				  	return item.value;
				  }
				});
			},
			error: function(mes) {
				console.log(mes);
			}
		});
	},
	loadClients: function(q) {
		$.ajax({ url: '/clients/list', data: {q: q, pageSize: 100}, method: 'get',
      success: function(data) {
        var items = data.items;
        if(app.arrays.clients === undefined) {
					app.arrays.clients = new app.arrays.Clients(items);
        } else {
					app.arrays.clients.recreate(items);
        }
      }
    });
	},
	selectClient: function(item) {
		if(app.forms.client === undefined)
			app.forms.client = new app.forms.Client(item);
		else {
			item.isEmpty = false;
			app.forms.client.jset(item);
		}

		app.arrays.clients.recreate();

		app.clientId = item.id;

		if(app.forms.clientSearch.$sandbox.is(':visible')) {
    	app.forms.clientSearch.$sandbox.fadeOut("fast").addClass("closed");

    	setTimeout(function() {
          app.forms.clientSearch.$nodes.input.focus();
          app.forms.clientSearch.$nodes.input.val('');
    			app.forms.clientSearch.$nodes.field.val('').blur();
      }.bind(app), 100);
    }

    app.$clientInfo.toggleClass('hidden');
    app.$clientSearchDiv.toggleClass('hidden');
	},
	initSearchClient: function() {
		app.$clientInfo = $('#client-info');
		app.$clientSearchDiv = $('#order-client-search');
		app.forms.clientSearch = new app.forms.ClientSearch();
		app.forms.clientCreate = new app.forms.ClientCreate();
	},
	createClient: function(item) {
		if(item.phone !== undefined) {
			item.phone = item.phone.trim();
			console.log(item);
			var l = item.phone.length;
			if(l > 10) {
				item.phoneCode = item.phone.substring(0, l - 10);
				item.phone = item.phone.substring(l - 10, l);
			} else {
				item.phoneCode = '+7';
			}
			

			item.first_name = null;
			
			console.log(item);
		} else if(item.first_name !== undefined) {
			item.phoneCode = '+7';
			item.phone = null;
		}

		app.selects.selectPhoneCode.select2('data', {value: item.phoneCode});
		
		item.phone2 = item.phone;
		item.last_name = null;
		item.birthday = null;

		if(app.forms.clientCreate === undefined)
			app.forms.clientCreate = new app.forms.ClientCreate(item);
		else
			app.forms.clientCreate.jset(item);

		app.forms.clientCreate.overlayOpen();
	},
	$orderSum: $('.order-sum'),
	filter: {},
	arrays: {},
	forms: {},
	models: {},
	selects: {}
};

app.forms.Order = Class({
	'extends': MK.Object,
	constructor: function() {
		var _this = this;

		_this.bindNode({
				sandbox: '#order-finish-overlay',
				search: '#product-search',
				closeBtn: ':sandbox .overlayClose',
				doneBtn: ':sandbox #done-btn',
				message: ':sandbox #message',
				icon: ':sandbox #icon',
				subMessage: ':sandbox #sub-message',
				finishBtn: '.finish-btn'
			}).on('keyup::search', function(e) {
				_this.filter.q = _this.$nodes.search.val();
				_this.loadProducts();
			}).on('click::finishBtn', function(e) {
		  	e.preventDefault();
		  	_this.finishOrder();
		  }).on('click::closeBtn', function(e) {
				e.preventDefault();
				_this.overlayClose();
				app.arrays.items.unselectAll();
			})
		;
	},
	finishOrder: function() {
		var _this = this;
		_this.overlayOpen();

		var data = {
			client_id: app.clientId,
			products: app.arrays.cartItems.toJSON(),
			products_length: app.arrays.cartItems.length,
			sum: app.arrays.cartItems.orderSum
		};

		$.ajax({ url: '/orders/save', data: data, method: 'post',
			success: function(data) {
				console.log('data', data);
				_this.$nodes.icon.removeClass('fa-spinner').removeClass('fa-pulse').removeClass('fa-fw').addClass('fa-check');
				_this.$nodes.doneBtn.removeClass('btn-warning').addClass('btn-primary');
				_this.$nodes.closeBtn.removeClass('hidden');
				_this.$nodes.message.text(data.message);
				_this.$nodes.subMessage.text(data.subMessage);
			},
			error: function(data) {
				_this.$nodes.icon.removeClass('fa-spinner').removeClass('fa-pulse').addClass('fa-times');
				_this.$nodes.doneBtn.removeClass('btn-primary').addClass('btn-warning');
				_this.$nodes.closeBtn.removeClass('hidden');
				_this.$nodes.message.text(data.message);
				_this.$nodes.subMessage.text(data.subMessage);
			}
		});
	},
	overlayClose: function() {
		var _this = this;
		_this.$sandbox.fadeOut("fast").addClass("closed");
		app.forms.client.emptyClient();
  	app.arrays.cartItems.recreate();

	},
	overlayOpen: function() {
		var _this = this;
		_this.$sandbox.fadeIn("fast").removeClass("hide");
	}
});

app.forms.ClientCreate = Class({
	'extends': MK.Object,
	constructor: function(data) {
		console.log(data);
		var _this = this;
		_this
			.jset(data)
			.bindNode({
				sandbox: '#client-create-overlay',
				phone: ':sandbox #phoneInput',
				phoneCode: ':sandbox #phoneCode',
				first_name: ':sandbox #first_name',
				last_name: ':sandbox #last_name',
				birthday: ':sandbox #birthday',
				submitBtn: ':sandbox form a.submit',
				cancelBtn: ':sandbox a.cancel'
			}).linkProps('isValid', 'phone2 first_name', function(phone2, first_name) {
				return phone2 !== undefined && phone2 != null && phone2.length == 10
						&& first_name !== undefined && first_name != null && first_name.length >= 2;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			}).onDebounce('click::submitBtn', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('keyup::phone', function(e) {
				this.phone2 = this.$nodes.phone.val().replace(/\s|_/g, '').trim();
			})
		;
	},
	submitForm: function() {
		var _this = this,
				item = this.toJSON();
		console.log(item, this.toNative(), this.first_name);
		$.ajax({ url: '/clients/save', data: item, method: 'post',
			success: function(data) {
				_this.overlayClose();
				app.selectClient(data.item);
			}
		});
	},
	overlayClose: function() {
		var _this = this;
		_this.$sandbox.fadeOut("fast").addClass("closed");
  	setTimeout(function() {
      _this.$sandbox.find('form').trigger('reset').blur();
    }, 100);
	},
	overlayOpen: function() {
		var _this = this;
		_this.$sandbox.fadeIn("fast").removeClass("hide");
		if(_this.phone === undefined || _this.phone == null) {
			_this.$nodes.phone.focus();
			return;
		}
		
		if(_this.first_name === undefined || _this.first_name == null) {
			_this.$nodes.first_name.focus();
			return;
		}
	}
});

app.forms.ClientSearch = Class({
	'extends': MK.Object,
	constructor: function(data) {
		var _this = this;
		_this.set('length', 0);
		_this
			.bindNode({
				sandbox: '#client-search-overlay',
				input: '#client-search',
				field: ':sandbox #overlay-search',
				typeMoreText: ':sandbox .type-more-text',
				clientCreateText: ':sandbox .client-create-text',
				typeCount: ':sandbox .type-count-text',
				closeBtn: ':sandbox .overlay-close'
			}).on('keyup::input', function(e) {
				if(e.keyCode != 27) {
	        var key = $(e.target).val();

	        _this.$sandbox.removeClass('hide');
	        _this.$sandbox.fadeIn(100);
	        
	        if (!_this.$nodes.field.is(':focus')) {
            _this.$nodes.field.val(key);
						// _this.length++;
            // typeCount();
            setTimeout(function() {
                _this.$nodes.field.focus();
                var tmpStr = _this.$nodes.field.val();
                _this.$nodes.field.val('');
                _this.$nodes.field.val(tmpStr);
            }, 10);

            if(!_this.$nodes.clientCreateText.is(':visible')) {
	            setTimeout(function() {
	            	_this.$nodes.clientCreateText.fadeIn('fast');
	            }, 3000);
	          }
	        }

	        _this.$sandbox.removeClass('closed');
	      }
			}).on('keyup::field', function(e) {
				if(_this.$sandbox.is(':visible') && e.which == 27) {
	      	_this.overlayClose();
	      } else if(_this.$sandbox.is(':visible') && e.which == 13) {
	      	console.log('enter', this.field, this.field.match(/^[0-9]+$/));
	      	if(this.field.match(/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/)
	      		|| this.field.match(/^[0-9]+$/)) {
	      		app.createClient({phone: this.field});
	      	} else {
	      		app.createClient({first_name: this.field});
	      	}

	      } else {
	      	if(_this.$nodes.field.val().length > 2)
	      		app.loadClients(_this.$nodes.field.val());
	      }
			}).on('keyup::field keyup::input', function(e) {
				_this.length = _this.field.length;
    		var symbol = ' символа';
	    	if(_this.length >= 3) {
	    		if(_this.$nodes.typeMoreText.is(':visible'))
	    			_this.$nodes.typeMoreText.fadeOut();
	    	} else {
	    		if(!_this.$nodes.typeMoreText.is(':visible'))
	    			_this.$nodes.typeMoreText.fadeIn();
	    	}

	    	if(_this.length == 2)
	    		symbol = ' символ';
	    	
	      _this.$nodes.typeMoreText.text('Введите еще ' + (3 - _this.length) + symbol);

			}).on('click::closeBtn', function(e) {
				e.preventDefault();
				_this.overlayClose();
			})
		;

		this.$nodes.clientCreateText.hide();
    this.$sandbox.width(app.$clientSearchDiv.width());

	},
	overlayClose: function() {
		var _this = this;
		_this.$sandbox.fadeOut("fast").addClass("closed");
  	setTimeout(function() {
      _this.$nodes.input.focus();
      _this.$nodes.input.val('');
			_this.$nodes.field.val('').blur();
    }, 100);
	}
})

app.forms.Client = Class({
	'extends': MK.Object,
	constructor: function(data) {
		var _this = this;
		data.isEmpty = false;
		_this.jset(data)
			.bindNode({
				sandbox: '#client-info',
				phone: '.phone',
				closeBtn: ':sandbox .client-change',
				clientname: '.clientname'
			}, {
				setValue: function(v) {
					this.innerHTML = v;
				}
			}).bindNode({
				clientname: ':sandbox .clientname-short'
			}, {
				setValue: function(v) {
					this.innerHTML = v.substring(0, 1);
				}
			}).on('click::closeBtn', function(e) {
				e.preventDefault();
				_this.emptyClient();
			}).linkProps('isValid', 'isEmpty', function(isEmpty) {
				return !isEmpty;
			}).bindNode('isValid', '.finish-btn', {
				setValue: function(v) {
					console.log('isEmpty', !v);
					$(this).toggleClass('disabled', !v || app.arrays.cartItems === undefined || app.arrays.cartItems.length == 0);
				}
			})
		;
	},
	emptyClient: function() {
		app.clientId = undefined;
		app.$clientSearchDiv.toggleClass('hidden');
		app.$clientInfo.toggleClass('hidden');
    app.forms.clientSearch.$nodes.input.focus();

		app.forms.client.jset('isEmpty', true);
	}
}); 

app.models.Client = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						clientname: ':sandbox .clientname',
						phone: ':sandbox .phone',
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::sandbox', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					})
				;
			})
		;
	}
});

app.arrays.Clients = Class({
	'extends': MK.Array,
	itemRenderer: '#client-template',
	Model: app.models.Client,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#clients-list',
				noResult: ':sandbox .no-result',
				container: ':sandbox'
			}).on( '@clicked', function( item ) {
				console.log('client clicked', item);
				app.selectClient(item.toObject());
			}).on('recreate', function(e) {
				this.$nodes.noResult.toggleClass('hidden', this.length > 0);
			})
			.recreate(data);
		;
	}
});

// app.models.Category = Class({
// 	'extends': MK.Object,
// 	constructor: function(data) {
// 		this.jset(data)
// 			.on('render', function() {
// 				this
// 					.bindNode({
// 						title: ':sandbox .title',
// 					}, {
// 						setValue: function(v) {
// 							this.innerHTML = v;
// 						}
// 					}).on('click::sandbox', function(e) {
// 						e.preventDefault();
// 						this.trigger('clicked', this);
// 					})
// 				;
// 			})
// 		;
// 	}
// });

// app.arrays.Categories = Class({
// 	'extends': MK.Array,
// 	itemRenderer: '#category-template',
// 	Model: app.models.Category,
// 	constructor: function(data) {
// 		data.unshift({id: null, title: 'Все'});
// 		this
// 			.bindNode({
// 				sandbox: '#categories-list',
// 				container: ':sandbox'
// 			}).on( '@clicked', function( item ) {
// 				this.$sandbox.find('li').removeClass('active');
// 				item.$sandbox.addClass('active');
// 				app.filter.category_id = item.id;
// 				app.loadProducts();
// 			})
// 			.recreate(data);
// 		;
// 		this[0].$sandbox.addClass('active');
// 	}
// });

app.models.Item = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						pricewithtax: ':sandbox .pricewithtax',
						artcode: ':sandbox .artcode',
						width: ':sandbox .width',
						color: ':sandbox .color',
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::sandbox', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					}).linkProps('isSelected', 'id', function(id) {
						if(app.arrays.cartItems === undefined)
							return false;

						var isSelected = false;
						app.arrays.cartItems.each(function(itm, idx) {
							if(itm.id == id) {
								isSelected = true;
								return;
							}
						});
						return isSelected;
					}).bindNode('isSelected', ':sandbox', {
						setValue: function(v) {
							$(this).toggleClass('product__selected', v);
						}
					})
				;
			})
		;
	}
});

app.arrays.Items = Class({
	'extends': MK.Array,
	itemRenderer: '#item-template',
	Model: app.models.Item,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#products-list',
				noResult: ':sandbox .no-result',
				container: ':sandbox'
			}).on( '@clicked', function( item ) {
				item.$sandbox.toggleClass('product__selected');
				if(!item.$sandbox.hasClass('product__selected')) {
					app.pullFromCart(item);
				} else {
					app.pushToCart(item);
				}
			}).on('recreate', function(e) {
				this.$nodes.noResult.toggleClass('hidden', this.length > 0);
			})
			.recreate(data)
		;
	},
	unselectAll: function() {
		$('.product').removeClass('product__selected');
	}
});

app.models.CartItem = Class({
	'extends': MK.Object,
	constructor: function(data) {
		var _this = this;
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						pricewithtax: ':sandbox .pricewithtax',
						artcode: ':sandbox .artcode',
						amount: ':sandbox .amount',
						discount: ':sandbox .discount',
						sum: ':sandbox .sum',
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).bindNode({
						overlayer: ':sandbox .amount-input-area'
					}).on('click::sandbox', function(e) {
						e.preventDefault();
						$('.cart-item').removeClass('selected');
						_this.$sandbox.addClass('selected');
						// $('.amount-input-area').removeClass('bg-primary').addClass('bg-success');
						// _this.$nodes.overlayer.removeClass('bg-success').addClass('bg-primary');
						_this.$nodes.amount.focus().select();

					}).on('keyup::amount', function() {
						this.sum = this.amount * this.pricewithtax;
						this.trigger('sumChanged');
					})
				;
			})
		;
	}
});

app.arrays.CartItems = Class({
	'extends': MK.Array,
	itemRenderer: '#cart-item-template',
	Model: app.models.CartItem,
	constructor: function(data) {
		var _this = this;
		_this.set('orderSum', 0)
			.bindNode({
				sandbox: '.products-cart',
				container: ':sandbox tbody'
			}).bindNode({
				orderSum: '.order-sum'
			}, {
				setValue: function(v) {
					this.innerHTML = v;
				}
			}).on( '@clicked', function( item ) {
				// app.itemSelected(item);
			}).on('push', function(e) {
				e.args.forEach(function(item, index) {
					_this.orderSum += item.sum;
					// app.arrays.products_ids.push(item.id);
				});
			}).on('remove', function(e) {
				e.removed.forEach(function(item, index) {
					_this.orderSum -= item.sum;
					// app.arrays.products_ids.pull(item.id);
				});
			}).on('@sumChanged', function() {
				var sum = 0;
				_this.forEach(function(item, index) {
					// console.log(item, index);
					sum += item.sum;
				});
				_this.set('orderSum', sum);
			}).linkProps('isValid', 'orderSum', function(orderSum) {
				return orderSum !== undefined && orderSum !== null && orderSum > 0;
			}).bindNode('isValid', '.finish-btn', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v || app.clientId === undefined);
				}
			})
			.recreate(data.arr)
		;
	}
});