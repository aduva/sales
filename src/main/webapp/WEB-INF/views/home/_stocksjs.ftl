window.app = {
	init: function() {
		console.log('initing cashier app');
		this.loadOrders();
	},
	loadOrders: function() {
		$.ajax({ url: '/orders/cashierList', data: app.filter, method: 'get',
      success: function(data) {
      	console.log(data);
        var items = data.items;
        if(app.arrays.orders === undefined) {
					app.arrays.orders = new app.arrays.Orders(items);
        } else {
					app.arrays.orders.recreate(items);
        }
      }
    });
	},
	selectOrder: function(item) {
		app.orderId = item.id;

		if(app.arrays.orderInfo === undefined) {
			app.arrays.orderInfo = new app.arrays.OrderInfo();
		}
		
		app.arrays.orderInfo.set(item);

		$.ajax({ url: '/orders/itemsList', data: { order_id: item.id }, method: 'get',
			success: function(data) {
				console.log(data.items);
				app.arrays.orderInfo.recreate(data.items);
			}
		});
	},
	filter: {},
	arrays: {},
	forms: {},
	models: {},
	selects: {}
};

app.models.OrderItem = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						artcode: ':sandbox .artcode',
						amount: ':sandbox .amount',
						color: ':sandbox .color',
						width: ':sandbox .width'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::sandbox', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					})
				;
			})
		;
	}
});

app.arrays.OrderInfo = Class({
	'extends': MK.Array,
	itemRenderer: '#order-item-template',
	Model: app.models.OrderItem,
	constructor: function(data) {
		var _this = this;
		_this.bindNode({
			sandbox: '#order-info',
			clientname: ':sandbox .clientname',
			phone: ':sandbox .phone',
			username: ':sandbox .username',
			sum: ':sandbox .sum',
			noResult: ':sandbox .noResult',
			finishBtn: ':sandbox #finish-btn',
			overlayer: '#order-payment-overlay',
			closeBtn: '#order-payment-overlay .overlayClose',
			doneBtn: '#order-payment-overlay #done-btn',
			printBtn: '#order-payment-overlay #print-btn',
			message: '#order-payment-overlay #message',
			icon: '#order-payment-overlay #icon',
			subMessage: '#order-payment-overlay #sub-message',
			container: ':sandbox tbody'
		}, {
			setValue: function(v) {
				this.innerHTML = v;
			}
		}).bindNode({
			clientname: ':sandbox .clientname-short'
		}, {
			setValue: function(v) {
				this.innerHTML = v.substring(0, 1);
			}
		}).linkProps('isOpen', 'status_code', function(status_code) {
			return status_code === 'OPEN';
		}).bindNode('isOpen', '.open-info', {
			setValue: function(v) {
				$(this).toggleClass('hidden', !v);
			}
		}).linkProps('isPaid', 'status_code', function(status_code) {
			return status_code === 'PAID';
		}).bindNode('isPaid', '.paid-info', {
			setValue: function(v) {
				$(this).toggleClass('hidden', !v);
			}
		}).linkProps('isClosed', 'status_code', function(status_code) {
			return status_code === 'CLOSED';
		}).bindNode('isClosed', '.closed-info', {
			setValue: function(v) {
				$(this).toggleClass('hidden', !v);
			}
		}).on('keyup::cashInput', function(e) {
			if(this.cashInput >= this.sum)
				this.change = this.cashInput - this.sum;
			else
				this.change = 0;
		}).on('click::finishBtn', function(e) {
			e.preventDefault();
		  _this.finishOrder();
		}).on('click::doneBtn', function(e) {
			e.preventDefault();
			_this.overlayClose();
		})

		.recreate(data);

		this.$sandbox.find('.row:first').toggleClass('hidden');
		this.$nodes.noResult.toggleClass('hidden');
	},
	finishOrder: function() {
		var _this = this;
		_this.overlayOpen();

		var data = {
			id: app.orderId,
		};

		$.ajax({ url: '/orders/stock', data: data, method: 'put',
			success: function(data) {
				console.log('data', data);
				_this.$nodes.icon.removeClass('fa-spinner').removeClass('fa-pulse').removeClass('fa-fw').addClass('fa-check');
				_this.$nodes.doneBtn.removeClass('btn-warning').addClass('btn-primary');
				_this.$nodes.closeBtn.removeClass('hidden');
				_this.$nodes.message.text(data.message);
				_this.$nodes.subMessage.text(data.subMessage);
				_this.status_code = 'CLOSED';
				app.arrays.orders.selectedItem.status_code = 'CLOSED';
			},
			error: function(data) {
				_this.$nodes.icon.removeClass('fa-spinner').removeClass('fa-pulse').addClass('fa-times');
				_this.$nodes.doneBtn.removeClass('btn-primary').addClass('btn-warning');
				_this.$nodes.closeBtn.removeClass('hidden');
				_this.$nodes.message.text(data.message);
				_this.$nodes.subMessage.text(data.subMessage);
			}
		});
	},
	overlayClose: function() {
		var _this = this;
		_this.$nodes.overlayer.fadeOut("fast").addClass("closed");
		// _this.
	},
	overlayOpen: function() {
		var _this = this;
		_this.$nodes.overlayer.fadeIn("fast").removeClass("hide");
	}
});

app.models.Order = Class({
	'extends': MK.Object,
	constructor: function(data) {
		var _this = this;
		_this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						clientname: ':sandbox .clientname',
						phone: ':sandbox .phone',
						status_code: ':sandbox .status',
						sum: ':sandbox .sum'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::sandbox', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					}).linkProps('isPaid', 'status_code', function(status_code) {
						return status_code === 'PAID';
					}).bindNode('isPaid', ':sandbox .status', {
						setValue: function(v) {
							$(this).toggleClass('label-success', v);
							$(this).toggleClass('label-warning', !v);
						}
					})
				;
			})
		;
	}
});

app.arrays.Orders = Class({
	'extends': MK.Array,
	itemRenderer: '#order-template',
	Model: app.models.Order,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#orders-list',
				container: ':sandbox tbody',
				noResult: ':sandbox .noResult',
				input: ':sandbox #input-search'
			}).on( '@clicked', function( item ) {
				$('.cart-item').removeClass('selected');
				item.$sandbox.toggleClass('selected');
				this.selectedItem = item;
				app.selectOrder(item);
			}).on('recreate', function(e) {
				this.$nodes.noResult.toggleClass('hidden', this.length > 0);
			}).on('keyup::input', function(e) {
				app.filter.q = this.$nodes.input.val();
				app.loadOrders();
			})
			.recreate(data)
		;
	},
	unselectAll: function() {
		$('.cart-item').removeClass('selected');
	}
});


app.models.CartItem = Class({
	'extends': MK.Object,
	constructor: function(data) {
		var _this = this;
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						pricewithtax: ':sandbox .pricewithtax',
						artcode: ':sandbox .artcode',
						amount: ':sandbox .amount',
						discount: ':sandbox .discount',
						sum: ':sandbox .sum',
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).bindNode({
						overlayer: ':sandbox .amount-input-area'
					}).on('click::sandbox', function(e) {
						e.preventDefault();
						$('.cart-item').removeClass('selected');
						_this.$sandbox.addClass('selected');
						// $('.amount-input-area').removeClass('bg-primary').addClass('bg-success');
						// _this.$nodes.overlayer.removeClass('bg-success').addClass('bg-primary');
						_this.$nodes.amount.focus().select();

					}).on('change::amount', function() {
						this.sum = this.amount * this.pricewithtax;
						this.trigger('sumChanged');
					})
				;
			})
		;
	}
});

app.arrays.CartItems = Class({
	'extends': MK.Array,
	itemRenderer: '#cart-item-template',
	Model: app.models.CartItem,
	constructor: function(data) {
		var _this = this;
		_this.set('orderSum', 0)
			.bindNode({
				sandbox: '.products-cart',
				container: ':sandbox tbody'
			}).bindNode({
				orderSum: '.order-sum'
			}, {
				setValue: function(v) {
					this.innerHTML = v;
				}
			}).on( '@clicked', function( item ) {
				// app.itemSelected(item);
			}).on('push', function(e) {
				e.args.forEach(function(item, index) {
					_this.orderSum += item.sum;
					// app.arrays.products_ids.push(item.id);
				});
			}).on('remove', function(e) {
				e.removed.forEach(function(item, index) {
					_this.orderSum -= item.sum;
					// app.arrays.products_ids.pull(item.id);
				});
			}).on('@sumChanged', function() {
				var sum = 0;
				_this.forEach(function(item, index) {
					// console.log(item, index);
					sum += item.sum;
				});
				_this.set('orderSum', sum);
			})
			.recreate(data.arr)
		;
	}
});