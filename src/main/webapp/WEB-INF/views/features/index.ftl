<#ftl encoding="utf-8">
<@content for="title">Attributes List</@content>


<div class="message"><@flash name="message"/></div>



<@link_to action="new_form">Add new attribute</@link_to>

<table>
    <tr>
        <td>Title</td>
        <td>Edit</td>
    </tr>
<#list items as item>
    <tr>
        <td>
            <@link_to action="show" id=item.id>${item.title}</@link_to>
        </td>
        <td>
            <@confirm text="Are you sure you want to delete this item: " + item.title + "?" form=item.id>Delete</@confirm>
            <@form  id=item.id action="delete" method="delete" html_id=item.id />
        </td>
    </tr>
</#list>
</table>




