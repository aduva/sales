<@content for="title">Add new attribute</@content>

<span class="error_message"><@flash name="message"/></span>
<h2>Adding new attribute</h2>


<@form action="create" method="post">
    <table style="margin:30px">
        <tr>
            <td>Title:</td>
            <td><input type="text" name="title" value="${(flasher.params.title)!}"> *
                            <span class="error">${(flasher.errors.title)!}</span>
            </td>
        </tr>
        <tr>
            <td>URL:</td>
            <td><input type="text" name="url" value="${(flasher.params.url)!}"> *
                            <span class="error">${(flasher.errors.url)!}</span>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><@link_to>Cancel</@link_to> | <input type="submit" value="Add new attribute"></td>

        </tr>
    </table>
</@form>



