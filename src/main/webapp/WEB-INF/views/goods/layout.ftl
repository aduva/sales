<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<title>Sales - <@yield to="title"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="apple-touch-icon" href="${context_path}/pages/ico/60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="${context_path}/pages/ico/76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="${context_path}/pages/ico/120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="${context_path}/pages/ico/152.png">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description" />
	<meta content="Askhat Shakenov" name="author" />
	<link rel="icon" type="image/x-icon" href="${context_path}/img/favicon.ico" />
	<!-- BEGIN Vendor CSS-->
	<link href="${context_path}/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
	<link href="${context_path}/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="${context_path}/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
	<link href="${context_path}/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="${context_path}/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="${context_path}/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
	<!-- BEGIN Pages CSS-->
	<link href="${context_path}/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
	<link class="main-stylesheet" href="${context_path}/pages/css/pages.css" rel="stylesheet" type="text/css" />
	<link href="${context_path}/css/main.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 9]>
	    <link href="${context_path}/pages/css/ie9.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<script type="text/javascript">
		window.onload = function()
		{
		  // fix for windows 8
		  if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
		    document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="${context_path}/pages/css/windows.chrome.fix.css" />'
		}
  </script>
  <@yield to="styles"/>
</head>
<body class="fixed-header <@yield to="bodyClass"/>">
	<#include "../layouts/sidebar.ftl">
	<div class="page-container">
		<#include "../layouts/header.ftl">
		<div class="page-content-wrapper full-height">
			<div class="content full-height">
				
					${page_content}
			</div>
			
		</div>
    </div>

	<script src="${context_path}/plugins/pace/pace.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/modernizr.custom.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/pjax/jquery.pjax.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/jquery-bez/jquery.bez.min.js"></script>
	<script src="${context_path}/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="${context_path}/plugins/jquery-actual/jquery.actual.min.js"></script>
	<script src="${context_path}/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
  <script src="${context_path}/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
	<script src="${context_path}/plugins/bootstrap-select2/select2.min.js"></script>

	<!-- END VENDOR JS -->
	<!-- BEGIN CORE TEMPLATE JS -->
	<script src="${context_path}/pages/js/pages.js" type="text/javascript"></script>
	<script src="${context_path}/js/aw.js" type="text/javascript"></script>
	<script type="text/javascript">
		// $(document).pjax('a', '#page-content');
	</script>
	<@yield to="js"/>

</body>

</html>
