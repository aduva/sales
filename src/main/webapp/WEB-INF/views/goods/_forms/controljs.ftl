window.food = {
	init: function(item) {
		
		food.initRoutes();
		food.initFilterMenu();
    food.initGrid();
    food.initSelections();
    food.initRangeSlider();
		food.grid.jsGrid("loadData");
		
		$('#info-panel').portlet({
      progress: 'circle',
      progressColor: 'success'
    });
	},
	selectFood: function(item) {
		console.log(item.state);
		$('#foods-sub-list table tr').removeClass('active');
		food.item = item;
		if(food.item.state === undefined) {
			food.item.state = 'show';
		}
		food.showInfo();
		food.renderFood();
	},
	showInfo: function() {
		$('div.no-result').hide();
		$('div.email-content-wrapper').show();
		$("div.email-content-wrapper").scrollTop(0);
	},
	renderFood: function() {
		if(food.forms.info === undefined) {
			food.forms.info = new food.forms.Info(food.item);
		} else {
			food.forms.info.jset(food.item);
		}
    
		food.selects.selectCategory.select2('data', {id: food.item.category_id, title: food.item.category});
		food.selects.selectWidth.select2('data', {id: food.item.width_id, title: food.item.width});
	},
	hideInfo: function() {
  	$('div.no-result').show();
		$('div.email-content-wrapper').hide();
	},
	initRoutes: function() {
		riot.route.base('/goods/')
		riot.route.start(true);

		riot.route('/create', food.createFood);
	},
	initRangeSlider: function() {
		$("#priceSlider").ionRangeSlider({
        min: 0,
        max: 5000,
        type: 'double',
        postfix: " тг.",
        maxPostfix: "+",
        prettify: false,
        hasGrid: true
    });
    food.priceSlider = $("#priceSlider").data("ionRangeSlider");
	},
	initFilterMenu: function() {
		$.ajax({ url: '/categories/tree?url=foods', method: 'get',
      success: function(data) {
        var menus = data.items;
				food.arrays.foodTypeMenus = new food.arrays.FoodTypeMenus(menus);
      }
    });

    $.ajax({ url: '/features/featureValues?url=width', method: 'get',
      success: function(data) {
        var menus = data.items;
				food.arrays.foodWidthMenus = new food.arrays.FoodWidthMenus(menus);
      }
    });

    $('body').on('keyup', function(e) {
    	if(e.keyCode == 27) {
    		food.resetFilter();
 	   		food.loadFoods();	
    	}
    });

    $('#filter').on('keyup', function(e) {
    	if(e.keyCode >= 48 && e.keyCode <= 90) {
	    	food.filter.q = $(this).val();
	    	food.gridTitle.q = $(this).val();
	    	food.loadFoods();
	    }
    });

    $('#priceSlider').on('change', function(e) {
    	var range = $(e.target).val(),
    		priceFrom = range.split(';')[0],
    		priceTo = range.split(';')[1];
    	
    	if(priceFrom != food.filter.priceFrom || priceTo != food.filter.priceTo) {
    		food.filter.priceFrom = priceFrom;
    		food.filter.priceTo = priceTo;
    		food.loadFoods();
    	}
    });

    $('#resetFilter').on('click', function(e) {
    	food.resetFilter();
    	food.loadData();
    });

    $('#allPrices').on('click', function(e) {
    	food.priceSlider.reset();
    	food.loadData();
    });

    $('a#createBtn').on('click', function(e) {
    	e.preventDefault();
	    riot.route('create');
    })
	}, 
	initGrid: function() {
		food.grid = $("#foods-sub-list").jsGrid({
	    hidth: "100%",
	    width: "100%",
	    paging: true,
	    autoload: false,
	    pageLoading: true,
	    pageSize: 10,
	    pageIndex: 1,
	    pagerFormat: "&nbsp; {next}",
	    pageNextText: "Загрузить еще...",
	    controller: {
	      loadData: function(filter) {
	        
	        filter.pageSize *= filter.pageIndex;
	        filter.pageIndex = 1;
	        var deferred = $.Deferred();
	        $.ajax({
	            url: '/goods/list',
	            data: filter,
	            dataType: 'json',
	            success: function(data) {
	            	console.log(data.items);
	              deferred.resolve({ 
	                data: data.items,
	                itemsCount: data.total
	              });
	            }
	        });

	        return deferred.promise();
	      }
 	    },
 	    onItemInserted: function(grid, item) {
 	    	console.log('inserted', grid, item);
 	    },
			rowClick: function(args) {
        food.selectFood(args.item);
        setTimeout(function() {
        	$(args.event.target).parent('tr').addClass('active');
        }, 200);
      },
	    rowRenderer: function(item) {
	    	// if(item === undefined) {
	    	// 	return;
	    	// }
	        // var food = item;
	        var $icon = $("<span>").addClass("icon-thumbnail bg-master-light pull-left text-master title-short").text(item.title.capitalize().substr(0, 1));
	        var $info = $("<div>").addClass("pull-left")
	          .append($("<p>").addClass("hint-text all-caps font-montserrat small no-margin overflow-ellipsis category-title").text(item.category))
	          .append($("<h5>").addClass("no-margin overflow-ellipsis title").text(item.title));
	        var $price = $("<div>").addClass("pull-right text-right")
	          .append($("<p>").addClass("hint-text all-caps font-montserrat small no-margin overflow-ellipsis")
	            .append($("<span>").addClass("width-title").text(item.width.concat(' / ').concat(item.color)))
	          .append($("<h5>").addClass("no-margin overflow-ellipsis").append($("<span>").addClass("pricewithtax").text(item.pricewithtax))));
	
	        if(item.is_disabled) {
	        	$info.find('h5').addClass('strikethrough');
	        }

	        var $tr = $("<tr>");
	        
	        if(item.isActive) {
	        	$tr.addClass('active');
	        }
	        return $tr.append($("<td>").append($icon).append($info).append($price));
	    },
	    fields: [
	      { title: "Все блюда" }
	    ]
		});
	},
	initSelections: function() {
		this.selects.selectProducts = $('#setproduct').select2({
      ajax: {
        url: "/products/connectedByCategory?url=ingredients", dataType: 'json', delay: 250,
        data: function (term, page) {
          return { q:  term, page: page };
        },
        results: function (data, page) {
          return { results: data.items };
        },
        cache: true
      },
      initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== "") {
              $.ajax("/products/" + id + "/show", {
                  dataType: "json"
              }).done(function(data) { callback(JSON.parse(data.item)); });
          }
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: food.formatSelectResult,
      formatSelection: food.formatSelectResult
    });

		this.selects.selectCategory = $('#category').select2({
      ajax: {
        url: "/categories/tree?url=foods", dataType: 'json', delay: 250,
        data: function (term, page) {
          return { q:  term, page: page };
        },
        results: function (data, page) {
          return { results: data.items };
        },
        cache: true
      },
      initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== "") {
              $.ajax("/categories/" + id + "/show", {
                  dataType: "json"
              }).done(function(data) { callback(JSON.parse(data.item)); });
          }
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: food.formatSelectResult,
      formatSelection: food.formatSelectResult
    });

    this.selects.selectWidth = $('#width').select2({
      ajax: {
        url: "/features/featureValues?url=width", dataType: 'json', delay: 250,
        data: function (term, page) {
          return { q:  term, page: page };
        },
        results: function (data, page) {
          return { results: data.items };
        },
        cache: true
      },
      initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== "") {
              $.ajax("/features/" + id + "/showValue", {
                  dataType: "json"
              }).done(function(data) { callback(JSON.parse(data.item)); });
          }
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: food.formatSelectResult,
      formatSelection: food.formatSelectResult
    });
	},
	loadFoods: function() {
		var title = 'Все блюда';
		
		if(food.gridTitle.category !== undefined && food.gridTitle.category !== '')
			title = food.gridTitle.category;

		if(food.gridTitle.width !== undefined && food.gridTitle.width !== '')
			title = title.concat(', ').concat(food.gridTitle.width);

		if(food.gridTitle.q !== undefined && food.gridTitle.q !== '')
			title = title.concat(', ').concat(food.gridTitle.q);

		if(food.filter.priceFrom != 0 || food.filter.priceTo != 5000)
			title = title.concat(', ').concat(food.filter.priceFrom).concat('тг. - ').concat(food.filter.priceTo).concat('тг.');
		food.grid.jsGrid("option", "fields", [{title: title }]);
		
		setTimeout(function() {
			food.grid.jsGrid("loadData", food.filter);
    }, 100);
	},
	createFood: function() {
		food.selectFood({id: undefined, 
  		title: '', 
    	pricewithtax: undefined, 
    	is_disabled: false, 
    	width_id: undefined, 
    	width: undefined, 
    	category_id: undefined, 
    	category: undefined,
    	state: food.states.create
    });
	},
	resetFilter: function() {
		food.filter.q = null;
		food.filter.width_id = null;
		food.filter.category_id = null;
		
		food.priceSlider.reset();

		food.filter.priceFrom = null;
		food.filter.priceTo = null;

		food.gridTitle.q = undefined;
		food.gridTitle.width = undefined;
   	food.gridTitle.category = undefined;
   	food.arrays.foodWidthMenus.$sandbox.find('li').removeClass('active');
   	food.arrays.foodTypeMenus.$sandbox.find('li').removeClass('active');
   	$('#filter').val('');
	},
	forms: {},
	filter: {priceFrom: 0, priceTo: 5000},
	gridTitle: {},
	models: {},
	arrays: {},
	selects: {},
	states: {
		create: 'create',
		edit: 'edit',
		show: 'show'
	},
	state: 'show',
	formatSelectResult: function(data) {
		return data.title;
	}
}

food.models.FoodTypeMenu = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::title', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					})
				;
			})
		;
	}
});

food.arrays.FoodTypeMenus = Class({
	'extends': MK.Array,
	itemRenderer: '#foods-menu-template',
	Model: food.models.FoodTypeMenu,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#foods-menu',
				container: ':sandbox ul.sub-menu',
				allFoods: ':sandbox #allFoods'
			}).on( '@clicked', function( item ) {
				food.filter.category_id = item.id;
				food.gridTitle.category = item.title;
    		food.loadFoods();

    		this.$sandbox.find('li').removeClass('active');
    		item.$sandbox.addClass('active');
			}).on('click::allFoods', function(e) {
				e.preventDefault();
				food.filter.category_id = null;
				food.gridTitle.category = 'Все блюда';
    		food.loadFoods();
    		this.$sandbox.find('li').removeClass('active');
			})
			.recreate(data);
		;
	}
});

food.models.FoodWidthMenu = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::title', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					})
				;
			})
		;
	}
});

food.arrays.FoodWidthMenus = Class({
	'extends': MK.Array,
	itemRenderer: '#foods-menu-template',
	Model: food.models.FoodWidthMenu,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#widths-menu',
				container: ':sandbox ul.sub-menu',
				allWidths: ':sandbox #allWidths'
			}).on( '@clicked', function( item ) {
				food.filter.width_id = item.id;
				food.gridTitle.width = item.title;
    		food.loadFoods();
    		this.$sandbox.find('li').removeClass('active');
    		item.$sandbox.addClass('active');
			}).on('click::allWidths', function(e) {
				e.preventDefault();
				food.filter.width_id = null;
				food.gridTitle.width = '';
    		food.loadFoods();
    		this.$sandbox.find('li').removeClass('active');
			})
			.recreate(data);
		;
	}
});

food.forms.Info = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#info-panel',
				title: ':sandbox input#title',
				pricewithtax: ':sandbox input#pricewithtax',
				width_id: ':sandbox input#width',
				category_id: ':sandbox input#category',
				cancel: ':sandbox button.cancel',
				infoForm: ':sandbox #info-form',
				editForm: ':sandbox #edit-form',
				editBtn: ':sandbox a#edit',
				deleteBtn: 'div#deleteModal button#delete',
				enableBtn: ':sandbox a#enable',
				submitBtn: ':sandbox a#submitBtn'
			}).bindNode({
				title: ':sandbox .title',
				color: ':sandbox .color',
				description: '.description',
				category: ':sandbox .category-title',
				width: ':sandbox .width-title',
				pricewithtax: ':sandbox .pricewithtax'
			}, {
				setValue: function(v) {
					if(!v)
						v = 'Описания нет';
						this.innerHTML = v;
				}
			}).linkProps('isValid', 'title category width pricewithtax', function( title, category, width, pricewithtax ) {
				return title.length >= 3 && category !== undefined && width !== undefined && pricewithtax !== undefined && pricewithtax !== '';
			}).linkProps('isDisabled', 'is_disabled', function( is_disabled ) {
				return is_disabled;
			}).linkProps('show', 'state', function(state) {
				return state === food.states.show;
			}).linkProps('edit', 'state', function(state) {
				return state === food.states.edit;
			}).linkProps('create', 'state', function(state) {
				return state === food.states.create;
			}).onDebounce('click::submitBtn', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('click::editBtn', function(e) {
				e.preventDefault();
				this.jset('state', 'edit');
			}).on('click::cancel', function(e) {
				if(this.id !== undefined) {
					this.jset('state', 'show');
				} else {
					food.hideInfo();
				}
			}).bindNode('isValid', 'a#submitBtn', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			}).bindNode('show', '.info', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).bindNode('edit', '.edit', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).bindNode('create', '.create', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).bindNode('isDisabled', '.showIfDisabled', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).bindNode('isDisabled', '.showIfEnabled', {
				setValue: function(v) {
					$(this).toggleClass('hidden', v);
				}
			}).on('change::category_id', function(e) {
				this.category = food.selects.selectCategory.select2('data').title;
				this.category_id = food.selects.selectCategory.select2('data').id;
			}).on('change::width_id', function(e) {
				this.width = food.selects.selectWidth.select2('data').title;
				this.width_id = food.selects.selectWidth.select2('data').id;
			}).on({
				'click::deleteBtn': this.toggleDisable, 
				'click::enableBtn': this.toggleDisable
			})
		;
	},
	toggleDisable: function(e) {
		e.preventDefault();
		var _ = this,
			item = _.toJSON(),
			_item = food.item;

		$.ajax({url: '/foods/toggleDisable', data: item, method: 'put',
			success: function(data) {
				showNotification(data);
				item.isActive = true;
				item.is_disabled = !item.is_disabled;
    		item.state = 'show';

    		food.item = item;
        food.forms.info.jset('is_disabled', item.is_disabled);

       //  setTimeout(function() {
    			// refreshPortlet(false);
       //  }, 500);
				food.grid.jsGrid('updateItem', _item, item);
			},
			error: function(data) {
				showNotification(data);
				setTimeout(function() {
    			refreshPortlet(false);
        }, 500);
			}
		});

	},
	submitForm: function() {

		var _ = this,
			item = _.toJSON(),
			url = '/foods/save',
			method = 'post';

		refreshPortlet(true);
		
		if(item.id !== undefined) {
			url = '/foods/update';
			method = 'put';
		}

		console.log('item >> ', item);

		var _item = food.item;
		
		$.ajax({url: url, data: item, method: method,
			success: function(data) {
				showNotification(data);
				console.log(data);
				item = data.item;
    		item.state = 'show';
				item.isActive = true;
    		
        food.forms.info.jset(item);
      	
        setTimeout(function() {
    			refreshPortlet(false);
        }, 500);

  			if(method === 'put') {
  	    	food.grid.jsGrid('updateItem', _item, item);
  			} else {
  				food.grid.jsGrid('insertItem', item);
  				food.grid.jsGrid('refresh');
  				food.resetFilter();
  			}
        
			},
			error: function(data) {
				showNotification(data);
				setTimeout(function() {
    			refreshPortlet(false);
        }, 500);
			}
		});

		return _;
	}
});



window.showNotification = function(data) {
	$('body').pgNotification({
        style: 'bar',
        message: data.message,
        position: 'top',
        timeout: 3000,
        type: data.alert
    }).show();
};