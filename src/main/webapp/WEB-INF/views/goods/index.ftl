<nav class="secondary-sidebar padding-30">
  <a href="#" class="btn btn-complete btn-block btn-compose m-b-30" id="createBtn"><i class="fa fa-plus"></i> Новое блюдо</a>
  <!-- <p class="menu-title">ФИЛЬТР</p> -->
  <div class="form-group form-group-default">
    <input type="text" class="form-control" name="filter" id="filter" autofocus placeholder="Поиск...">
  </div>
  <ul class="main-menu">
    <li class="" id="foods-menu">
      <a href="#" id="allFoods"><span class="title"><i class="pg-folder"></i> Категория</span></a>
      <ul class="sub-menu no-padding"></ul>
    </li>
    <li class="" id="widths-menu">
      <a href="#" id="allWidths"><span class="title"><i class="fa fa-balance-scale"></i> Вес</span></a>
      <ul class="sub-menu no-padding"></ul>
    </li>
    <li class="" id="prices-menu">
      <a href="#" id="allPrices"><span class="title"><i class="fa fa-money"></i> Цена</span></a>
      <ul class="sub-menu no-padding">
        <li>
          <div class="irs-wrapper primary">
            <input type="text" id="priceSlider" name="priceSlider" value="0;5000" />
          </div>
        </li>
      </ul>
    </li>
    <li class="">
      <a href="#" id="resetFilter"><span class="title"><i class="pg-close"></i> Сбросить фильтр (Esc)</span></a>
    </li>
  </ul>
</nav>



<div class="inner-content full-height" id="foods-list">
  <div class="split-view">
    <div class="split-list">
      <div class="list-view" id="foods-sub-list"></div>
    </div>
  
      <div data-email="opened" class="split-details">
        <div class="no-result">
          <h1>Выберите блюдо</h1>
        </div>
        <div class="email-content-wrapper" style="display:none">
          
          <div class="email-content padding-10" style="width:100%;">
            <div class="panel panel-transparent" id="info-panel" data-pages="portlet">
              <div class="panel-heading">
                <div class="panel-title">Блюдо</div> 
                <ul class="list-inline pull-right info">
                  <li>
                    <a href="#" class="no-decoration showIfEnabled hidden" id="edit"><i class="fa fa-pencil"></i></a>
                  </li>
                  <li>
                    <a href="#" class="no-decoration showIfEnabled hidden" id="trash" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i></a>
                  </li>
                  <li>
                    <a href="#" class="no-decoration showIfDisabled hidden" id="enable"><i class="fa fa-refresh"></i> Восстановить</a>
                  </li>
                </ul>
                <span class="label label-important showIfDisabled hidden" id="disabledStatus">НЕ АКТИВЕН</span>
              </div>
              <div class="panel-body" id="info">
                <div class="row m-t-10 info" id="info-form">
                  <div class="col-md-4">
                    <p class="small hint-text no-margin category-title"></p>
                    <h4 class="title"></h4>
                  </div>
                  <div class="col-md-4">
                    <div class="pull-left">
                      <p class="small hint-text no-margin">Стоимость / Ширина</p>
                      <h4 class="text-danger bold no-margin"><span class="pricewithtax"></span> тг.
                        <span class="small">/ <span class="width-title"></span></span>
                      </h4>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="pull-left">
                      <p class="small hint-text no-margin">Цвет</p>
                      <h4 class="text-primary bold no-margin"><span class="color"></span></h4>
                    </div>
                  </div>
                </div>

                <div class="row m-t-10 edit create hidden" id="edit-form">
                  <div id="form-info" role="form" autocomplete="off">
                    <p>Основные данные блюда</p>
                    <div class="form-group-attached">
                      <div class="form-group form-group-default required">
                        <label>Наименование</label>
                        <input type="text" id="title" class="form-control" required>
                      </div>
                      
                      <div class="row clearfix" style="margin-left:0px;margin-right:0px;">
                        <div class="col-sm-6">
                          <div class="form-group form-group-default required">
                            <label>Цена</label>
                            <input type="number" id="pricewithtax" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group form-group-default form-group-default-select2 required">
                            <label style="z-index:1;">Вес на выходе</label>
                            <input type="hidden" class="full-width" data-init-plugin="select2" id="width" required>
                          </div>
                        </div>
                      </div>

                      <div class="form-group form-group-default form-group-default-select2 required">
                        <label style="z-index:1;">Тип блюда</label>
                        <input type="hidden" class="full-width" data-init-plugin="select2" id="category" required>
                      </div>
                    </div>

                    <hr class="b-grey b-dashed">
                    <a href="#" class="btn btn-block btn-primary" id="submitBtn">
                      <span class="pull-left"><i id="submit-icon" class="fa fa-check"></i></span>
                      <span id="submit-text">Готово</span>
                    </a>
                    <button type="button" class="btn btn-block btn-danger cancel">
                      <span class="pull-left"><i class="fa fa-ban"></i></span>
                      <span>Отмена</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
                       
            <div class="panel panel-default info">
              <div class="panel-heading">
                <div class="panel-title">Описание</div>
              </div>
              <div class="auto-overflow ingrs-table">
                <div class="p-l-20 p-b-20 p-t-15 full-width">
                  <p class="hint-text description">Описания нет</p>
                </div>
              </div>
              
              </div>


            </div>
          
        </div>
      </div>
      <!-- END SPLIT DETAILS -->
      <!-- START COMPOSE BUTTON FOR TABS -->
      
  </div>
</div>

<div class="modal fade slide-right in" id="deleteModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
        <div class="container-xs-height full-height">
          <div class="row-xs-height">
            <div class="modal-body col-xs-height col-middle text-center   ">
              <h5 class="text-primary ">Действительно хотите <span class="semi-bold">удалить</span> данное блюдо?</h5>
              <br>
              <button type="button" id="delete" class="btn btn-danger btn-block" data-dismiss="modal"><i class="fa fa-trash"></i> Удалить</button>
              <button type="button" class="btn btn-default btn-block" data-dismiss="modal"><i class="fa fa-ban"></i> Отмена</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/html" id="foods-menu-template">
<li><a href="#"><span class="title"></span></a></li>
</script>

<script type="text/html" id="set-template">
<tr>
  <td class="font-montserrat all-caps">
    <a href="#" class="remove-item delete fs-18 m-r-20 pull-left"><i class="pg-close"></i></a>
    <span class="title fs-12"></span>
  </td>
  <td class="text-right">
    <span class="hint-text small artcode"></span>
  </td>
  <td class="text-right b-r b-dashed b-success">
    <span class="hint-text small amount"></span>
  </td>
  <td class="">
    <span class="font-montserrat fs-18 bold price"></span>
  </td>
</tr>
</script>

<script type="text/html" id="food-template">
<li class="item padding-15">
  <span class="icon-thumbnail bg-master-light pull-left text-master title-short">?</span>
  <div class="pull-left">
    <p class="hint-text all-caps font-montserrat small no-margin overflow-ellipsis category-title">Категория?</p>
    <h5 class="no-margin overflow-ellipsis title">Название?</h5>
  </div>
  <div class="pull-right text-right">
    <p class="hint-text all-caps font-montserrat small no-margin overflow-ellipsis">Вес <span class="width-title">-</span> г</p>
    <h5 class="no-margin overflow-ellipsis"><span class="pricewithtax">-</span> тг</h5>
  </div>
  <div class="clearfix"></div>
</li>
</script>

<@content for="styles">
<link type="text/css" rel="stylesheet" href="${context_path}/plugins/jsgrid/jsgrid.min.css" />
<link type="text/css" rel="stylesheet" href="${context_path}/plugins/jsgrid/jsgrid-theme.min.css" />
<link media="screen" type="text/css" rel="stylesheet" href="${context_path}/plugins/ion-slider/css/ion.rangeSlider.css">
<link media="screen" type="text/css" rel="stylesheet" href="${context_path}/plugins/ion-slider/css/ion.rangeSlider.skinFlat.css">

<style>
</style>

</@content>

<@content for="js">
<script src="${context_path}/plugins/jquery-dynatree/jquery.dynatree.min.js" type="text/javascript"></script>
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>
<script src="${context_path}/plugins/riot/riot.min.js"></script>
<script src="${context_path}/plugins/nvd3/lib/d3.v3.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/src/utils.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/src/tooltip.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/src/interactiveLayer.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/src/models/axis.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
<script src="${context_path}/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
<script type="text/javascript" src="${context_path}/plugins/jsgrid/jsgrid.min.js"></script>
<script type="text/javascript" src="${context_path}/plugins/ion-slider/js/ion.rangeSlider.min.js"></script>
<script>
  $(document).ready(function() {
    $('li#menu-kitchen, li#menu-foods').addClass('active open');

    String.prototype.capitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    };

    function refreshPortlet(refresh) {
      $('#info-panel').portlet({refresh: refresh});
    }

    // $("#foods-sub-list").jsGrid("loadData", { category_id: "3" }).done(function() {
    //   console.log("data loaded");
    // });

    // $("#foods-sub-list").jsGrid("option", "fields", [{title: "Салаты"}]);
    <@render partial="forms/controljs"/>
    food.init();
  });
</script>
</@content>
