

<!-- BEGIN SIDEBAR -->
    <div class="page-sidebar" data-pages="sidebar">
      <div id="appMenu" class="sidebar-overlay-slide from-top">
      </div>
      <!-- BEGIN SIDEBAR HEADER -->
      <div class="sidebar-header">
        <img src="${context_path}/img/logo_white.png" alt="logo" class="brand" data-src="${context_path}/img/logo_white.png" data-src-retina="${context_path}/img/logo_white_2x.png" width="78" height="22">
        <div class="sidebar-header-controls">
          <button data-pages-toggle="#appMenu" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" type="button"><i class="fa fa-angle-down fs-16"></i>
          </button>
          <button data-toggle-pin="sidebar" class="btn btn-link visible-lg-inline" type="button"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR HEADER -->
      <!-- BEGIN SIDEBAR MENU -->
      <div class="sidebar-menu">
        <ul class="menu-items">
          <li class="m-t-30">
            <a href="${context_path}/" class="detailed">
              <span class="title"><@msg key="menu.dashboard"/></span>
              <span class="details">уведомлений нет</span>
            </a>
            <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
          </li>
          
          <li class="" id="menu-order">
            <a href="">
              <span class="title"><@msg key="menu.orders"/></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-shopping-cart"></i></span>
          </li>
          <li class="" id="menu-catalog">
            <a href="javascript:;">
              <span class="title"><@msg key="menu.catalog"/></span>
              <span class="arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
            <ul class="sub-menu">
              <li class="" id="menu-products">
                <a href="/products"><@msg key="menu.products"/></a>
                <span class="icon-thumbnail"><@msg key="menu.products" menushort=true/> </span>
              </li>
              <li class="" id="menu-categories">
                <a href="/categories"><@msg key="menu.categories"/></a>
                <span class="icon-thumbnail"><@msg key="menu.categories" menushort=true/></span>
              </li>
              <li class="" id="menu-attributes">
                <a href="/attributes"><@msg key="menu.attributes"/></a>
                <span class="icon-thumbnail"><@msg key="menu.attributes" menushort=true/></span>
              </li>
              <li class="" id="menu-features">
                <a href="/features"><@msg key="menu.features"/></a>
                <span class="icon-thumbnail"><@msg key="menu.features" menushort=true/></span>
              </li>
            </ul>
          </li>
          <li class="">
            <a href="${context_path}/goods">
              <span class="title"><@msg key="menu.products"/></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-book"></i></span>
          </li>
          <li class="">
            <a href="${context_path}/services">
              <span class="title"><@msg key="menu.services"/></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-paint-brush"></i></span>
          </li>

          <li class="" id="menu-stock">
            <a href="javascript:;">
              <span class="title"><@msg key="menu.stock"/></span>
              <span class="arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="pg-bag"></i></span>
            <ul class="sub-menu">
              <li class="" id="menu-warehouses">
                <a href="/warehouses"><@msg key="menu.warehouses"/></a>
                <span class="icon-thumbnail"><@msg key="menu.warehouses" menushort=true/></span>
              </li>
              <li class="" id="menu-management">
                <a href="/warehouses/management" action="management"><@msg key="menu.stock.management"/></a>
                <span class="icon-thumbnail"><@msg key="menu.stock.management" menushort=true/></span>
              </li>
              <li class="" id="menu-movement">
                <a href="/warehouses/movement"><@msg key="menu.stock.movement"/></a>
                <span class="icon-thumbnail"><@msg key="menu.stock.movement" menushort=true/></span>
              </li>
              <li class="" id="menu-supplies">
                <a href="/warehouses"><@msg key="menu.stock.requests"/></a>
                <span class="icon-thumbnail"><@msg key="menu.stock.requests" menushort=true/></span>
              </li>
            </ul>
          </li>
          <li class="" id="menu-properties">
            <a href="javascript:;">
              <span class="title"><@msg key="menu.properties"/></span>
              <span class="arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="pg-settings"></i></span>
            <ul class="sub-menu">
              <li class="" id="menu-taxes">
                <a href="/taxes"><@msg key="menu.taxes"/></a>
                <span class="icon-thumbnail"><@msg key="menu.taxes" menushort=true/></span>
              </li>
            </ul>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->