<@content for="title">Users</@content>


<div class="message"><@flash name="message"/></div>



<table>
    <tr>
        <td>username</td>
        <td>created_at</td>
        <td>Edit</td>
    </tr>
<#list items as user>
    <tr>
        <td>
            ${user.username}
        </td>
        <td>
            ${user.created_at}</td>
        <td>
            <@confirm text="Are you sure you want to delete this user: " + user.username + "?" form=user.username>Delete</@confirm>
            <@form  id=user.username action="delete" method="delete" html_id=user.username />
        </td>
    </tr>
</#list>
</table>




