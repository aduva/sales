<@content for="title">Groups</@content>


<div class="message"><@flash name="message"/></div>



<table>
    <tr>
        <td>groupname</td>
        <td>created_at</td>
        <td>Edit</td>
    </tr>
<#list items as group>
    <tr>
        <td>
            ${group.groupname}
        </td>
        <td>
            ${group.created_at}</td>
        <td>
            <@confirm text="Are you sure you want to delete this group: " + group.groupname + "?" form=group.groupname>Delete</@confirm>
            <@form  id=group.groupname action="delete" method="delete" html_id=group.groupname />
        </td>
    </tr>
</#list>
</table>




