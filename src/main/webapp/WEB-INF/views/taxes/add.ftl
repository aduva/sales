<#ftl encoding="utf-8">
<ul class="breadcrumb">
  <li><a href="/">Главная</a></li>
  <li><a href="/taxes">Налоговые ставки</a></li>
  <li><a href="/taxes/add" class="active">Добавить налоговую ставку</a></li>
</ul>
<div class="row">
    
  <#-- form panel -->
  <div class="col-md-10">
    <div class="panel panel-default">
      <@render partial="forms/add"/>
    </div>
  </div>
  <#-- end form panel -->
</div>


<@content for="title">Добавить налоговую ставку</@content>
<@content for="js">
<script>
    $(document).ready(function() {
      $('form#form-categories').on('change', function(e) {
        console.log('change', $(this).serializeArray());
      });

      $('#clear-form').on('click', function() {
        $('form').trigger('reset');
      });

      $('li#menu-properties, li#menu-taxes').addClass('active open');
</script>
</@content>