<#ftl encoding="utf-8">

<div class="panel-heading separator">
   <div class="panel-title">Налоговая ставка</div>
</div>
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
			<form id="form-taxes" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" action="/taxes/save" autocomplete="off" method="post">
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Наименование</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="title" placeholder="Полное наименование" name="title" required>
					</div>
				</div>
							
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Процентная ставка</label>
					<div class="col-sm-9">
						<input type="number" value="1" step="0.1" class="form-control" id="rate" placeholder="Процентная ставка" name="rate" required>
					</div>
				</div>

				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">По умолчанию</label>
					<div class="col-sm-9">
						<input type="checkbox" data-init-plugin="switchery" name="is_default"/>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-9 col-md-offset-3">
						<button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Сохранить</button>
						<button class="btn btn-default" id="clear-form"><i class="pg-close"></i> Очистить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>