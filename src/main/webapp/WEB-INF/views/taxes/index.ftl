<#ftl encoding="utf-8">
<ul class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li><a href="/taxes" class="active">Налоговые ставки</a></li>
</ul>
<div class="row">
    <#-- list -->
    <div class="col-md-12">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <input type="text" class="form-control pull-right" placeholder="быстрый поиск по наименованию">
                    </div>
                    <div class="col-xs-6">
                        <@link_to action="add" class="btn btn-primary btn-cons pull-right" style="color:#fff !important;opacity:1;"><i class="fa fa-plus"></i> Добавить налоговую ставку</@link_to>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                
                    <table class="table">
                        <thead>
                            <th>ID</th>
                            <th>Наименование</th>
                            <th>Процентная ставка</th>
                            <th>Статус</th>
                        </thead>
                        <tbody>
                            <@render partial="items" items=items/>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-12">
                            <@link_to query_string="page=${next}" class="btn btn-default btn-rounded" disabled="<#if next==page>disabled</#if>"><i class="fa fa-chevron-left"></i> Назад</@link_to>
                            <@link_to query_string="page=${prev}" class="btn btn-default btn-rounded" disabled="<#if prev==page>disabled</#if>">Вперед <i class="fa fa-chevron-right"></i></@link_to>
                            <p>Показано <b>от ${from} до ${to}</b> из <b>${getCount}</b>. Всего страниц <b>${pageCount}</b>.</p>
                        </div>
                    </div>
               
            </div>
        </div>
    </div>
    <#-- end list -->
</div>


<@content for="title">Products</@content>
<@content for="js">
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('li#menu-properties, li#menu-taxes').addClass('active open');
    });
</script>
</@content>