<#ftl encoding="utf-8">
<@content for="title">Вход в аккаунт</@content>

<div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
  <div class="bg-pic">
    <!-- START Background Pic-->
    
    <!-- END Background Pic-->
    <!-- START Background Caption-->
    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <h2 class="semi-bold text-white">
			Sales make it easy to enjoy what matters the most in the life</h2>
      <p class="small">
        images Displayed are solely for representation purposes only, All work copyright of respective owner, otherwise © 2013-2014 REVOX.
      </p>
    </div>
    <!-- END Background Caption-->
  </div>
  <!-- END Login Background Pic Wrapper-->
  <!-- START Login Right Container-->
  <div class="login-container bg-white">
    <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
      <img src="${context_path}/img/logo.png" alt="logo" data-src="${context_path}/img/logo.png" data-src-retina="${context_path}/img/logo_2x.png" width="78" height="22">
      <p class="p-t-35">Введите ваши данные для входа</p>
      <p class="p-t-35 text-error"><@flash name="message"/></p>
      <!-- START Login Form -->

      <form id="form-login" class="p-t-15" role="form" action="/authentication/do_login" method="post">
        <!-- START Form Control-->
        <div class="form-group form-group-default">
          <label>Имя пользователя</label>
          <div class="controls">
            <input type="text" name="username" placeholder="Имя пользователя" class="form-control" required autofocus>
          </div>
        </div>
        <!-- END Form Control-->
        <!-- START Form Control-->
        <div class="form-group form-group-default">
          <label>Пароль</label>
          <div class="controls">
            <input type="password" class="form-control" name="password" placeholder="Пароль" required>
          </div>
        </div>
        <!-- START Form Control-->
        <div class="row">
          <div class="col-md-6 no-padding">
            <div class="checkbox ">
              <input type="checkbox" value="1" name="remember_me" id="checkbox1">
              <label for="checkbox1">Запомнить меня</label>
            </div>
          </div>
          <div class="col-md-6 text-right">
            <a href="#" class="text-info small">Помощь</a>
          </div>
        </div>
        <!-- END Form Control-->
        <button class="btn btn-primary btn-cons m-t-10" type="submit">Вход</button>
      </form>
      <!--END Login Form-->
      <div class="pull-bottom sm-pull-bottom">
        <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
          <div class="col-sm-3 col-md-2 no-padding">
            <img alt="" class="m-t-5" data-src="${context_path}/pages/ico/120.png" data-src-retina="${context_path}/pages/ico/120_2x.png" height="60" src="${context_path}/pages/ico/120.png" width="60">
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- END Login Right Container-->
</div>