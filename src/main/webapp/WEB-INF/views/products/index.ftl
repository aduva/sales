<#ftl encoding="utf-8">
<ul class="breadcrumb">
    <li><a href="/"><@msg key="menu.main"/></a></li>
    <li><a href="/products" class="active"><@msg key="menu.products"/></a></li>
</ul>
<div class="row">
    <#-- list -->
    <div class="col-md-12">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group form-group-default">
                            <label><i class="fa fa-search"></i> <@msg key="search.title"/></label>
                            <input type="text" class="form-control" id="search-text" placeholder="<@msg key="search.starttypingforsearch"/>" autofocus>
                          </div>
                    </div>
                    <div class="col-xs-6">
                        <@link_to action="add" class="btn btn-primary btn-cons pull-right" style="color:#fff !important;opacity:1;"><i class="fa fa-plus"></i> <@msg key="products.add"/></@link_to>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                
                    <table class="table">
                        <thead>
                            <th><@msg key="products.id"/></th>
                            <th><@msg key="products.title"/></th>
                            <th><@msg key="products.artcode"/></th>
                            <th><@msg key="products.pricewithtax"/></th>
                            <th><@msg key="products.stock.amount"/></th>
                        </thead>
                        <tbody>
                            <@render partial="items" items=items/>
                        </tbody>
                    </table>
                    <div class="row">
                        
                    </div>
               
            </div>
        </div>
    </div>
    <#-- end list -->
</div>


<@content for="title"><@msg key="menu.products"/></@content>
<@content for="js">
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('li#menu-catalog, li#menu-products').addClass('active open');
    });
</script>
</@content>