window.productForms = {
	initInfoForm: function() {
		this.forms.info = new this.forms.InfoForm(this.item);
		productForms.selects.selectProducts = $('.composite-products-select').select2({
		  ajax: {
		    url: "/products",
		    dataType: 'json',
		    delay: 250,
		    data: function (term, page) {
		      return {
		        q:  term, // search term
		        page: page
		      };
		    },
		    results: function (data, page) {
		      return {
		        results: data.items
		      };
		    },
		    cache: true
		  },
		  escapeMarkup: function (markup) { return markup; },
		  minimumInputLength: 0,
		  formatResult: productForms.formatSelectResult,
		  formatSelection: productForms.formatProductSelect
		});
	},
	initPricesForm: function() {
		productForms.forms.prices = new productForms.forms.PricesForm(productForms.item);

		
	},
	initCategoriesForm: function() {
		console.log('category form init', productForms.category);
		$("#categories-tree").dynatree({
	        checkbox: true,
	        // Override class name for checkbox icon:
	        classNames: { checkbox: "dynatree-radio" },
	        selectMode: 1,
	        initAjax: { url: '/products/' + productForms.item.id + '/getCategoryTree' },
	        onActivate: function(node) {
	          node.toggleExpand();
	        },
	        onLazyRead: function(node) {
	          node.appendAjax({
	            url: '/categories/tree',
	            data: {
	              id: node.data.id
	            },
	            success: function(data) {
	            }
	          });
	        },
	        onSelect: function(select, node) {
	          productForms.forms.categories.set('category', {id: node.data.id, title: node.data.title});
	          node.activateSilently();
	        },
	        onDblClick: function(node, event) {
	          node.toggleSelect();
	        }
	      });
		if(productForms.category.id == null) {
			$.ajax({ url: '/products/' + productForms.item.id + '/getCategory', method: 'GET',
				success: function(data) {
					productForms.category = JSON.parse(data.category);
					productForms.forms.categories = new productForms.forms.CategoriesForm(productForms.item.id, productForms.category);
				}
			});
		} else {
			productForms.forms.categories = new productForms.forms.CategoriesForm(productForms.item.id, productForms.category);
		}
	},
	initCombinationsForm: function() {
		$.ajax({ url: '/attributes/list', method: 'GET',
			success: function(data) {
				productForms.attributes = JSON.parse(data.attributes);
				console.log(data, productForms.attributes);

				$('#attribute').select2({
					data: productForms.attributes,
					formatSelection: function(item) {
			        	productForms.arrays.attributeValues.set('attribute', item);
			        	return item.title;
			        }
				});
			}
		});
		productForms.forms.combinations = new productForms.forms.CombinationsForm({product_id:productForms.item.id,artcode:null,pricewithtax:0,priceperunit:0,is_default:false});
		productForms.arrays.attributeValues = new productForms.arrays.AttributeValues();
		$.ajax({ url: '/products/' + productForms.item.id + '/combos', method: 'GET',
			success: function(data) {
				console.log(data);
				var combos = JSON.parse(data.items);
				productForms.arrays.combinations = new productForms.arrays.Combinations(combos);
			}
		});
		
	},
	initSuppliersForm: function(data) {
		this.selects.selectSuppliers = $('#supplier-select').select2({
	      ajax: {
	        url: "/suppliers",
	        dataType: 'json',
	        delay: 250,
	        data: function (term, page) {
	          return {
	            q:  term, // search term
	            page: page
	          };
	        },
	        results: function (data, page) {
	          return {
	            results: data.items
	          };
	        },
	        cache: true
	      },
	      escapeMarkup: function (markup) { return markup; },
	      minimumInputLength: 0,
	      formatResult: productForms.formatSelectResult,
	      formatSelection: productForms.formatSuppliersSelect
	    });
		productForms.arrays.suppliers = new productForms.arrays.Suppliers(data);
	},
	initWarehousesForm: function(data) {
		this.selects.selectSuppliers = $('#warehouses-select').select2({
	      ajax: {
	        url: "/stocks/warehouses",
	        dataType: 'json',
	        delay: 250,
	        data: function (term, page) {
	          return {
	            q:  term, // search term
	            page: page
	          };
	        },
	        results: function (data, page) {
	          return {
	            results: data.items
	          };
	        },
	        cache: true
	      },
	      escapeMarkup: function (markup) { return markup; },
	      minimumInputLength: 0,
	      formatResult: productForms.formatSelectResult,
	      formatSelection: productForms.formatWarehousesSelect
	    });
		productForms.arrays.warehouses = new productForms.arrays.Warehouses(data);
	},
	initStockForm: function() {
		productForms.forms.stock = new productForms.forms.StockForm(this.item);
	},
	// var selectedData;
	formatSelectResult: function(data) {
	  return data.title;
	},
	formatSuppliersSelect: function(data) {
		productForms.arrays.suppliers.set('supplier', data);
		return data.title;
	},
	formatWarehousesSelect: function(data) {
		productForms.arrays.warehouses.set('warehouse', data);
		return data.title;
	},
	formatProductSelect: function(data) {
	  productForms.forms.info.composites.set('data', data);
	  return data.title;
	},
	formatTaxSelect: function(data) {
      productForms.forms.prices.set('taxRate', data);
      return data.title + ' ' + data.rate + '%';
    },
    formatTaxSelectResult: function(data) {
	  return data.title + ' ' + data.rate + '%';
	},
	forms: {},
	models: {},
	arrays: {},
	selects: {},
	category: {
		id: null,
		title: ''
	}
};

productForms.forms.CategoriesForm = Class({
	'extends': MK.Object,
	constructor: function(id, category) {
		this.set('id', id)
			.set('category', category)
			.bindNode({
				sandbox: '#form-categories'
			}).bindNode('category', ':sandbox .category-title', {
				setValue: function(v) {
					this.innerHTML = v.title;
				}
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('change:category', function() {
			}).linkProps('isValid', 'category id', function( category, id ) {
				return category !== undefined && id !== undefined;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			})
		;
	},
	submitForm: function() {
		$.ajax({url: '/products/category', data: { category_id: this.category.id, id: this.id }, method: 'put',
			success: function(data) {
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
		return this;
	}
});

productForms.forms.PricesForm = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-prices',
				cost: ':sandbox input#cost',
				pricepretax: ':sandbox input#pricePreTax',
				pricewithtax: ':sandbox input#priceWithTax',
				priceperunit: ':sandbox input#pricePerUnit',
				unitname: ':sandbox input#unitName'
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('change:pricepretax', function() {
				if(this.taxRate !== undefined)
					this.set('pricewithtax', (parseFloat(this.pricepretax) + parseFloat(this.pricepretax) * parseFloat(this.taxRate.rate) / 100).toFixed(2), {silent: true, forceHTML: true});
				else
					this.set('pricewithtax', parseFloat(this.pricepretax), {silent: true, forceHTML: true});

			}).on('change:pricewithtax', function() {
				if(this.taxRate !== undefined) {
					var p = parseFloat(parseFloat(this.taxRate.rate) / 100);
						p += 1;
					this.set('pricepretax', (parseFloat(this.pricewithtax) / p).toFixed(2), {silent: true, forceHTML: true});
				} else {
					this.set('pricepretax', parseFloat(this.pricewithtax), {silent: true, forceHTML: true});
				}

			}).on('change:taxRate', function() {
				if(this.pricepretax !== undefined) {
					this.set('pricewithtax', (parseFloat(this.pricepretax) + parseFloat(this.pricepretax) * parseFloat(this.taxRate.rate) / 100).toFixed(2), {silent: true, forceHTML: true});
				} else if(this.pricewithtax !== undefined) {
					var p = parseFloat(parseFloat(this.taxRate.rate) / 100);
						p += 1;
					this.set('pricepretax', (parseFloat(this.pricewithtax) / p).toFixed(2), {silent: true, forceHTML: true});
				}

			}).linkProps('isValid', 'cost pricepretax pricewithtax taxRate', function( cost, pricepretax, pricewithtax, taxRate ) {
				return cost && pricepretax && pricewithtax && taxRate;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			})
		;
	},
	submitForm: function() {
		if(this.isValid) {
			this.jset('tax_id', this.taxRate.id);
		}

		$.ajax({url: '/products/prices', data: this.toJSON(), method: 'put',
			success: function(data) {
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
		return this;
	}
});

productForms.forms.InfoForm = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-info',
				title: ':sandbox input#title',
				artcode: ':sandbox input#artcode',
				type_code: ':sandbox input[name=type_code]'
			}).linkProps('isValid', 'title artcode', function( title, artcode ) {
				return title.length >= 4 && artcode.length >= 5;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			}).linkProps('isComposite', 'type_code', function(type_code) {
				return type_code === 'DEFAULT';
			}).bindNode('isComposite', 'li#composite-menu', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
			
				this.submitForm();
			}).on('change:isComposite', function() {
				if(this.isComposite && this.composites === undefined) {
					this.composites = new productForms.arrays.Composites(data);
				}
			})
		;
	},
	submitForm: function() {
		if(this.isValid) {
			this.jset('composites', this.composites);
			this.jset('compositesLength', this.composites === undefined ? 0 : this.composites.length);
		}

		var method = $(this.sandbox).attr('method');

		$.ajax({url: $(this.sandbox).attr('action'), data: this.toJSON(), method: method,
			success: function(data) {
				showNotification(data);
				productForms.item = data.item;
                if(method === 'post') {
                	$('#currentBreadcrumb').attr('href', '/products/show/'+data.item.id);
                	$('#currentBreadcrumb').html(data.item.title);
                	$('a[href=#savefirst]').each(function() {
                		$(this).attr('href', $(this).attr('data-href'));
                	});
                	$(document).attr('title', 'Sales - Товар: ' + data.item.title);
                	window.history.pushState({state: {'controller': 'products', 'action': 'show', 'id': data.item.id}}, "", '/products/show/' + data.item.id);
                }
			},
			error: function(data) {
				showNotification(data);
			}
		});

		return this;
	}
});

productForms.models.Composite = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						qty: ':sandbox .qty',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					})
				;
			})
		;
	}
});

productForms.arrays.Composites = Class({
	'extends': MK.Array,
	itemRenderer: '#composite-template',
	Model: productForms.models.Composite,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#composite-control',
				table: ':sandbox #composite-table',
				cadd: ':sandbox #composite-add',
				qty: ':sandbox #composite-qty',
				container: ':sandbox tbody'
			}).on( '@readytodie', function( composite ) {
				this.pull( composite );
			}).on('click::cadd', function(e) {
				e.preventDefault();
				
				for( var i = 0; i < this.length; i++ ) {
					if( this[i].id === this.data.id ) {
						this[i].qty = this.qty;
						return;
					}
				}

				this.push({
					id: this.data.id,
					title: this.data.title,
					qty: this.qty
				});
			}).linkProps('isEmpty', 'length', function(length) {
				return length <= 0;
			}).bindNode('isEmpty', ':sandbox #alertrow', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			})
			.recreate(data);
		;
	}
});

productForms.models.AttributeValue = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						attribute: ':sandbox .attribute',
						value: ':sandbox .value',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					})
				;
			})
		;
	}
});

productForms.arrays.AttributeValues = Class({
	'extends': MK.Array,
	itemRenderer: '#attributes-template',
	Model: productForms.models.AttributeValue,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#attributes-control',
				attribute: ':sandbox #attribute',
				cadd: ':sandbox #attribute-add',
				value: ':sandbox #attribute-value',
				container: ':sandbox #attributes-container>tbody'
			}).on( '@readytodie', function( attribute ) {
				var _ = this;
				$.ajax({ url: '/products/deleteAttributeValue/' + attribute.id, method: 'delete',
					success: function(data) {
						_.pull( attribute );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::cadd', function(e) {
				e.preventDefault();
				for( var i = 0; i < this.length; i++ ) {
					if( this[i].attribute_id === this.attribute.id ) {
						this[i].value = this.value.title;
						this[i].attribute_value_id = this.value.id;
						return;
					}
				}

				this.push({
					attribute_id: this.attribute.id,
					attribute_value_id: this.value.id,
					attribute: this.attribute.title,
					value: this.value.title
				});

			}).on('change:attribute', function(attribute) {
				$.ajax({ url: '/attributes/vals', data: {id: attribute.value.id},
					success: function(data) {
						$('#attribute-value').select2({
							data: { 
								results: data, 
								text: function(item) { 
									return item.title; 
								}
							},
							formatSelection: function(data) {
					        	productForms.arrays.attributeValues.value = data;
					        	return data.title;
					        }
						});
					}
				});
			})
		;

		if(data !== undefined)
			this.recreate(data);
	}
});

productForms.models.Combination = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.jset('artcode', data.artcode)
			.jset('pricewithtax', data.pricewithtax)
			.jset('priceperunit', data.priceperunit)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						pricewithtax: ':sandbox .price',
						artcode: ':sandbox .artcode',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					}).on('click::title click::pricewithtax click::artcode', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					}).linkProps('isDefault', 'is_default', function(is_default) {
						return is_default;
					}).bindNode('isDefault', ':sandbox', {
						setValue: function(v) {
							$(this).toggleClass('success', v);
						}
					})
				;
			})
		;
	}
});

productForms.arrays.Combinations = Class({
	'extends': MK.Array,
	itemRenderer: '#combinations-template',
	Model: productForms.models.Combination,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#combinations-control',
				container: ':sandbox #combinations-container>tbody',
				add: ':sandbox #combinations-add'
			}).on( '@readytodie', function( combination ) {
				var _ = this;
				$.ajax({ url: '/products/delete_combination/' + combination.id, method: 'delete',
					success: function(data) {
						_.pull( combination );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				productForms.forms.combinations.show();
			}).on('@clicked', function(combination) {
				productForms.forms.combinations.jset(combination.toJSON());
				console.log('>>>>>>>>>> ', combination.attributeValues);

				if(combination.attributeValues === undefined) {
					$.ajax({ url: '/products/' + combination.id + '/attributeValues', method: 'GET',
						success: function(data) {
							console.log('av', data, data.items);
							combination.attributeValues = JSON.parse(data.items);
							productForms.arrays.attributeValues.recreate(combination.attributeValues);
						}
					})
				} else {
					productForms.arrays.attributeValues.recreate(combination.attributeValues);
				}

				productForms.forms.combinations.show();
			})
			.recreate(data)
		;
	},
	addOrChange: function(data) {
		console.log(data);
		var newItem = true;
		for( var i = 0; i < this.length; i++ ) {
			if( this[i].id === data.id ) {
				this[i].jset(data)
					.jset('artcode', data.artcode)
					.jset('pricewithtax', data.pricewithtax)
					.jset('priceperunit', data.priceperunit);	
				newItem = false;
			}
			if(data.is_default && this[i].is_default && this[i].id !== data.id) {
				this[i].jset('is_default', false);
			}
		}

		if(newItem) {
			this.push(data);
		}
	}
});

productForms.forms.CombinationsForm = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-combinations',
				pricewithtax: ':sandbox input#pricewithtax',
				priceperunit: ':sandbox input#priceperunit',
				artcode: ':sandbox input#artcode',
				cancel: ':sandbox button.cancel',
				is_default: ':sandbox input#is_default'
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('click::cancel', function(e) {
				e.preventDefault();
				this.hide();
			}).linkProps('isDefault', 'is_default', function(is_default) {
				return is_default;
			})
		;
	},
	submitForm: function() {
		var _ = this,
			attribute_value_id = [];

		productForms.arrays.attributeValues.forEach(function(element, index, array) {
			console.log('....', element.attribute_value_id);
			attribute_value_id.push(element.attribute_value_id);
		});
		
		var combo = _.toJSON(),
			url = '/products/addCombos',
			method = 'post';
		
		if(combo.id !== undefined) {
			url = '/products/editCombo';
			method = 'put';
		}

		combo.attribute_value_id = attribute_value_id;

		$.ajax({ url: url, data: combo, method: method,
			success: function(data) {
				productForms.arrays.combinations.addOrChange(data.item);
				_.hide();
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
	},
	show: function() {
		$(this.sandbox).removeClass('hidden');
	},
	hide: function() {
		$(this.sandbox).addClass('hidden');
	}
});

productForms.models.Supplier = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				var _ = this;
				_
					.bindNode({
						title: ':sandbox .title',
						makedefault: ':sandbox a#makedefault',
						delete: ':sandbox a#delete'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					}).linkProps('isDefault', 'is_default', function(is_default) {
						return is_default;
					}).bindNode('isDefault', ':sandbox', {
						setValue: function(v) {
							$(this).toggleClass('success', v);
							$(this).find('a#makedefault').toggleClass('hidden', v);
						}
					}).on('click::makedefault', function(e) {
						this.trigger('makedefault', this);
					})
				;
			})
		;
	}
});

productForms.arrays.Suppliers = Class({
	'extends': MK.Array,
	// useBindingsParser: true,
	itemRenderer: '#suppliers-template',
	Model: productForms.models.Supplier,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#suppliers-control',
				container: ':sandbox #suppliers-container>tbody',
				add: ':sandbox #suppliers-add'
			}).on( '@readytodie', function( supplier ) {
				var _ = this;
				$.ajax({ url: '/products/delete_supplier/' + supplier.id, data: {product_id: productForms.item.id}, method: 'delete',
					success: function(data) {
						_.pull( supplier );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on( '@makedefault', function( supplier ) {
				var _ = this;
				$.ajax({ url: '/products/edit_supplier/', data: {product_id: productForms.item.id, supplier_id: supplier.id, is_default: true}, method: 'put',
					success: function(data) {
						_.map(function(item) {
							item.is_default = false;
						});
						supplier.is_default = true;
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				var _ = this;
				$.ajax({ url: '/products/add_supplier', data: {id: productForms.item.id, supplier_id: _.supplier.id}, method: 'post',
					success: function(data) {
						_.push(data.supplier);
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				})
				
			})
			.recreate(data)
		;
	}
});

productForms.models.Warehouse = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				var _ = this;
				_
					.bindNode({
						title: ':sandbox .title',
						delete: ':sandbox a#delete'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					}).linkProps('isSelected', 'is_selected', function(is_selected) {
						return is_selected;
					}).bindNode('isSelected', ':sandbox', {
						setValue: function(v) {
							$(this).toggleClass('success', v);
						}
					}).on('click::title', function(e) {
						this.trigger('select', this);
						// this.is_selected = true;
					})
				;
			})
		;
	}
});

productForms.arrays.Warehouses = Class({
	'extends': MK.Array,
	// useBindingsParser: true,
	itemRenderer: '#warehouses-template',
	Model: productForms.models.Warehouse,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#warehouses-control',
				container: ':sandbox #warehouses-container>tbody',
				add: ':sandbox #warehouses-add'
			}).on( '@readytodie', function( warehouse ) {
				var _ = this;
				$.ajax({ url: '/products/delete_warehouse/' + warehouse.id, data: {product_id: productForms.item.id}, method: 'delete',
					success: function(data) {
						_.pull( warehouse );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on( '@select', function( warehouse ) {
				var _ = this;
				$.ajax({ url: '/products/stock', data: {id: productForms.item.id, warehouse_id: warehouse.id}, method: 'get',
					success: function(data) {
						// showNotification(data);
						for (var i = _.length - 1; i >= 0; i--) {
							_[i].is_selected = false;
						}
						warehouse.is_selected = true;

						console.log('>>>', data);

						if(productForms.arrays.quantities === undefined)
							productForms.arrays.quantities = new productForms.arrays.Quantities(data);
						else
							productForms.arrays.quantities.recreate(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				var _ = this;
				$.ajax({ url: '/products/add_warehouse', data: {id: productForms.item.id, warehouse_id: _.warehouse.id}, method: 'post',
					success: function(data) {
						_.push(data.warehouse);
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				})
				
			})
			.recreate(data)
		;
	}
});

productForms.models.Quantity = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				var _ = this;
				_
					.bindNode({
						title: ':sandbox .title'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).bindNode({
						amount: ':sandbox input#amount'
					}, {
						setValue: function(v) {
							this.value = v;
						}
					}).on('change::amount', function(e) {
						this.trigger('amountchanged', this);
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					}).linkProps('isDefault', 'is_default', function(is_default) {
						return is_default;
					}).bindNode('isDefault', ':sandbox', {
						setValue: function(v) {
							$(this).toggleClass('success', v);
							$(this).find('a#makedefault').toggleClass('hidden', v);
						}
					}).on('click::title', function(e) {
						this.trigger('select', this);
					})
				;
			})
		;
	}
});

productForms.arrays.Quantities = Class({
	'extends': MK.Array,
	// useBindingsParser: true,
	itemRenderer: '#quantities-template',
	Model: productForms.models.Quantity,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#quantities-control',
				container: ':sandbox #quantities-container>tbody',
			}).on('@amountchanged', function(qty) {
				$.ajax({ url: '/products/edit_stock_amount', data: {product_id: qty.id, warehouse_id: qty.warehouse_id, amount: qty.amount}, method: 'put',
					success: function(data) {
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on( '@select', function( warehouse ) {
				var _ = this;
				$.ajax({ url: '/products/stock', data: {id: productForms.item.id, warehouse_id: warehouse.id}, method: 'get',
					success: function(data) {
						// showNotification(data);
						console.log(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				var _ = this;
				$.ajax({ url: '/products/add_warehouse', data: {id: productForms.item.id, warehouse_id: _.warehouse.id}, method: 'post',
					success: function(data) {
						_.push(data.warehouse);
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				})
				
			})
			.recreate(data)
		;
	}
});

productForms.forms.StockForm = Class({
	'extends': MK.Object,
	constructor: function(data) {
		data.when_out_of_stock = data.when_out_of_stock == null ? 'DEFAULT' : data.when_out_of_stock;
		data.pack_quantities = data.pack_quantities == null ? 'DEFAULT' : data.pack_quantities;

		this.jset(data)
			.bindNode({
				sandbox: '#form-stock',
				when_out_of_stock: ':sandbox input[name=when_out_of_stock]',
				pack_quantities: ':sandbox input[name=pack_quantities]'
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			})
		;
	},
	submitForm: function() {
		$.ajax({url: '/products/edit_stock_settings', data: this.toJSON(), method: 'put',
			success: function(data) {
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});

		return this;
	}
});

window.showNotification = function(data) {
	$('body').pgNotification({
        style: 'bar',
        message: data.message,
        position: 'top',
        timeout: 3000,
        type: data.alert
    }).show();
}