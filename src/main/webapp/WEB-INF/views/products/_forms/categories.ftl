<#ftl encoding="utf-8">

<div class="panel-heading separator">
    <div class="panel-title">Категория</div>
</div>
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
			<form id="form-categories" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" autocomplete="off">
				<div class="form-group">
          <label for="title" class="col-sm-3 control-label">Категория товара</label>
          <div class="col-sm-9">
            <p class="category-title"></p>
          </div>
        </div>
				<div class="form-group">
          <label for="title" class="col-sm-3 control-label">Выберите категорию товара</label>
          <div class="col-sm-9">
            <div id="categories-tree"></div>
          </div>
        </div>

				<br>
				<div class="row">
					<div class="col-sm-3">
						<p>I hereby certify that the information above is true and accurate. </p>
					</div>
					<div class="col-sm-9">
						<button class="btn btn-success submit" type="submit">Сохранить и продолжить</button>
						<button class="btn btn-default"><i class="pg-close"></i> Очистить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>