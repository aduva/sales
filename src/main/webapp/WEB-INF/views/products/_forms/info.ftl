 <#ftl encoding="utf-8">
<div class="panel-heading separator">
    <div class="panel-title">Информация</div>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <form id="form-info" action="${action!"/products/save"}" method="${method!"post"}" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" autocomplete="off">
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label">Наименование</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="title" placeholder="Полное наименование" name="title" required>
						</div>
					</div>
					<div class="form-group">
						<label for="artcode" class="col-sm-3 control-label">Артикул</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="artcode" placeholder="Артикул товара" name="artcode" required>
						</div>
					</div>
					<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
						<div class="row">
							<label class="col-sm-3 control-label">Тип товара</label>
							<div class="col-sm-9">
								<div class="radio radio-success">
									<input type="radio" value="PARTIAL" name="type_code" id="partial" >
									<label for="partial">Составная часть товара</label><span class="help">(Продается только в составе других товаров: ингредиенты, инвентарь и т.п.)</span><br>
									<input type="radio" value="DEFAULT" name="type_code" id="default" checked>
									<label for="default">Обычный товар</label><br>
									<#-- <input type="radio" value="COMPOSITE" name="type_code" id="composite" > -->
									<#-- <label for="composite">Комплект товаров</label><br> -->
									<input type="radio" value="VIRTUAL" name="type_code" id="virtual" >
									<label for="virtual">Виртуальный товар</label><span class="help">(услуги, скачиваемые товары и т.п.)</span>
								</div>
							</div>
						</div>
						<div id="composite-control" class="hidden">
							<hr class="b-grey b-dashed">
							<div class="row hidden" id="alertrow">
								<div class="col-md-6"><br>
								  <div class="alert alert-warning" id="composite-alert" role="alert">
								    <p><i class="fa fa-warning"></i> Комплект пуст. Вы должны добавить хотя бы один товар в комплект.</p>
								  </div>
								</div>
							</div>
							<div class="row"><label class="col-sm-3 control-label">Товары в комплекте</label>
								<div class="col-sm-9"><table id="composite-table" class="table table-condensed">
									<thead>
										<th>Товар</th><th>Количество</th><th></th>
									</thead>
									<tbody id="composite-items">
									</tbody>
								</table></div>
							</div>
							<hr class="b-grey b-dashed">
							
							<div class="row">
								<label class="col-sm-3 control-label">Добавить товар в комплект</label>
								<div class="col-sm-3">
									<input class="composite-products-select col-sm-12" id="composite-product" type="hidden">
						    </div>
						    <div class="col-sm-3">
						    	<input type="number" value="1" id="composite-qty" class="form-control" placeholder="Количество">
						    </div>
						    <div class="col-sm-3">
						    	<a href="#" class="btn btn-default" id="composite-add"><i class="fa fa-plus"></i> Добавить</a>
						    </div>
							</div>
						</div>
					</div>
					<br><br>
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-success submit" type="submit"><i class="fa fa-save"></i> Сохранить и продолжить</button>
							<button class="btn btn-default pull-right"><i class="pg-close"></i> Очистить</button>
						</div>
					</div>
				</form>
      </div>
    </div>
  </div>
<script id="composite-template" type="text/html">
<tr><td class="title"></td><td class="qty"></td><td><a href="#" class="btn btn-sm"><i class="fa fa-trash"></i> Удалить</a></td></tr>
</script>