<#ftl encoding="utf-8">

<div class="panel-heading separator">
    <div class="panel-title">Комплектация</div>
</div>
<div class="panel-body" id="set-control">
	<div class="row">
		<div class="col-md-12">
      <form id="form-set" action="/products/save" method="post" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10 hidden" role="form" autocomplete="off">
				<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
					<div class="row">
						<label for="title" class="col-sm-3 control-label">Выбрать товар</label>
						<div class="col-sm-9">
							<input type="hidden" class="full-width" id="setproduct">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="price" class="col-sm-3 control-label">Количество</label>
					<div class="col-sm-9">
						<input type="number" step="0.01" value="0" class="form-control" id="amount" placeholder="Цена товара" name="amount" required>
					</div>
				</div>

				<div class="form-group">
					<label for="priceperunit" class="col-sm-3 control-label">Цена за единицу в комплекте</label>
					<div class="col-sm-9">
						<span class="help">Значение равно 0, если цена не меняется</span>
						<input type="number" step="0.01" value="0" class="form-control" id="priceperunit" placeholder="Цена товара" name="priceperunit" required>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-12">
						<button class="btn btn-success submit" type="submit"><i class="fa fa-save"></i> Сохранить и продолжить</button>
						<button class="btn btn-default cancel"><i class="fa fa-ban"></i> Отмена</button>
					</div>
				</div>
				<br><hr class="b-dashed"><br>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 p-l-30 p-r-30 sm-p-l-10 sm-p-r-10">
			<h4>Товары в комплекте</h4>
    	<table class="table table-condensed table-hover" id="set-container">
    		<thead>
    			<th>Наименование</th>
    			<th>Артикул</th>
    			<th>Цена за еденицу</th>
    			<th>Количество</th>
    			<th></th>
    		</thead>
    		<tbody></tbody>
    	</table>
      
      <br>
      
			<button class="btn btn-primary pull-right" id="set-add"><i class="fa fa-plus"></i> Добавить товар в комплект</button>
		</div>
	</div>
</div>

<script type="text/html" id="set-template">
<tr class="pointer">
	<td class="title"></td>
	<td class="artcode"></td>
	<td class="price"></td>
	<td class="amount"></td>
	<td><a href="#" class="btn btn-sm delete"><i class="fa fa-trash"></i> Удалить</a></td>
</tr>
</script>