<#ftl encoding="utf-8">

<div class="panel-heading separator">
    <div class="panel-title">Цены</div>
</div>
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
			<form id="form-prices" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" autocomplete="off">
				<div class="form-group">
          <label class="control-label col-sm-3">Себестоимость</label>
          <span class="help">Цена за которую вы приобрели товар</span>
          <div class="col-sm-9">
          	<input type="number" step="0.01" id="cost" name="cost" class="form-control" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3">Цена без НДС</label>
          <span class="help">Цена за которую вы собираетесь продавать товар. Разница между ценой и себестоимостью будет вашей прибылью.</span>
          <div class="col-sm-9">
          	<input type="number" step="0.01" id="pricePreTax" name="pricePreTax" class="form-control" required>
          </div>
        </div>
        <div class="form-group form-group-default-select2 required">
          <label class="control-label col-sm-3" class="">Ставка НДС</label>
          <div class="col-md-9">
	          <input type="hidden" id="tax" class="taxes-select" style="width:100%;">
	        </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3">Цена с НДС</label>
          <div class="col-sm-9">
          	<input type="number" step="0.01" id="priceWithTax" name="priceWithTax" class="form-control" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3">Цена за единицу в комплекте</label>
          <span class="help">Когда продаете товар в комплекте вы можете указать цену за единицу: "за одну бутылку", "за одну штуку"</span>
          <div class="col-sm-9"><div class="input-group">
            <input type="number" step="0.01" class="form-control" id="pricePerUnit" name="pricePerUnit">
            <span class="input-group-addon">за</span>
            <input type="text" class="form-control" id="unitName" name="unitName">
          </div></div>
        </div>

				<br>
				<div class="row">
					<div class="col-sm-9 offset-col-md-3">
						<button class="btn btn-success submit" type="submit">Сохранить и продолжить</button>
						<button class="btn btn-default"><i class="pg-close"></i> Очистить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>