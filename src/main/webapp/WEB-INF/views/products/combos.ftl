<#ftl encoding="utf-8">
<ul class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li><a href="/products" class="active">Товары</a></li>
</ul>
<div class="row">
    <#-- list -->
    <div class="col-md-12">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <input type="text" class="form-control pull-right" placeholder="быстрый поиск по артикулу">
                    </div>
                    <div class="col-xs-6">
                        <link_to action="add" class="btn btn-primary btn-cons pull-right" style="color:#fff !important;opacity:1;"><i class="fa fa-plus"></i> Добавить товар</link_to>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                
                    <table class="table">
                        <thead>
                            <th>ID</th>
                            <th>Наименование</th>
                            <th>Артикул</th>
                            <th>Цена</th>
                            <th>Количество</th>
                            <th>Статус</th>
                        </thead>
                        <tbody>
                            <@render partial="items" items=items/>
                        </tbody>
                    </table>
                    <div class="row">
                        
                    </div>
               
            </div>
        </div>
    </div>
    <#-- end list -->
</div>


<@content for="title">Products</@content>
<@content for="js">
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('li#menu-catalog, li#menu-products').addClass('active open');
    });
</script>
</@content>