<#ftl encoding="utf-8">
<ul class="breadcrumb">
  <li><a href="/">Главная</a></li>
  <li><a href="/products">Товары</a></li>
  <#assign id=0/>
  <#assign method="post"/>
  
  <#if item??>
    <li><a id="currentBreadcrumb" href="/products/show/${item.id}" class="active">${item.title}</a></li>
    <#assign id=item.id/>
    <#assign method="put"/>
    
  <#else>
    <#assign tabHref="#savefirst"/>
    <li><a id="currentBreadcrumb" href="/products/add" class="active">Добавить товар</a></li>
  </#if>
</ul>
<div class="row">
    <#-- sidebar menu panel -->
  <div class="col-md-2">
    <div class="panel panel-default">
      <div class="panel-body">
        <ul class="nav nav-tabs nav-tabs-simple nav-tabs-right bg-white pull-left" id="tab-3">
          <li class="active"><a data-toggle="tab" href="#info">Информация</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#prices"}" data-href="#prices">Цены</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#categories"}" data-href="#categories">Категория</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#combinations"}" data-href="#combinations">Комбинации</a></li>
          <li id="composite-menu"><a data-toggle="tab" href="${tabHref!"#composites"}" data-href="#composites">Товары в комплекте</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#suppliers"}" data-href="#suppliers">Поставщики</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#stock"}" data-href="#stock">Складской учет</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#warehouses"}" data-href="#warehouses">Складские помещения</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#images"}" data-href="#images">Изображения</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#features"}" data-href="#features">Особенности</a></li>
          <li><a data-toggle="tab" href="${tabHref!"#customization"}" data-href="#customization">Кастомизация</a></li>
        </ul>
      </div>
    </div>
  </div>

  <#-- end sidebar menu panel -->
  
  <#-- form panel -->
  <div class="col-md-10">
    <div class="alert alert-${alert!"danger"} <#if !(alert??)>hidden</#if>" id="flash" role="alert">
      <button class="close" data-dismiss="alert"></button>
      <@flash name="message"/>
    </div>
    <div class="panel panel-default">
      <div class="tab-content bg-white">
        <div class="tab-pane active" id="info">
          <@render partial="forms/info"/>
        </div>

        <div class="tab-pane" id="prices">
          <@render partial="forms/prices" />
        </div>

        <div class="tab-pane" id="categories">
          <@render partial="forms/categories" />
        </div>

        <div class="tab-pane" id="combinations">
          <@render partial="forms/combinations" />
        </div>

        <div class="tab-pane" id="composites">
          <@render partial="forms/composites" />
        </div>

        <div class="tab-pane" id="suppliers">
          <@render partial="forms/suppliers" />
        </div>

        <div class="tab-pane" id="stock">
          <@render partial="forms/stock" />
        </div>

        <div class="tab-pane" id="warehouses">
          <@render partial="forms/warehouses" />
        </div>

        <div class="tab-pane" id="savefirst">
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6"><br>
                <div class="alert alert-warning" role="alert">
                  <p class="pull-left"><b><i class="fa fa-warning"></i> Внимание!</b></p>
                  
                  <div class="clearfix"></div>
                  <br>
                  <p>Чтобы продолжить Вы должны сохранить данный товар.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <#-- end form panel -->
</div>


<@content for="title">Добавить товар</@content>
<@content for="js">
<script src="${context_path}/plugins/jquery-dynatree/jquery.dynatree.min.js" type="text/javascript"></script>
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>
<!--script src="${context_path}/js/products/forms/main.js" type="text/javascript"></script-->
<script>
  $(document).ready(function() {
    $('li#menu-catalog, li#menu-products').addClass('active open');
  
    <@render partial="forms/newjs"/>
    var item = {"title": '', "artcode":'', "type_code":'', "id": undefined, 'cost': undefined, 'pricewithtax': undefined, 'pricepretax': undefined, 'priceperunit': undefined, 'unitname': undefined, 'tax_id': undefined};
    <#if item??> item = ${item.toJson(false)} </#if>;
    product.init(item);
    
    <#if item??>

      $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        switch($(e.target).attr('href')) {
        case '#prices':
          if(product.forms.prices === undefined) {
            product.initPrices();
          }
          break;
        case '#categories':
          if(product.forms.categories === undefined) {
            product.initCategory();
          }
          break;
        case '#combinations':
          if(product.arrays.combos === undefined) {
            product.initCombos();
          }
          break;
        case '#composites':
          if(product.arrays.sets === undefined) {
            product.initComposites();
          }
          break;
        case '#suppliers':
          if(product.arrays.suppliers === undefined) {
            <#if item??>
              if(product.item === undefined) {
                product.item = ${item.toJson(false)};
              }
            </#if>

            var suppliers;
            <#if suppliers??>
              suppliers = JSON.parse('${suppliers}');
            </#if>
            product.initSuppliersForm(suppliers);
          }
          break;
        case '#warehouses':
          if(product.arrays.warehouses === undefined) {
            product.initWarehouses();
          }
          break;
        case '#stock':
          if(product.forms.stock === undefined) {
            product.initStockForm();
          }
          break;
        }
      });
    </#if>
});

</script>
</@content>

<@content for="styles">
<link href="${context_path}/plugins/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css" media="screen" />
</@content>