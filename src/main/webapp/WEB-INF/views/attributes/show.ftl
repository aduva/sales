
<@content for="title">Attribute: ${item.title}</@content>



<@link_to>Back to all attributes</@link_to>
<@link_to action="new_value_form" id="${item.id}">Add Attribute Value</@link_to>




<h2>Attribute: "${item.title}"</h2>

<table>
    <tr>
        <td>Title</td>
        <td>Edit</td>
    </tr>
<#list values as value>
    <tr>
        <td>
            ${value.title}
        </td>
        <td>
            <@confirm text="Are you sure you want to delete this value: " + value.title + "?" form=value.id>Delete</@confirm>
            <@form  id=value.id action="deleteValue" method="delete" html_id=value.id />
        </td>
    </tr>
</#list>
</table>