<#ftl encoding="utf-8">
<ul class="breadcrumb">
  <li><a href="/">Главная</a></li>
  <li><a href="/categories">Категории</a></li>
  <li><a href="/categories/add" class="active">Добавить категорию</a></li>
</ul>
<div class="row">
    
  <#-- form panel -->
  <div class="col-md-10">
    <div class="panel panel-default">
      <@render partial="forms/add"/>
    </div>
  </div>
  <#-- end form panel -->
</div>


<@content for="title">Добавить категорию</@content>
<@content for="js">
<script src="${context_path}/plugins/jquery-dynatree/jquery.dynatree.min.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
      $('form#form-categories').on('change', function(e) {
        console.log('change', $(this).serializeArray());
      });

      $('#clear-form').on('click', function() {
        $('form').trigger('reset');
      });

      $('li#menu-catalog, li#menu-categories').addClass('active open');

      $("#categories-tree").dynatree({
        checkbox: true,
        // Override class name for checkbox icon:
        classNames: { checkbox: "dynatree-radio" },
        selectMode: 1,
        initAjax: { url: '/categories/tree?dynatree=true' },
        onActivate: function(node) {
          node.toggleExpand();
        },
        onLazyRead: function(node) {
          node.appendAjax({
            url: '/categories/tree?dynatree=true',
            data: {
              id: node.data.id
            },
            success: function(data) {
              console.log(data);
            }
          });
        },
        onSelect: function(select, node) {
          $('input#parent').val(node.data.id);
          node.activateSilently();
        },
        onDblClick: function(node, event) {
          node.toggleSelect();
        }
      });
    });
</script>
</@content>

<@content for="styles">
<link href="${context_path}/plugins/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css" media="screen" />
</@content>