<#ftl encoding="utf-8">
<ul class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li><a href="/categories">Категории</a></li>
    <li><a href="/categories/show/${item.id}" class="active">${item.title}</a></li>
</ul>
<div class="row">
    <#-- list -->
    <div class="col-md-12">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <input type="text" class="form-control pull-right" placeholder="быстрый поиск по наименованию">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <h3>${item.title}</h3>
            </div>
        </div>
    </div>
    <#-- end list -->
</div>


<@content for="title">Products</@content>
<@content for="js">
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('li#menu-catalog, li#menu-categories').addClass('active open');
    });
</script>
</@content>