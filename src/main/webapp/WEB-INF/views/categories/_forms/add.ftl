<#ftl encoding="utf-8">

<div class="panel-heading separator">
   <div class="panel-title">Категория</div>
</div>
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
			<form id="form-categories" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" action="/categories/create" autocomplete="off" method="post">
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Наименование</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="title" placeholder="Полное наименование" name="title" required>
					</div>
				</div>

				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">URL</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="url" placeholder="URL" name="url" required>
					</div>
				</div>
							
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Родительский элемент</label>
					<div class="col-sm-9">
						<input type="hidden" name="parent_id" id="parent">
						<div id="categories-tree"></div>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-9 col-md-offset-3">
						<button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Сохранить</button>
						<button class="btn btn-default" id="clear-form"><i class="pg-close"></i> Очистить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>