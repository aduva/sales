<@content for="title">Add new combination</@content>

<span class="error_message"><@flash name="message"/></span>
<h2>Adding new combination</h2>


<@form action="addCombination" method="post">
    <input type="text" name="product_id" value="${product}">
    <table style="margin:30px">
        <tr>
            <td>Attribute:</td>
            <td>
                <select name="attribute" id="attribute">
                    <#list attributes as item>
                        <option value="${item.id}">${item.title}</option>
                    </#list> 
                </select>
            </td>
        </tr>
        <tr>
            <td>Values:</td>
            <td>
                <select name="value" id="value"></select><a href="#" id="addValue">Add</a>
            </td>
        </tr>
        <tr>
            <td>Combinations:</td>
            <td>
                <select name="combinations" id="combinations" multiple="multiple"></select>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><@link_to>Cancel</@link_to> | <input type="submit" value="Add new combination"></td>

        </tr>
    </table>
</@form>

<@content for="js">
<script type="text/javascript">
    $(document).ready(function() {
        var id = $('select#attribute').find('option:selected').val();
        loadValueSelect(id);
    });

    $('select#attribute').change(function(e) {
        var id = $(this).find('option:selected').val();
        loadValueSelect(id);
    });

    $('#addValue').on('click', function(e){
        e.preventDefault();
        var selectedValue = $('select#value').find('option:selected');
            title = $('select#attribute').find("option:selected").text() + ": " + selectedValue.text(),
            el = '<option value="' + selectedValue.val() + '">' + title + "</option>",
            input = '<input type="hidden" name="combo[]" value="' + selectedValue.val() + '">';
        console.log(el);
        $('form').append(input);
        $('select#combinations').append(el);
    });

    $('form').on('submit', function() {
        $('select#combinations').find('option').prop('selected', true);
    })

    function loadValueSelect(id) {
        $.ajax({
            url: '/attributes/show_values/' + id,
            type: 'get',
            success: function(el) {
                console.log(el);
                $('select#value').html(el);
            }
        });
    }
</script>
</@>