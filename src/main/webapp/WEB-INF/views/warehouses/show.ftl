<#ftl encoding="utf-8">
<ul class="breadcrumb">
  <li><a href="/">Главная</a></li>
  <li><a href="/warehouses">Складские помещения</a></li>
  <li><a href="/warehouses/${item.id}/show" class="active">${item.artcode}: ${item.title}</a></li>
</ul>
<div class="row">
    
  <#-- form panel -->
  <div class="col-md-10">
    <div class="panel panel-default">
      <@render partial="forms/add" action="/warehouses/update/${item.id}" title="${item.title}" artcode="${item.artcode}"/>
    </div>
  </div>
  <#-- end form panel -->
</div>


<@content for="title">Добавить помещение</@content>
<@content for="js">
<script>
    $(document).ready(function() {
      $('#clear-form').on('click', function() {
        $('form').trigger('reset');
      });

      $('li#menu-stock, li#menu-warehouses').addClass('active open');
</script>
</@content>