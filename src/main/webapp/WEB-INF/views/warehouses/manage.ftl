<#ftl encoding="utf-8">
<ul class="breadcrumb">
  <li><a href="/">Главная</a></li>
  <li><a href="/warehouses">Складские помещения</a></li>
  <li><a href="/warehouses/management" class="active">Добавить помещение</a></li>
</ul>
<div class="row">
    
  <#-- form panel -->
  <div class="col-md-10">
    <div class="panel panel-default">
      <div class="panel-heading separator">
         <div class="panel-title">Складское помещение</div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <form id="form-warehouses" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" action="/warehouses/updateStock?id=${product.id}" method="post" autocomplete="off">
              <div class="form-group">
                <label for="title" class="col-sm-3 control-label">Наименование</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control disabled" id="title" placeholder="Полное наименование" name="title" value="${product.title}" disabled>
                </div>
              </div>
                    
              <div class="form-group">
                <label for="title" class="col-sm-3 control-label">Артикул</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control disabled" id="title" placeholder="Артикул" name="artcode" value="${product.artcode}" disabled>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-3 control-label">Нужда</label>
                <div class="col-sm-9">
                  <div class="radio radio-success">
                    <input type="radio" value="INCREASE" name="manage_type_code" id="increase" checked>
                    <label for="increase">Добавить товар</label><br>
                    <input type="radio" value="DECREASE" name="manage_type_code" id="decrease">
                    <label for="decrease">Сократить товар</label><br>
                    <input type="radio" value="TRANSFER" name="manage_type_code" id="transfer" >
                    <label for="transfer">Переместить товар</label><span class="help">(перемещение из одного склада в другой)</span>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="amount" class="col-sm-3 control-label">Количество</label>
                <div class="col-sm-9">
                  <input type="number" step="0.1" class="form-control disabled" id="amount" placeholder="Количество" name="amount" value="" required>
                </div>
              </div>

              <div class="form-group">
                <label for="warehouseSource" class="col-sm-3 control-label">Исходное помещение</label>
                <div class="col-sm-9">
                  <input type="hidden" name="source" id="warehouseSource" class="warehouse-select" style="width:100%;">
                </div>
              </div>

              <div class="form-group hidden" id="warehouseTargetForm">
                <label for="warehouseTarget" class="col-sm-3 control-label">Целевое помещение</label>
                <div class="col-sm-9">
                  <input type="hidden" name="target" id="warehouseTarget" class="warehouse-select" style="width:100%;">
                </div>
              </div>

              <br>
              <div class="row">
                <div class="col-sm-9 col-md-offset-3">
                  <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Готово</button>
                  <a href="/warehouses/management" class="btn btn-default"><i class="pg-close"></i> Отмена</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <#-- end form panel -->
</div>


<@content for="title">Добавить помещение</@content>
<@content for="js">
<script>
    $(document).ready(function() {
      $('#clear-form').on('click', function() {
        $('form').trigger('reset');
      });

      $('li#menu-stock, li#menu-warehouses').addClass('active open');

      $('input[name=manage_type_code]').change(function() {
        console.log('......', $(this).attr('id'));
        if($(this).attr('id') === 'transfer') {
          $('#warehouseTargetForm').removeClass('hidden');
        } else {
          if(!$('#warehouseTargetForm').hasClass('hidden'))
            $('#warehouseTargetForm').addClass('hidden');
        }
      });

      $('.warehouse-select').select2({
        ajax: {
          url: "/warehouses", 
          dataType: 'json', 
          delay: 250,
          data: function (term, page) {
            return { q:  term, page: page };
          },
          results: function (data, page) {
            return { results: data.items };
          },
          cache: true
        }, 
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 0,
        formatResult: formatSelectResult,
        formatSelection: formatSelectResult
      });

      function formatSelectResult(data) {
        return data.title.concat(": ", data.artcode);
      }
    });
</script>
</@content>