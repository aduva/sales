<#ftl encoding="utf-8">
<ul class="breadcrumb">
    <li><a href="/">Главная</a></li>
    <li><a href="/warehouses">Склады</a></li>
    <li><a href="/warehouses/management" class="active"><@msg key="menu.stock.management"/> </a></li>
</ul>
<div class="row">
    <#-- list -->
    <div class="col-md-12">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <input type="text" class="form-control pull-right" placeholder="быстрый поиск">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                
                    <table class="table">
                        <thead>
                            <th><@msg key="products.id"/></th>
                            <th><@msg key="products.title"/></th>
                            <th><@msg key="products.artcode"/></th>
                            <th><@msg key="products.stock.amount"/></th>
                        </thead>
                        <tbody>
                            <#list items as item>
                            <tr>
                                <td>${item.id}</td>
                                <td>${item.title}</td>
                                <td>${item.artcode}</td>
                                <td><#if (item.combos > 0)>
                                        ${item.stock} 
                                    <#else>
                                        -
                                    </#if>
                                </td>
                                <td>
                                <#if (item.combos > 0)>
                                    <a class="btn btn-default btn-xs" href="/warehouses/management?pid=${item.id}"><i class="fa fa-search"></i> Детали</a>
                                <#else>
                                    <a class="btn btn-default btn-xs" href="/warehouses/${item.id}/manage"><i class="fa fa-cog"></i> Управлять</a>
                                </#if>
                                </td>
                            </tr>
                            </#list>
                        </tbody>
                    </table>
               
            </div>
        </div>
    </div>
    <#-- end list -->
</div>


<@content for="title">Warehouses</@content>
<@content for="js">
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/datatables.responsive.js"></script>
<script type="text/javascript" src="${context_path}/plugins/datatables-responsive/js/lodash.min.js"></script>
<script>
    $(document).ready(function() {
        $('li#menu-stock, li#menu-management').addClass('active open');
    });
</script>
</@content>