<#ftl encoding="utf-8">

<div class="panel-heading separator">
   <div class="panel-title">Складское помещение</div>
</div>
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
			<form id="form-warehouses" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" action="${action!"/warehouses/save"}" method="${method!"post"}" autocomplete="off">
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Наименование</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="title" placeholder="Полное наименование" name="title" value="${title!""}" required>
					</div>
				</div>
							
				<div class="form-group">
					<label for="title" class="col-sm-3 control-label">Артикул</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="title" placeholder="Артикул" name="artcode" value="${artcode!""}" required>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-9 col-md-offset-3">
						<button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Сохранить</button>
						<a href="/warehouses" class="btn btn-default"><i class="pg-close"></i> Отмена</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>