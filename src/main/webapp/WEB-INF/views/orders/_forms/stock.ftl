 <#ftl encoding="utf-8">
<div class="panel-heading separator">
    <div class="panel-title">Складской учет</div>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <form id="form-stock" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10" role="form" autocomplete="off">
					<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
						<div class="row">
							<label class="col-sm-3 control-label">Когда нет запасов</label>
							<div class="col-sm-9">
								<div class="radio radio-success">
									<input type="radio" value="ALLOW" name="when_out_of_stock" id="allow" >
									<label for="allow">Разрешать заказы</label><br>
									<input type="radio" value="DENY" name="when_out_of_stock" id="deny" >
									<label for="deny">Не разрешать заказы</label><br>
									<input type="radio" value="DEFAULT" name="when_out_of_stock" id="wofs-default" checked>
									<label for="wofs-default">По умолчанию: <#if stockDefaults?? && stockDefaults.when_out_of_stock == 'DENY'>Не разрешать заказы<#else>Разрешать заказы</#if></label><span class="help">(<a href="#">Согласно заданным настройкам <i class="fa fa-external-link-square"></i></a>)</span><br>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
						<div class="row">
							<label class="col-sm-3 control-label">Количество в комплекте</label>
							<div class="col-sm-9">
								<div class="radio radio-success">
									<input type="radio" value="DEC_PACK_ONLY" name="pack_quantities" id="packonly" >
									<label for="packonly">Уменшать только количество комплектов</label><br>
									<input type="radio" value="DEC_PROD_IN_PACK" name="pack_quantities" id="prodinpack" >
									<label for="prodinpack">Уменшать составляющие товары в комплекте</label><br>
									<input type="radio" value="DEC_BOTH" name="pack_quantities" id="both" >
									<label for="both">Уменшать и то, и другое</label><br>
									<input type="radio" value="DEFAULT" name="pack_quantities" id="pq-default" checked>
									<label for="pq-default">По умолчанию: <#if stockDefaults?? && stockDefaults.pack_quantities == 'DEC_PACK_ONLY'>Уменшать только количество комплектов</#if><#if stockDefaults?? && stockDefaults.pack_quantities == 'DEC_BOTH'>Уменшать и то, и другое</#if><#if stockDefaults?? && stockDefaults.pack_quantities == 'DEC_PROD_IN_PACK'>Уменшать составляющие товары в комплекте</#if></label><span class="help">(<a href="#">Согласно заданным настройкам <i class="fa fa-external-link-square"></i></a>)</span><br>
								</div>
							</div>
						</div>
					</div>
					<br><br>
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-success submit" type="submit"><i class="fa fa-save"></i> Сохранить и продолжить</button>
							<button class="btn btn-default pull-right"><i class="pg-close"></i> Очистить</button>
						</div>
					</div>
				</form>
      </div>
    </div>
  </div>
<script id="composite-template" type="text/html">
<tr><td class="title"></td><td class="qty"></td><td><a href="#" class="btn btn-sm"><i class="fa fa-trash"></i> Удалить</a></td></tr>
</script>