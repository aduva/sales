<#ftl encoding="utf-8">

<div class="panel-heading separator">
    <div class="panel-title">Комбинации</div>
</div>
<div class="panel-body">
	<div class="row" id="attributes-control">
		<div class="col-md-12">
      <form id="form-combinations" action="${action!"/products/save"}" method="${method!"post"}" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10 hidden" role="form" autocomplete="off">
				<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
					<div class="row">
						<label for="title" class="col-sm-3 control-label">Добавить атрибут</label>
						<div class="col-sm-3">
							<input type="hidden" class="full-width" id="attribute">
						</div>
						<div class="col-sm-3">
							<input type="hidden" id="attribute-value" class="full-width">
						</div>
						<div class="col-sm-3">
							<a href="#" id="attribute-add" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Добавить</a>
						</div>
					</div>
					<hr class="b-dashed">
					<div class="row">
						<label for="title" class="col-sm-3 control-label">Атрибуты комбинации</label>
						<div class="col-sm-9 col-sm-offset-3">
							<table class="table table-condensed" id="attributes-container">
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="artcode" class="col-sm-3 control-label">Артикул</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="artcode" placeholder="Артикул товара" name="artcode" required>
					</div>
				</div>

				<div class="form-group">
					<label for="price" class="col-sm-3 control-label">Цена</label>
					<div class="col-sm-9">
						<span class="help">Значение равно 0, если цена не меняется</span>
						<input type="number" step="0.01" value="0" class="form-control" id="pricewithtax" placeholder="Цена товара" name="pricewithtax" required>
					</div>
				</div>

				<div class="form-group">
					<label for="priceperunit" class="col-sm-3 control-label">Цена за единицу в комплекте</label>
					<div class="col-sm-9">
						<span class="help">Значение равно 0, если цена не меняется</span>
						<input type="number" step="0.01" value="0" class="form-control" id="priceperunit" placeholder="Цена товара" name="priceperunit" required>
					</div>
				</div>

				<div class="form-group">
					<label for="is_default" class="col-sm-3 control-label">Основная комбинация</label>
					<div class="checkbox check-success checkbox-circle">
						<input type="checkbox" value="true" id="is_default">
						<label for="is_default">Сделать комбинацию основным</label>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<button class="btn btn-success submit" type="submit"><i class="fa fa-save"></i> Сохранить и продолжить</button>
						<button class="btn btn-default cancel"><i class="fa fa-ban"></i> Отмена</button>
					</div>
				</div>
				<br><hr class="b-dashed"><br>
			</form>
		</div>
	</div>
	<div class="row" id="combinations-control">
		<div class="col-md-12 p-l-30 p-r-30 sm-p-l-10 sm-p-r-10">
			<h4>Комбинации товара</h4>
    	<table class="table table-condensed table-hover" id="combinations-container">
    		<thead>
    			<th>Атрибут-значение</th>
    			<th>Цена</th>
    			<th>Артикул</th>
    			<th></th>
    		</thead>
    		<tbody></tbody>
    	</table>
      
      <br>
      
			<button class="btn btn-primary pull-right" id="combinations-add"><i class="fa fa-plus"></i> Добавить комбинацию</button>
		</div>
	</div>
</div>

<script type="text/html" id="attributes-template">
<tr class="pointer">
	<td class="attribute"></td>
	<td class="value"></td>
	<td><a href="#" class="btn btn-sm delete"><i class="fa fa-trash"></i> Удалить</a></td>
</tr>
</script>

<script type="text/html" id="combinations-template">
<tr class="pointer">
	<td class="title"></td>
	<td class="price"></td>
	<td class="artcode"></td>
	<td><a href="#" class="btn btn-sm delete"><i class="fa fa-trash"></i> Удалить</a></td>
</tr>
</script>