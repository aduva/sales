<#ftl encoding="utf-8">

<div class="panel-heading separator">
    <div class="panel-title">Складские помещения</div>
</div>
<div class="panel-body" id="warehouse-control">
	<div class="row">
		<div class="col-md-12">
      <form id="form-warehouse" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10 hidden" role="form" autocomplete="off">
				<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
					<div class="row">
						<label for="title" class="col-sm-3 control-label">Выбрать помещение</label>
						<div class="col-sm-9">
							<input type="hidden" class="full-width" id="warehouseselect">
						</div>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-12">
						<button class="btn btn-success submit" type="submit"><i class="fa fa-save"></i> Сохранить и продолжить</button>
						<button class="btn btn-default cancel"><i class="fa fa-ban"></i> Отмена</button>
					</div>
				</div>
				<br><hr class="b-dashed"><br>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 p-l-30 p-r-30 sm-p-l-10 sm-p-r-10">
			<h4>Складские помещения</h4>
    	<table class="table table-condensed table-hover" id="warehouse-container">
    		<thead>
    			<th>Наименование</th>
    			<th>Артикул</th>
    			<th>Остаток</th>
    		</thead>
    		<tbody></tbody>
    	</table>
      
      <br>
      
			<button class="btn btn-primary pull-right" id="warehouse-add"><i class="fa fa-plus"></i> Добавить товар в комплект</button>
		</div>
	</div>
</div>

<script type="text/html" id="warehouse-template">
<tr class="pointer">
	<td class="title"></td>
	<td class="artcode"></td>
	<td class="stock"></td>
	<td><a href="#" class="btn btn-sm delete"><i class="fa fa-trash"></i> Удалить</a></td>
</tr>
</script>