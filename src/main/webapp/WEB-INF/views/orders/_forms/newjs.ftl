window.product = {
	init: function(item) {
		this.forms.info = new this.forms.Info(item);
		this.item = item;
	},
	initWarehouses: function() {

		this.selects.selectWarehouses = $('#warehouseselect').select2({
      ajax: {
        url: "/warehouses", dataType: 'json', delay: 250,
        data: function (term, page) {
          return { q:  term, page: page };
        },
        results: function (data, page) {
          return { results: data.items };
        },
        cache: true
      },
      initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== "") {
              $.ajax("/warehouses/" + id + "/show", {
                  dataType: "json"
              }).done(function(data) { console.log(data.item); callback(JSON.parse(data.item)); });
          }
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: product.formatProductSelectResult,
      formatSelection: product.formatProductSelectResult
    });

    product.forms.warehouse = new product.forms.Warehouse({id:null,product_id:product.item.id});
    product.arrays.warehouses = new product.arrays.Warehouses([]);
		
		$.ajax({ url: '/products/' + product.item.id + '/warehouses', method: 'GET',
			success: function(data) {
				console.log(data);
				var warehouses = JSON.parse(data.items);
    		product.arrays.warehouses = new product.arrays.Warehouses(warehouses);
			}
		});
	},
	initComposites: function() {

		this.selects.selectProducts = $('#setproduct').select2({
      ajax: {
        url: "/products", dataType: 'json', delay: 250,
        data: function (term, page) {
          return { q:  term, page: page };
        },
        results: function (data, page) {
          return { results: data.items };
        },
        cache: true
      },
      initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== "") {
              $.ajax("/products/" + id + "/show", {
                  dataType: "json"
              }).done(function(data) { console.log(data.item); callback(JSON.parse(data.item)); });
          }
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: product.formatProductSelectResult,
      formatSelection: product.formatProductSelectResult
    });

    product.forms.set = new product.forms.SetProduct({id:null,product_id:product.item.id,amount:0});
    product.arrays.sets = new product.arrays.SetProducts([]);
		
		$.ajax({ url: '/products/' + product.item.id + '/sets', method: 'GET',
			success: function(data) {
				console.log(data);
				var sets = JSON.parse(data.items);
    		product.arrays.sets = new product.arrays.SetProducts(sets);
			}
		});
	},
	initCombos: function() {
		this.selects.selectAttributes = $('#attribute').select2({
      ajax: {
        url: "/attributes",
        dataType: 'json',
        delay: 250,
        data: function (term, page) {
          return {
            q:  term, // search term
            page: page
          };
        },
        results: function (data, page) {
          return {
            results: data.items
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: product.formatSelectResult,
      formatSelection: product.formatAttributeSelectResult
    });

    product.forms.combo = new product.forms.ComboProduct({id:null,product_id:product.item.id,artcode:null,pricewithtax:0,priceperunit:0,is_default:false});
    product.arrays.combos = new product.arrays.ComboProducts([]);
    product.arrays.comboProductValues = new product.arrays.ComboProductValues(undefined);
		
		$.ajax({ url: '/products/' + product.item.id + '/combos', method: 'GET',
			success: function(data) {
				console.log(data);
				var combos = JSON.parse(data.items);
    		product.arrays.combos = new product.arrays.ComboProducts(combos);
			}
		});
	},
	initCategory: function() {
		$("#categories-tree").dynatree({
      checkbox: true,
      // Override class name for checkbox icon:
      classNames: { checkbox: "dynatree-radio" },
      selectMode: 1,
      initAjax: { url: '/products/' + product.item.id + '/getCategoryTree' },
      onActivate: function(node) {
        node.toggleExpand();
      },
      onLazyRead: function(node) {
        node.appendAjax({
          url: '/categories/tree',
          data: {
            id: node.data.id
          },
          success: function(data) {
          }
        });
      },
      onSelect: function(select, node) {
        product.forms.category.jset({'id': node.data.id, 'title': node.data.title});
      	
        node.activateSilently();
      },
      onDblClick: function(node, event) {
        node.toggleSelect();
      }
    });
		
		product.forms.category = new product.forms.Category({id: undefined, title: undefined});
		
		if(product.item.category_id !== undefined && product.item.category_id != null) {
			$.ajax({ url: '/categories/' + product.item.category_id + '/single', method: 'GET',
				success: function(data) {
					console.log(data.item);
					product.forms.category.jset(JSON.parse(data.item));
				}
			});
		}
	},
	initPrices: function() {
		var _ = this;
		_.forms.prices = new _.forms.Prices(_.item);
		product.selects.selectTaxes = $('.taxes-select').select2({
      ajax: {
        url: "/taxes",
        dataType: 'json',
        delay: 250,
        data: function (term, page) {
          return {
            q:  term, // search term
            page: page
          };
        },
        results: function (data, page) {
          return {
            results: data.items
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; },
      minimumInputLength: 0,
      formatResult: product.formatTaxSelectResult,
      formatSelection: product.formatTaxSelect
    });

		if(_.item.tax_id !== undefined) {
			$.ajax({ url: '/taxes/' + _.item.tax_id + '/show', method: 'GET',
				success: function(data) {
					console.log(data);
					product.selects.selectTaxes.select2('data', data.item);
				}
			});
		}
	},
	forms: {},
	models: {},
	arrays: {},
	selects: {},
	formatSelectResult: function(data) {
		return data.title;
	},
	formatProductSelectResult: function(data) {
		return data.title.concat(": ", data.artcode);
	},
	formatAttributeSelectResult: function(data) {
		$.ajax({ url: '/attributes/attributeValues', data: {id: data.id},
			success: function(data) {
				product.selects.selectAttributeValue = $('#attribute-value').select2({
					data: { 
						results: data, 
						text: function(item) { 
							return item.title; 
						}
					},
					formatSelection: function(data) {
	        	// productForms.arrays.attributeValues.value = data;
	        	return data.title;
	        }
				});
			}
		});
		return data.title;
	},
	formatTaxSelect: function(data) {
    product.forms.prices.set('taxRate', data);
    return data.title + ' (' + data.rate + '%)';
  },
  formatTaxSelectResult: function(data) {
  	return data.title + ' (' + data.rate + '%)';
	},
}

product.forms.Warehouse = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-warehouse',
				cancel: ':sandbox button.cancel',
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('click::cancel', function(e) {
				e.preventDefault();
				this.hide();
			})
		;
	},
	submitForm: function() {
		var _ = this,
		
		warehouse = _.toJSON(),
			url = '/products/addWarehouse',
			method = 'post';
		

		warehouse.id = product.selects.selectWarehouses.select2('data').id;
		warehouse.product_id = product.item.id;

		$.ajax({ url: url, data: warehouse, method: method,
			success: function(data) {
				console.log("...........", data);
				product.arrays.warehouses.add(data.item);
				_.hide();
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
	},
	show: function() {
		$(this.sandbox).removeClass('hidden');
	},
	hide: function() {
		$(this.sandbox).addClass('hidden');
		product.selects.selectWarehouses.select2('val', '');
		$(this.sandbox).trigger('reset');
	}
});

product.models.Warehouse = Class({
	'extends': MK.Object,
	constructor: function(data) {
		console.log(data);
		this.jset(data)
			.jset('title', data.title)
			.jset('artcode', data.artcode)
			.jset('stock', data.stock)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						artcode: ':sandbox .artcode',
						stock: ':sandbox .stock',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					})
				;
			})
		;
	}
});

product.arrays.Warehouses = Class({
	'extends': MK.Array,
	itemRenderer: '#warehouse-template',
	Model: product.models.Warehouse,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#warehouse-control',
				container: ':sandbox tbody',
				add: ':sandbox #warehouse-add'
			}).on( '@readytodie', function( warehouse ) {
				var _ = this;
				$.ajax({ url: '/products/delete_warehouse/' + warehouse.warehouse_product_id, method: 'delete',
					success: function(data) {
						_.pull( warehouse );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				console.log(".......... addd");
				product.forms.warehouse.show();
			}).recreate(data)
		;
	},
	add: function(data) {
		console.log(data);
		var newItem = true;
		for( var i = 0; i < this.length; i++ ) {
			if( this[i].set_product_id === data.set_product_id ) {
				newItem = false;
			}
		}

		if(newItem) {
			this.push(data);
		}
	}
});

product.forms.SetProduct = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-set',
				priceperunit: ':sandbox input#priceperunit',
				amount: ':sandbox input#amount',
				cancel: ':sandbox button.cancel',
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('click::cancel', function(e) {
				e.preventDefault();
				this.hide();
			})
		;
	},
	submitForm: function() {
		var _ = this,
		
		set = _.toJSON(),
			url = '/products/addSet',
			method = 'post';
		
		if(set.set_product_id !== undefined && set.set_product_id != null) {
			url = '/products/editSet';
			method = 'put';
		}

		set.product_id = product.selects.selectProducts.select2('data').id;
		set.id = product.item.id;

		$.ajax({ url: url, data: set, method: method,
			success: function(data) {
				console.log("...........", data);
				product.arrays.sets.addOrChange(data.item);
				_.hide();
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
	},
	show: function() {
		$(this.sandbox).removeClass('hidden');
	},
	hide: function() {
		$(this.sandbox).addClass('hidden');
		product.selects.selectProducts.select2('val', '');
		$(this.sandbox).trigger('reset');
	}
});

product.models.SetProduct = Class({
	'extends': MK.Object,
	constructor: function(data) {
		console.log(data);
		this.jset(data)
			.jset('title', data.title)
			.jset('artcode', data.artcode)
			.jset('amount', data.amount)
			.jset('priceperunit', data.priceperunit)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						artcode: ':sandbox .artcode',
						priceperunit: ':sandbox .price',
						amount: ':sandbox .amount',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					}).on('click::title click::artcode click::priceperunit click::amount', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					})
				;
			})
		;
	}
});

product.arrays.SetProducts = Class({
	'extends': MK.Array,
	itemRenderer: '#set-template',
	Model: product.models.SetProduct,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#set-control',
				container: ':sandbox tbody',
				add: ':sandbox #set-add'
			}).on( '@readytodie', function( set ) {
				var _ = this;
				$.ajax({ url: '/products/delete_set/' + set.set_product_id, method: 'delete',
					success: function(data) {
						_.pull( set );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				console.log(".......... addd");
				product.forms.set.show();
			}).on('@clicked', function(set) {
				console.log("set.....", set.id);
				product.forms.set.jset(set.toJSON());
				product.selects.selectProducts.select2('val', set.product_id);
				product.forms.set.show();
			}).recreate(data)
		;
	},
	addOrChange: function(data) {
		console.log(data);
		var newItem = true;
		for( var i = 0; i < this.length; i++ ) {
			if( this[i].set_product_id === data.set_product_id ) {
				this[i].jset(data)
					.jset('artcode', data.artcode)
					.jset('pricewithtax', data.pricewithtax)
					.jset('priceperunit', data.priceperunit)
					.jset('amount', data.amount);	
				newItem = false;
			}
		}

		if(newItem) {
			this.push(data);
		}
	}
});

product.forms.ComboProduct = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-combinations',
				pricewithtax: ':sandbox input#pricewithtax',
				priceperunit: ':sandbox input#priceperunit',
				artcode: ':sandbox input#artcode',
				cancel: ':sandbox button.cancel',
				is_default: ':sandbox input#is_default'
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('click::cancel', function(e) {
				e.preventDefault();
				this.hide();
			}).linkProps('isDefault', 'is_default', function(is_default) {
				return is_default;
			})
		;
	},
	submitForm: function() {
		var _ = this,
			attribute_value_id = [];

		product.arrays.comboProductValues.forEach(function(element, index, array) {
			attribute_value_id.push(element.attribute_value_id);
		});
		
		var combo = _.toJSON(),
			url = '/products/addCombos',
			method = 'post';
		
		if(combo.id !== undefined && combo.id != null) {
			url = '/products/editCombo';
			method = 'put';
		}

		combo.attribute_value_id = attribute_value_id;

		$.ajax({ url: url, data: combo, method: method,
			success: function(data) {
				product.arrays.combos.addOrChange(data.item);
				_.hide();
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
	},
	show: function() {
		$(this.sandbox).removeClass('hidden');
	},
	hide: function() {
		$(this.sandbox).addClass('hidden');
	}
});

product.models.ComboProduct = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.jset('artcode', data.artcode)
			.jset('pricewithtax', data.pricewithtax)
			.jset('priceperunit', data.priceperunit)
			.on('render', function() {
				this
					.bindNode({
						title: ':sandbox .title',
						pricewithtax: ':sandbox .price',
						artcode: ':sandbox .artcode',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					}).on('click::title click::pricewithtax click::artcode', function(e) {
						e.preventDefault();
						this.trigger('clicked', this);
					}).linkProps('isDefault', 'is_default', function(is_default) {
						return is_default;
					}).bindNode('isDefault', ':sandbox', {
						setValue: function(v) {
							$(this).toggleClass('success', v);
						}
					})
				;
			})
		;
	}
});

product.arrays.ComboProducts = Class({
	'extends': MK.Array,
	itemRenderer: '#combinations-template',
	Model: product.models.ComboProduct,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#combinations-control',
				container: ':sandbox #combinations-container>tbody',
				add: ':sandbox #combinations-add'
			}).on( '@readytodie', function( combination ) {
				var _ = this;
				$.ajax({ url: '/products/delete_combination/' + combination.id, method: 'delete',
					success: function(data) {
						_.pull( combination );
						showNotification(data);
					},
					error: function(data) {
						showNotification(data);
					}
				});
			}).on('click::add', function(e) {
				e.preventDefault();
				product.forms.combo.show();
			}).on('@clicked', function(combination) {
				product.forms.combo.jset(combination.toJSON());
				$.ajax({ url: '/products/' + combination.id + '/comboProductsValues', method: 'GET',
					success: function(data) {
						product.arrays.comboProductValues.recreate(JSON.parse(data.items));
					}
				});
				product.forms.combo.show();
			}).recreate(data)
		;
	},
	addOrChange: function(data) {
		console.log(data);
		var newItem = true;
		for( var i = 0; i < this.length; i++ ) {
			if( this[i].id === data.id ) {
				this[i].jset(data)
					.jset('artcode', data.artcode)
					.jset('pricewithtax', data.pricewithtax)
					.jset('priceperunit', data.priceperunit);	
				newItem = false;
			}
			if(data.is_default && this[i].is_default && this[i].id !== data.id) {
				this[i].jset('is_default', false);
			}
		}

		if(newItem) {
			this.push(data);
		}
	}
});

product.models.ComboProductValue = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.on('render', function() {
				this
					.bindNode({
						attribute: ':sandbox .attribute',
						title: ':sandbox .value',
						delete: ':sandbox a'
					}, {
						setValue: function(v) {
							this.innerHTML = v;
						}
					}).on('click::delete', function(e) {
						e.preventDefault();
						this.trigger( 'readytodie', this );
					})
				;
			})
		;
	}
});

product.arrays.ComboProductValues = Class({
	'extends': MK.Array,
	itemRenderer: '#attributes-template',
	Model: product.models.ComboProductValue,
	constructor: function(data) {
		this
			.bindNode({
				sandbox: '#attributes-control',
				attribute: ':sandbox #attribute',
				cadd: ':sandbox #attribute-add',
				value: ':sandbox #attribute-value',
				container: ':sandbox #attributes-container>tbody'
			}).on( '@readytodie', function( attribute ) {
				var _ = this;
				if(attribute.id != null) {
					$.ajax({ url: '/products/deleteAttributeValue/' + attribute.id, method: 'delete',
						success: function(data) {
							_.pull( attribute );
							showNotification(data);
						},
						error: function(data) {
							showNotification(data);
						}
					});
				} else {
					_.pull(attribute);
				}
			}).on('click::cadd', function(e) {
				e.preventDefault();
				var data = product.selects.selectAttributeValue.select2('data');
				data.attribute = product.selects.selectAttributes.select2('data').title;
				data.attribute_value_id = data.id;
				data.id = null;
				this.push(data);
			})
		;

		if(data !== undefined)
			this.recreate(data);
	}
});


product.forms.Category = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-categories',
			}).bindNode('title', ':sandbox .category-title', {
				setValue: function(v) {
					this.innerHTML = v;
				}
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).linkProps('isValid', 'title id', function( title, id ) {
				return title !== undefined && id !== undefined;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			})
		;
	},
	submitForm: function() {
		$.ajax({url: '/products/category', data: { category_id: this.id, id: product.item.id }, method: 'put',
			success: function(data) {
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
		return this;
	}
});

product.forms.Prices = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-prices',
				cost: ':sandbox input#cost',
				pricepretax: ':sandbox input#pricePreTax',
				pricewithtax: ':sandbox input#priceWithTax',
				priceperunit: ':sandbox input#pricePerUnit',
				unitname: ':sandbox input#unitName'
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			}).on('change:pricepretax', function() {
				if(this.taxRate !== undefined)
					this.set('pricewithtax', (parseFloat(this.pricepretax) + parseFloat(this.pricepretax) * parseFloat(this.taxRate.rate) / 100).toFixed(2), {silent: true, forceHTML: true});
				else
					this.set('pricewithtax', parseFloat(this.pricepretax), {silent: true, forceHTML: true});

			}).on('change:pricewithtax', function() {
				if(this.taxRate !== undefined) {
					var p = parseFloat(parseFloat(this.taxRate.rate) / 100);
						p += 1;
					this.set('pricepretax', (parseFloat(this.pricewithtax) / p).toFixed(2), {silent: true, forceHTML: true});
				} else {
					this.set('pricepretax', parseFloat(this.pricewithtax), {silent: true, forceHTML: true});
				}

			}).on('change:taxRate', function() {
				if(this.pricepretax !== undefined) {
					this.set('pricewithtax', (parseFloat(this.pricepretax) + parseFloat(this.pricepretax) * parseFloat(this.taxRate.rate) / 100).toFixed(2), {silent: true, forceHTML: true});
				} else if(this.pricewithtax !== undefined) {
					var p = parseFloat(parseFloat(this.taxRate.rate) / 100);
						p += 1;
					this.set('pricepretax', (parseFloat(this.pricewithtax) / p).toFixed(2), {silent: true, forceHTML: true});
				}

			}).linkProps('isValid', 'cost pricepretax pricewithtax taxRate', function( cost, pricepretax, pricewithtax, taxRate ) {
				return cost && pricepretax && pricewithtax && taxRate;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			})
		;
	},
	submitForm: function() {
		if(this.isValid) {
			this.jset('tax_id', this.taxRate.id);
		}

		$.ajax({url: '/products/prices', data: this.toJSON(), method: 'put',
			success: function(data) {
				showNotification(data);
			},
			error: function(data) {
				showNotification(data);
			}
		});
		return this;
	}
});

product.forms.Info = Class({
	'extends': MK.Object,
	constructor: function(data) {
		this.jset(data)
			.bindNode({
				sandbox: '#form-info',
				title: ':sandbox input#title',
				artcode: ':sandbox input#artcode',
				type_code: ':sandbox input[name=type_code]'
			}).linkProps('isValid', 'title artcode', function( title, artcode ) {
				return title.length >= 3 && artcode.length >= 3;
			}).bindNode('isValid', ':sandbox .submit', {
				setValue: function(v) {
					$(this).toggleClass('disabled', !v);
				}
			}).linkProps('isDefault', 'type_code', function(type_code) {
				return type_code === 'DEFAULT';
			}).bindNode('isDefault', 'li#composite-menu', {
				setValue: function(v) {
					$(this).toggleClass('hidden', !v);
				}
			}).on('submit::sandbox', function(e) {
				e.preventDefault();
				this.submitForm();
			})
		;
	},
	submitForm: function() {
		var _ = this,
			item = _.toJSON(),
			url = '/products/save',
			method = 'post';
		
		if(item.id !== undefined) {
			url = '/products/update';
			method = 'put';
		}

		console.log(_.toJSON(), item);

		$.ajax({url: url, data: item, method: method,
			success: function(data) {
				showNotification(data);
				product.item = data.item;
        if(method === 'post') {
        	$('#currentBreadcrumb').attr('href', '/products/' + data.item.id + '/show');
        	$('#currentBreadcrumb').html(data.item.title);
        	$('a[href=#savefirst]').each(function() {
        		$(this).attr('href', $(this).attr('data-href'));
        	});
        	$(document).attr('title', 'Sales - Товар: ' + data.item.title);
        	window.history.pushState({state: {'controller': 'products', 'action': 'show', 'id': data.item.id}}, "", '/products/' + data.item.id + '/show');
        }
			},
			error: function(data) {
				showNotification(data);
			}
		});

		return this;
	}
});

window.showNotification = function(data) {
	$('body').pgNotification({
        style: 'bar',
        message: data.message,
        position: 'top',
        timeout: 3000,
        type: data.alert
    }).show();
};