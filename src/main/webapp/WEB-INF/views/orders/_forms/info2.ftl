<#ftl encoding="utf-8">
<div class="row">
	<div class="col-md-3 col-lg-3 col-xlg-4">
		    <div class="panel panel-default padding-30">
		    	<div class="panel-body">
			    	<#-- <i class="fa fa-shopping-cart fa-2x hint-text"></i> -->
				    <h2>Основные данные заказа</h2>
				    
						<form role="form">
						  <p>Name and Email Address</p>
						  <div class="form-group-attached">
						    <div class="row clearfix" style="margin-left:0px;margin-right:0px;">
						      <div class="col-sm-8">
						        <div class="form-group form-group-default required">
						          <label>Дата</label>
						          <input type="text" class="form-control" required>
						        </div>
						      </div>
						      <div class="col-sm-4">
						        <div class="form-group form-group-default">
						          <label>Время</label>
						          <input type="text" class="form-control">
						        </div>
						      </div>
						    </div>
						    
						    <div class="form-group form-group-default required">
						      <label>Тип заказа</label>
						      <input type="text" class="form-control" required>
						    </div>
						    <div class="form-group form-group-default required">
						      <label>Место проведения</label>
						      <input type="text" class="form-control" required>
						    </div>

						    <div class="row clearfix" style="margin-left:0px;margin-right:0px;">
						      <div class="col-sm-6">
						        <div class="form-group form-group-default">
						          <label>Количество Гостей</label>
						          <input type="text" class="form-control">
						        </div>
						      </div>
						      <div class="col-sm-6">
						        <div class="form-group form-group-default">
						          <label>Оформление</label>
						          <input type="text" class="form-control">
						        </div>
						      </div>
						    </div>
						  </div>
						</form>
				
						<hr class="b-grey b-dashed m-t-40">

			    	<#-- <i class="fa fa-user-secret fa-2x hint-text"></i> -->
				    <h2>Информация о клиенте</h2>
				    
						<form role="form">
						  <div class="form-group-attached">
				        <div class="form-group form-group-default required">
				          <label>Дата</label>
				          <input type="text" class="form-control" required>
				        </div>
						    <div class="form-group form-group-default required">
				          <label>Время</label>
				          <input type="text" class="form-control" required>
				        </div>
						  </div>
						</form>
					</div>
		    </div>
		  
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-8 col-xlg-6">
	    <div class="panel panel-default padding-30">
	    	<div class="panel-body">
		    	<#-- <i class="fa fa-calculator fa-2x hint-text"></i> -->
			    <h2>Калькуляция</h2>
			    <table class="table table-condensed">
			      <tr>
			        <td class="col-lg-8 col-md-6 col-sm-7 ">
			          <a href="#" class="remove-item"><i class="pg-close"></i></a>
			          <span class="m-l-10 font-montserrat fs-18 all-caps">Webarch UI Framework</span>
			          <span class="m-l-10 ">Dashboard UI Pack</span>
			        </td>
			        <td class=" col-lg-2 col-md-3 col-sm-3 text-right">
			          <span>Qty 1</span>
			        </td>
			        <td class=" col-lg-2 col-md-3 col-sm-2 text-right">
			          <h4 class="text-primary no-margin font-montserrat">$27</h4>
			        </td>
			      </tr>
			      <tr>
			        <td class="col-lg-8 col-md-6 col-sm-7">
			          <a href="#" class="remove-item"><i class="pg-close"></i></a>
			          <span class="m-l-10 font-montserrat fs-18 all-caps">Pages UI Framework</span>
			          <span class="m-l-10 ">Next Gen UI Pack</span>
			        </td>
			        <td class="col-lg-2 col-md-3 col-sm-3 text-right">
			          <span>Qty 1</span>
			        </td>
			        <td class=" col-lg-2 col-md-3 col-sm-2 text-right">
			          <h4 class="text-primary no-margin font-montserrat">$27</h4>
			        </td>
			      </tr>
			    </table>
			    <h5>Donation</h5>
			    <div class="row">
			      <div class="col-lg-7 col-md-6">
			        <p class="no-margin">Donate now and give clean, safe water to those in need. </p>
			        <p class="small hint-text">
			          100% of your donation goes to the field, and you can track the progress of every dollar spent. <a href="#">Click Here</a>
			        </p>
			      </div>
			      <div class="col-lg-5 col-md-6">
			        <div class="btn-group" data-toggle="buttons">
			          <label class="btn btn-default active">
			            <input type="radio" name="options" id="option1" checked> <span class="fs-16">$0</span>
			          </label>
			          <label class="btn btn-default">
			            <input type="radio" name="options" id="option2"> <span class="fs-16">$10</span>
			          </label>
			          <label class="btn btn-default">
			            <input type="radio" name="options" id="option3"> <span class="fs-16">$20</span>
			          </label>
			        </div>
			      </div>
			    </div>
			    <br>
			    <div class="container-sm-height">
			      <div class="row row-sm-height b-a b-grey">
			        <div class="col-sm-3 col-sm-height col-middle p-l-10 sm-padding-15">
			          <h5 class="font-montserrat all-caps small no-margin hint-text bold">Discount (10%)</h5>
			          <p class="no-margin">$10</p>
			        </div>
			        <div class="col-sm-7 col-sm-height col-middle sm-padding-15 ">
			          <h5 class="font-montserrat all-caps small no-margin hint-text bold">Donations</h5>
			          <p class="no-margin">$0</p>
			        </div>
			        <div class="col-sm-2 text-right bg-primary col-sm-height col-middle padding-10">
			          <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
			          <h4 class="no-margin text-white">$44</h4>
			        </div>
			      </div>
			    </div>
				</div>
	    </div>
		</div>
	</div>
</div>