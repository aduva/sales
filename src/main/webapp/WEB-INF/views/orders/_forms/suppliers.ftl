<#ftl encoding="utf-8">

<div class="panel-heading separator">
    <div class="panel-title">Поставщики товара</div>
</div>
<div class="panel-body">
	<div class="row" id="suppliers-control">
		<div class="col-md-12 p-l-30 p-r-30 sm-p-l-10 sm-p-r-10">
			<form id="form-suppliers" class="form-horizontal p-l-30 p-r-30 sm-p-l-10 sm-p-r-10 " role="form" autocomplete="off">
				<div class="form-group p-l-20 p-r-20 sm-p-l-10 sm-p-r-10">
					<div class="row">
						<label for="title" class="col-sm-3 control-label">Добавить компанию поставщика</label>
						<div class="col-sm-6">
							<input type="hidden" class="full-width" id="supplier-select">
						</div>
						<div class="col-sm-3">
							<a href="#" id="suppliers-add" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Добавить</a>
						</div>
					</div>
				</div>
			</form>
      <hr class="b-dashed">

    	<table class="table table-condensed table-hover" id="suppliers-container">
    		<thead>
    			<th>Наименование поставщика</th>
    			<th></th>
    		</thead>
    		<tbody></tbody>
    	</table>
      
		</div>
	</div>
</div>

<script type="text/html" id="suppliers-template">
<tr class="pointer">
	<td class="title"></td>
	<td>
		<a href="#" class="btn btn-sm" id="delete"><i class="fa fa-trash"></i> Удалить</a>
		<a href="#" class="btn btn-sm" id="makedefault"><i class="fa fa-check"></i> Сделать основным</a>
	</td>
</tr>
</script>