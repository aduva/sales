<#ftl encoding="utf-8">
<ul class="breadcrumb">
  <li><a href="/">Главная</a></li>
  <li><a href="/products">Товары</a></li>
  <#assign id=0/>
  <#assign method="post"/>
  
  <#if item??>
    <li><a id="currentBreadcrumb" href="/products/show/${item.id}" class="active">${item.title}</a></li>
    <#assign id=item.id/>
    <#assign method="put"/>
    
  <#else>
    <#assign tabHref="#savefirst"/>
    <li><a id="currentBreadcrumb" href="/products/add" class="active">Добавить товар</a></li>
  </#if>
</ul>
<div class="row">

  <div class="col-md-6 col-lg-4 col-xlg-4">
    <div class="panel panel-default padding-30">
      <div class="panel-body">
        <h2>Мероприятие</h2>
        
        <form role="form">
          <p>Основные данные мероприятия</p>
          <div class="form-group-attached">
            <div class="row clearfix" style="margin-left:0px;margin-right:0px;">
              <div class="col-sm-8">
                <div class="form-group form-group-default required">
                  <label>Дата</label>
                  <input type="text" class="form-control" required>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group form-group-default">
                  <label>Время</label>
                  <input type="text" class="form-control">
                </div>
              </div>
            </div>
            
            <div class="form-group form-group-default required">
              <label>Тип заказа</label>
              <input type="text" class="form-control" required>
            </div>
            <div class="form-group form-group-default required">
              <label>Место проведения</label>
              <input type="text" class="form-control" required>
            </div>

            <div class="row clearfix" style="margin-left:0px;margin-right:0px;">
              <div class="col-sm-6">
                <div class="form-group form-group-default">
                  <label>Количество Гостей</label>
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-group-default">
                  <label>Оформление</label>
                  <input type="text" class="form-control">
                </div>
              </div>
            </div>
          </div>
        </form>
    
        <hr class="b-grey b-dashed m-t-40">

        <#-- <i class="fa fa-user-secret fa-2x hint-text"></i> -->
        <div class="m-b-10">
          <p class="pull-left lg-text">Клиент</p>
          <div class="pull-right">
            <label class="p-r-10">Юр. лицо</label>
            <input type="checkbox" data-init-plugin="switchery" />
          </div>
          <div class="clearfix"></div>
        </div>
        
        <form role="form">
          <p>Информация о клиенте</p>
          <div class="form-group-attached">
            <div class="form-group form-group-default required">
              <label>Наименование организации</label>
              <input type="text" class="form-control" placeholder="Название организации" required>
            </div>
            <div class="form-group form-group-default required">
              <label>Имя клиента</label>
              <input type="text" class="form-control" placeholder="Фамилия и имя клиента или контактное лицо организации" required>
            </div>
            <div class="form-group form-group-default required">
              <label>Телефон</label>
              <input type="text" class="form-control" placeholder="Номер телефона" required>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-8 col-xlg-8">
    <div class="panel panel-default padding-30">
      <div class="panel-body">
        <#-- <i class="fa fa-calculator fa-2x hint-text"></i> -->
        <h2>Калькуляция</h2>
        <table class="table table-condensed">
          <tr>
            <td class="col-lg-8 col-md-6 col-sm-7 ">
              <a href="#" class="remove-item"><i class="pg-close"></i></a>
              <span class="m-l-10 font-montserrat fs-18 all-caps">Webarch UI Framework</span>
              <span class="m-l-10 ">Dashboard UI Pack</span>
            </td>
            <td class=" col-lg-2 col-md-3 col-sm-3 text-right">
              <span>Qty 1</span>
            </td>
            <td class=" col-lg-2 col-md-3 col-sm-2 text-right">
              <h4 class="text-primary no-margin font-montserrat">$27</h4>
            </td>
          </tr>
          <tr>
            <td class="col-lg-8 col-md-6 col-sm-7">
              <a href="#" class="remove-item"><i class="pg-close"></i></a>
              <span class="m-l-10 font-montserrat fs-18 all-caps">Pages UI Framework</span>
              <span class="m-l-10 ">Next Gen UI Pack</span>
            </td>
            <td class="col-lg-2 col-md-3 col-sm-3 text-right">
              <span>Qty 1</span>
            </td>
            <td class=" col-lg-2 col-md-3 col-sm-2 text-right">
              <h4 class="text-primary no-margin font-montserrat">$27</h4>
            </td>
          </tr>
        </table>
        <h5>Donation</h5>
        <div class="row">
          <div class="col-lg-7 col-md-6">
            <p class="no-margin">Donate now and give clean, safe water to those in need. </p>
            <p class="small hint-text">
              100% of your donation goes to the field, and you can track the progress of every dollar spent. <a href="#">Click Here</a>
            </p>
          </div>
          <div class="col-lg-5 col-md-6">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-default active">
                <input type="radio" name="options" id="option1" checked> <span class="fs-16">$0</span>
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option2"> <span class="fs-16">$10</span>
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option3"> <span class="fs-16">$20</span>
              </label>
            </div>
          </div>
        </div>
        <br>
        <div class="container-sm-height">
          <div class="row row-sm-height b-a b-grey">
            <div class="col-sm-3 col-sm-height col-middle p-l-10 sm-padding-15">
              <h5 class="font-montserrat all-caps small no-margin hint-text bold">Discount (10%)</h5>
              <p class="no-margin">$10</p>
            </div>
            <div class="col-sm-7 col-sm-height col-middle sm-padding-15 ">
              <h5 class="font-montserrat all-caps small no-margin hint-text bold">Donations</h5>
              <p class="no-margin">$0</p>
            </div>
            <div class="col-sm-2 text-right bg-primary col-sm-height col-middle padding-10">
              <h5 class="font-montserrat all-caps small no-margin hint-text text-white bold">Total</h5>
              <h4 class="no-margin text-white">$44</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


<@content for="title">Добавить товар</@content>
<@content for="bodyClass">dashboard</@content>
<@content for="js">
<script src="${context_path}/plugins/jquery-dynatree/jquery.dynatree.min.js" type="text/javascript"></script>
<script src="${context_path}/plugins/matreshka/matreshka.min.js"></script>
<!--script src="${context_path}/js/products/forms/main.js" type="text/javascript"></script-->
<script>
  $(document).ready(function() {
    $('li#menu-order, li#menu-orders').addClass('active open');
  
   
});

</script>
</@content>

<@content for="styles">
<link href="${context_path}/plugins/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css" media="screen" />
</@content>