create extension tablefunc;

-- INSERT INTO groups(groupname, title) VALUES ('sazsyrnai', 'Саз-Сырнай');
-- INSERT INTO groups(groupname, title) VALUES ('yerkesylkym', 'Ерке Сылқым');
-- INSERT INTO users(username, password) VALUES ('askhat', '$2a$10$MzGkJKgpo8knTCyYH1sNje/vn6PgOUgwDJLKjzqgksKyzCjeRcrhy');
-- INSERT INTO roles(title, level, group_id, created_at, updated_at) VALUES ('Admin', 1, 1, 'now', 'now');
-- INSERT INTO roles(title, level, group_id, created_at, updated_at) VALUES ('Admin', 1, 2, 'now', 'now');
-- INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (1, 1, 1, 'now', 'now');
-- INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (2, 1, 2, 'now', 'now');
-- INSERT INTO profiles(user_id, first_name, last_name, created_at, updated_at) VALUES (1, 'Askhat', 'Shakenov', 'now', 'now');
-- --
-- --Sazsyrnai
-- --
-- INSERT INTO taxes(title, is_default, rate, user_id, group_id, created_at, updated_at) VALUES ('Обычная ставка', true, 10, 1, 1, 'now', 'now');
-- INSERT INTO taxes(title, is_default, rate, user_id, group_id, created_at, updated_at) VALUES ('Обычная ставка', true, 10, 1, 2, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Блюда', 'foods', null, 1, 1, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Ингредиенты', 'ingredients', null, 1, 1, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Салаты', 'salads', 1, 1, 1, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Горячие', 'maincourses', 1, 1, 1, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Холодные закуски', 'coldsnacks', 1, 1, 1, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Фуршетный стол', 'fourset', 1, 1, 1, 'now', 'now');
-- INSERT INTO categories(title, url, parent_id, user_id, group_id, created_at, updated_at) VALUES ('Гарниры', 'garnish', 1, 1, 1, 'now', 'now');

-- INSERT INTO features(title, url, user_id, group_id, created_at, updated_at) VALUES ('Вес', 'weight', 1, 1, 'now', 'now');
-- INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('100 г.', 1, 1, 'now', 'now');
-- INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('200 г.', 1, 1, 'now', 'now');
-- INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('400 г.', 1, 1, 'now', 'now');
-- INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('600 г.', 1, 1, 'now', 'now');

-- INSERT INTO products(title, artcode, type_code, pricewithtax, category_id, user_id, group_id, created_at, updated_at)
--  VALUES ('Цезарь', 'S1', 'DEFAULT', 1250, 3, 1, 1, 'now', 'now');
-- INSERT INTO products(title, artcode, type_code, pricewithtax, category_id, user_id, group_id, created_at, updated_at)
--  VALUES ('Греческий', 'S2', 'DEFAULT', 1050, 3, 1, 1, 'now', 'now');
-- INSERT INTO products(title, artcode, type_code, pricewithtax, category_id, user_id, group_id, created_at, updated_at)
--  VALUES ('Золотая рыбка', 'С1', 'DEFAULT', 1550, 5, 1, 1, 'now', 'now');
-- INSERT INTO products(title, artcode, type_code, pricewithtax, category_id, user_id, group_id, created_at, updated_at)
--  VALUES ('Казан кебаб', 'H1', 'DEFAULT', 1150, 4, 1, 1, 'now', 'now');
-- INSERT INTO products(title, artcode, type_code, pricewithtax, category_id, user_id, group_id, created_at, updated_at)
--  VALUES ('Рис ризотто', 'G1', 'DEFAULT', 750, 7, 1, 1, 'now', 'now');

-- INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, is_disabled, created_at, updated_at) 
--  VALUES (1, 2, 1, 1, false, 'now', 'now');
-- INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, is_disabled, created_at, updated_at) 
--  VALUES (2, 2, 1, 1, false, 'now', 'now');
-- INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, is_disabled, created_at, updated_at) 
--  VALUES (3, 4, 1, 1, false, 'now', 'now');
-- INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, is_disabled, created_at, updated_at) 
--  VALUES (4, 3, 1, 1, false, 'now', 'now');
-- INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, is_disabled, created_at, updated_at) 
--  VALUES (5, 1, 1, 1, false, 'now', 'now');



INSERT INTO groups(groupname, title) VALUES ('yerkesylkym', 'Ерке Сылқым');
INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Askhat', 'Shakenov', 'now', 'now');
INSERT INTO users(username, profile_id, password) VALUES ('manager', 1, '$2a$10$MzGkJKgpo8knTCyYH1sNje/vn6PgOUgwDJLKjzqgksKyzCjeRcrhy');
INSERT INTO roles(title, rolename, created_at, updated_at) VALUES ('Управляющий', 'manager', 'now', 'now');
INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (1, 1, 1, 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Бухгалтер', 'Shakenov', 'now', 'now');
INSERT INTO users(username, profile_id, password) VALUES ('accountant', 1, '$2a$10$MzGkJKgpo8knTCyYH1sNje/vn6PgOUgwDJLKjzqgksKyzCjeRcrhy');
INSERT INTO roles(title, rolename, created_at, updated_at) VALUES ('Бухгалтер', 'accountant', 'now', 'now');
INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (1, 2, 2, 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Продавец', 'Shakenov', 'now', 'now');
INSERT INTO users(username, profile_id, password) VALUES ('askhat', 1, '$2a$10$MzGkJKgpo8knTCyYH1sNje/vn6PgOUgwDJLKjzqgksKyzCjeRcrhy');
INSERT INTO roles(title, rolename, created_at, updated_at) VALUES ('Продавец', 'salesman', 'now', 'now');
INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (1, 3, 3, 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Кассир', 'Shakenov', 'now', 'now');
INSERT INTO users(username, profile_id, password) VALUES ('cashier', 1, '$2a$10$MzGkJKgpo8knTCyYH1sNje/vn6PgOUgwDJLKjzqgksKyzCjeRcrhy');
INSERT INTO roles(title, rolename, created_at, updated_at) VALUES ('Кассир', 'cashier', 'now', 'now');
INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (1, 4, 4, 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Зав. складом', 'Shakenov', 'now', 'now');
INSERT INTO users(username, profile_id, password) VALUES ('stocks', 1, '$2a$10$MzGkJKgpo8knTCyYH1sNje/vn6PgOUgwDJLKjzqgksKyzCjeRcrhy');
INSERT INTO roles(title, rolename, created_at, updated_at) VALUES ('Зав. складом', 'stocksman', 'now', 'now');
INSERT INTO groups_users(group_id, user_id, role_id, created_at, updated_at) VALUES (1, 5, 5, 'now', 'now');

--
-- Yerke Sylkym
--
INSERT INTO taxes(title, is_default, rate, user_id, group_id, created_at, updated_at) VALUES ('Обычная ставка', true, 10, 1, 1, 'now', 'now');
INSERT INTO categories(title, url, user_id, group_id, created_at, updated_at) VALUES ('Ткани', 'textiles', 1, 1, 'now', 'now');
INSERT INTO categories(title, url, user_id, group_id, created_at, updated_at) VALUES ('Тканевая фурнитура', 'txtfinds', 1, 1, 'now', 'now');
INSERT INTO categories(title, url, user_id, group_id, created_at, updated_at) VALUES ('Шторы', 'blinds', 1, 1, 'now', 'now');
INSERT INTO categories(title, url, user_id, group_id, created_at, updated_at) VALUES ('Портьерная фурнитура', 'blnfinds', 1, 1, 'now', 'now');

INSERT INTO features(title, url, user_id, group_id, created_at, updated_at) VALUES ('Ширина', 'width', 1, 1, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('1,2 м.', 1, 1, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('1,5 м.', 1, 1, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('2 м.', 1, 1, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('2,5 м.', 1, 1, 'now', 'now');

INSERT INTO features(title, url, user_id, group_id, created_at, updated_at) VALUES ('Цвет', 'color', 1, 1, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('Белый', 1, 2, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('Красный', 1, 2, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('Синий', 1, 2, 'now', 'now');
INSERT INTO feature_values(title, user_id, feature_id, created_at, updated_at) VALUES ('Черный', 1, 2, 'now', 'now');

INSERT INTO products(title, artcode, type_code, pricewithtax, user_id, group_id, description, created_at, updated_at)
 VALUES ('Шелк', 'SILK1', 'DEFAULT', 1250, 1, 1, 'Шелк белый', 'now', 'now');
INSERT INTO products(title, artcode, type_code, pricewithtax, user_id, group_id, description, created_at, updated_at)
 VALUES ('Драп', 'DRAPE1', 'DEFAULT', 1250, 1, 1, 'Драп Красный', 'now', 'now');
INSERT INTO products(title, artcode, type_code, pricewithtax, user_id, group_id, description, created_at, updated_at)
 VALUES ('Вискоза', 'VISK1', 'DEFAULT', 1250, 1, 1, 'Вискоза Синий', 'now', 'now');
INSERT INTO products(title, artcode, type_code, pricewithtax, user_id, group_id, description, created_at, updated_at)
 VALUES ('Атлас', 'ATLAS1', 'DEFAULT', 1250, 1, 1, 'Атлас Черный', 'now', 'now');
INSERT INTO products(title, artcode, type_code, pricewithtax, user_id, group_id, description, created_at, updated_at)
 VALUES ('Шерсть', 'WOOL1', 'DEFAULT', 1250, 1, 1, 'Шерсть белый', 'now', 'now');

INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (1, 2, 1, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (2, 2, 1, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (3, 4, 1, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (4, 3, 1, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (5, 1, 1, 1, 'now', 'now');


INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (1, 5, 2, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (2, 6, 2, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (3, 7, 2, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (4, 8, 2, 1, 'now', 'now');
INSERT INTO products_feature_values(product_id, feature_value_id, feature_id, user_id, created_at, updated_at) 
 VALUES (5, 5, 2, 1, 'now', 'now');

INSERT INTO categories_products(product_id, category_id, user_id, created_at, updated_at) 
 VALUES (1, 1, 1, 'now', 'now');
INSERT INTO categories_products(product_id, category_id, user_id, created_at, updated_at) 
 VALUES (2, 1, 1, 'now', 'now');
INSERT INTO categories_products(product_id, category_id, user_id, created_at, updated_at) 
 VALUES (3, 1, 1, 'now', 'now');
INSERT INTO categories_products(product_id, category_id, user_id, created_at, updated_at) 
 VALUES (4, 1, 1, 'now', 'now');
INSERT INTO categories_products(product_id, category_id, user_id, created_at, updated_at) 
 VALUES (5, 1, 1, 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Бекмырза', 'Карабала', 'now', 'now');
INSERT INTO clients(profile_id, group_id, created_at, updated_at) VALUES (2, 1, 'now', 'now');
INSERT INTO contacts(profile_id, title, value, created_at, updated_at) VALUES (2, 'PHONE', '+77077803688', 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Гулсалат', 'Алмабаева', 'now', 'now');
INSERT INTO clients(profile_id, group_id, created_at, updated_at) VALUES (3, 1, 'now', 'now');
INSERT INTO contacts(profile_id, title, value, created_at, updated_at) VALUES (3, 'PHONE', '+77077003666', 'now', 'now');

INSERT INTO profiles(first_name, last_name, created_at, updated_at) VALUES ('Аймурат', 'Карабаев', 'now', 'now');
INSERT INTO clients(profile_id, group_id, created_at, updated_at) VALUES (4, 1, 'now', 'now');
INSERT INTO contacts(profile_id, title, value, created_at, updated_at) VALUES (4, 'PHONE', '+77077803336', 'now', 'now');