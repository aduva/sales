CREATE TABLE groups ( 
	id BIGSERIAL PRIMARY KEY,
	groupname VARCHAR(20) UNIQUE,
	title VARCHAR(128),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP
);

CREATE TABLE users (
	id BIGSERIAL PRIMARY KEY,
	username VARCHAR(20) UNIQUE,
	password VARCHAR(128),
	profile_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP
);

CREATE TABLE roles (
	id BIGSERIAL PRIMARY KEY,
	title VARCHAR(128),
	rolename VARCHAR(20) UNIQUE,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP
);

CREATE TABLE groups_users (
	group_id bigint NOT NULL,
	user_id bigint NOT NULL,
	role_id bigint NOT NULL,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	PRIMARY KEY (group_id, user_id, role_id)
);

CREATE TABLE roles_permissions (
	role_id bigint NOT NULL,
	permission VARCHAR(60) NOT NULL,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	PRIMARY KEY (role_id, permission)
	-- id BIGSERIAL PRIMARY KEY
);

CREATE TABLE groups_permissions (
	-- id BIGSERIAL PRIMARY KEY,
	group_id bigint NOT NULL,
	permission VARCHAR(60) NOT NULL,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	PRIMARY KEY (group_id, permission)
);

CREATE TABLE users_permissions (
	-- id BIGSERIAL PRIMARY KEY
	user_id bigint NOT NULL,
	permission VARCHAR(60) NOT NULL,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	PRIMARY KEY (user_id, permission)
);

CREATE TABLE profiles (
	id BIGSERIAL PRIMARY KEY, 
	first_name VARCHAR(128),
	last_name VARCHAR(128),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP
);

CREATE TABLE contacts (
	id BIGSERIAL PRIMARY KEY, 
	profile_id bigint,
	title VARCHAR(128),
	value VARCHAR(128),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	CONSTRAINT ck_concat_item_title CHECK (title::text = ANY (ARRAY['PHONE'::character varying, 'EMAIL'::character varying, 'ADDRESS'::character varying]::text[]))
);

