CREATE TABLE products (
	id BIGSERIAL PRIMARY KEY,                   
	title VARCHAR(128),
	artcode VARCHAR(128),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	type_code VARCHAR(7),
	cost double precision,
	pricePreTax double precision,
	pricePerUnit double precision,
	priceWithTax double precision,
	unitName VARCHAR(128),
	tax_id bigint,
	user_id bigint,
	group_id bigint,
	when_out_of_stock VARCHAR(7),
	pack_quantities VARCHAR(16),
	is_disabled boolean,
	description text,
	CONSTRAINT ck_products_type_code CHECK (type_code::text = ANY (ARRAY['PARTIAL'::character varying, 'DEFAULT'::character varying, 'VIRTUAL'::character varying, 'SET'::character varying, 'COMBO'::character varying, 'P_COMBO'::character varying]::text[])),
	CONSTRAINT ck_prod_when_out_of_stock_code CHECK (when_out_of_stock::text = ANY (ARRAY['DEFAULT'::character varying, 'ALLOW'::character varying, 'DENY'::character varying]::text[])),
	CONSTRAINT ck_prod_pack_quantities_code CHECK (pack_quantities::text = ANY (ARRAY['DEFAULT'::character varying, 'DEC_PACK_ONLY'::character varying, 'DEC_PROD_IN_PACK'::character varying, 'DEC_BOTH'::character varying]::text[]))
);

CREATE TABLE attributes (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	user_id bigint,
	group_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE attribute_values (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	attribute_id BIGINT,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE composites (
	id BIGSERIAL PRIMARY KEY, 
	product_id BIGINT NOT NULL,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE combo_products (
	id BIGSERIAL PRIMARY KEY, 
	composite_id BIGINT NOT NULL,
	product_id BIGINT NOT NULL,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE combo_products_values (
	id BIGSERIAL PRIMARY KEY, 
	combo_product_id BIGINT NOT NULL,
	attribute_value_id BIGINT NOT NULL,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE set_products (
	id BIGSERIAL PRIMARY KEY, 
	composite_id BIGINT NOT NULL,
	product_id BIGINT NOT NULL,
	amount double precision,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE categories (
	id BIGSERIAL PRIMARY KEY,
	title VARCHAR(128),
	url VARCHAR(128),
	user_id bigint,
	group_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE categories_products (
	id BIGSERIAL PRIMARY KEY, 
	product_id bigint,
	category_id bigint,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE taxes (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	is_default boolean,
	rate double precision,
	user_id bigint,
	group_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE suppliers (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	user_id bigint,
	group_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE suppliers_products (
	id BIGSERIAL PRIMARY KEY, 
	product_id bigint,
	supplier_id bigint,
	user_id bigint,
	is_default boolean,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE warehouses (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	artcode VARCHAR(128),
	user_id bigint,
	group_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE warehouses_products (
	id BIGSERIAL PRIMARY KEY, 
	product_id bigint,
	warehouse_id bigint,
	stock double precision,
	user_id bigint,
	is_default boolean,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE warehouse_movements (
	id BIGSERIAL PRIMARY KEY, 
	product_id bigint,
	warehouse_id bigint,
	amount double precision,
	user_id bigint,
	group_id bigint,
	manage_subject_id bigint,
	manage_type_code VARCHAR(13),
	CONSTRAINT ck_manage_type_code CHECK (manage_type_code::text = ANY (ARRAY['INCREASE'::character varying, 'DECREASE'::character varying, 'TRANSFER_TO'::character varying, 'TRANSFER_FROM'::character varying, 'SUPPLY'::character varying, 'ORDER'::character varying]::text[])),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE features (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	url VARCHAR(128),
	user_id bigint,
	group_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE feature_values (
	id BIGSERIAL PRIMARY KEY, 
	title VARCHAR(128),
	feature_id BIGINT,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE products_feature_values (
	id BIGSERIAL PRIMARY KEY, 
	product_id BIGINT,
	feature_id BIGINT,
	feature_value_id BIGINT,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);