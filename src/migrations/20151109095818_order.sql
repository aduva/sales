CREATE TABLE orders (
	id BIGSERIAL PRIMARY KEY,
	order_status_id bigint, 
	due_date TIMESTAMP, 
	user_id bigint,
	client_id bigint,
	sum double precision,
	group_id bigint	,
	status_code VARCHAR(6),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP
	CONSTRAINT ck_orders_status_code CHECK (status_code::text = ANY (ARRAY['OPEN'::character varying, 'PAID'::character varying, 'CLOSED'::character varying]::text[]))
	);

CREATE TABLE orders_products (
	id BIGSERIAL PRIMARY KEY, 
	product_id bigint,
	order_id bigint,
	amount double precision,
	pricewithtax double precision,
	sum double precision,
	user_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP);

CREATE TABLE orders_payments (
	id BIGSERIAL PRIMARY KEY, 
	user_id bigint,
	order_id bigint,
	amount double precision,
	type_code VARCHAR(7),
	created_at TIMESTAMP, 
	updated_at TIMESTAMP,
	CONSTRAINT ck_order_payment_type_code CHECK (type_code::text = ANY (ARRAY['CASH'::character varying, 'DEFAULT'::character varying, 'C_CARD'::character varying]::text[]))
	);

CREATE TABLE clients (
	id BIGSERIAL PRIMARY KEY, 
	group_id bigint,
	profile_id bigint,
	created_at TIMESTAMP, 
	updated_at TIMESTAMP
);