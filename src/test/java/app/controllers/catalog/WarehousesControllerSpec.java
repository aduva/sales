package app.controllers;

import org.javalite.activeweb.DBControllerSpec;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import app.services.SecurityServiceMockModule;
import com.google.inject.Guice;
import org.javalite.activeweb.Cookie;
import app.services.Subject;

/**
 * @author Askhat Shakenov
 */
public class WarehousesControllerSpec extends DBControllerSpec {
    Group group = null;
    String groupname = "group1";
    Long user_id = 1l;
    String title = "Warehouse One Hello";
    Long id = null, id2 = null;
    String username = "askhat";
    Cookie userCookie = new Cookie("last_login", username);
    
    @Before
    public void before() {
        Product.deleteAll();
        Warehouse.deleteAll();
        Group.deleteAll();
        User.deleteAll();
        GroupsUsers.deleteAll();
        Role.deleteAll();
        group = Group.createIt("groupname", groupname, "title", "fake group");
        
        User u = User.newUser(username, "123456");
        u.saveIt();
        Role role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());

        GroupsUsers ur1 = new GroupsUsers();
        ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
        ur1.save();

        user_id = u.getLongId();

        Warehouse p = Warehouse.createIt("artcode", "Warehouse1", "user_id", user_id, "group_id", group.getLongId(), "title", title);
        id = p.getLongId();
        Warehouse p2 = Warehouse.createIt("artcode", "Warehouse2", "user_id", user_id, "group_id", group.getLongId(), "title", "Some another");
        id2 = p2.getLongId();
        
        Subject subject = new Subject(u, true, false);
        subject.setCurrentGroup(group);
        session("subject", subject);

    }

    @Test
    public void shouldListAll() {
        request().get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);
    }

    @Test
    public void shouldListByPageSize() {
        request().param("pageSize", 1).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldListByPageSizeAndPage() {
        request().param("pageSize", 2).param("page", 2).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(0);
    }

    @Test
    public void shouldListBySearchQueue() {
        request().param("q", title.substring(0,3)).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);

        request().param("q", "some").get("index");
        items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);

        request().param("q", "1234").get("index");
        items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(0);
    }

    @Test
    public void shouldShow() {
        request().param("id", id).get("show");
        Warehouse item = (Warehouse) assigns().get("item");
        a(item.get("title")).shouldBeEqual(title);
    }

    @Test
    public void shouldCreate() {
        //create a fourth item
        request()
            .param("title", "New Warehouse")
            // .param("username", "askhat")
            
        .post("save");
        

        a(redirected()).shouldBeTrue();
        a(Warehouse.count()).shouldBeEqual(3);
        a(flash("message")).shouldNotBeNull();

        request().get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(3);
    }

    @Test
    public void shouldChangeStockAmount() {
        Product p = Product.createIt("type_code", "DEFAULT", "user_id", user_id, "group_id", group.getLongId(), "title", "Product title", "artcode", "22222");
        request().param("id", id).param("product_id", p.getLongId()).param("stock_amount", 100).post("changeStockAmount");
        a(redirected()).shouldBeTrue();
        a(WarehousesProducts.count()).shouldBeEqual(1);

        request().param("id", id2).param("product_id", p.getLongId()).param("stock_amount", 150).post("changeStockAmount");
        a(redirected()).shouldBeTrue();
        a(WarehousesProducts.count()).shouldBeEqual(2);
    }

    @Test
    public void shouldIncreaseStockAmount() {
        Product p = Product.createIt("type_code", "DEFAULT", "user_id", user_id, "group_id", group.getLongId(), "title", "Product title", "artcode", "22222");
        request().param("id", id).param("product_id", p.getLongId()).param("stock_amount", 100).post("changeStockAmount");
        a(redirected()).shouldBeTrue();
        a(WarehousesProducts.count()).shouldBeEqual(1);

        request().param("id", id).param("product_id", p.getLongId()).param("stock_amount", 50).post("increaseStockAmount");
        a(redirected()).shouldBeTrue();
        a(WarehousesProducts.count()).shouldBeEqual(1);

        // request().param("id", id).get("show");
        // List<Map> items = (List<Map>) assigns().get("items");
        // a(items.size()).shouldBeEqual(1);
        // a(items.get(0).get("stock_amount")).shouldBeEqual(150);
        // a(items.get(0).get("title")).shouldBeEqual("Product title");
    }

    @Test
    public void shouldDecreaseStockAmount() {
        Product p = Product.createIt("type_code", "DEFAULT", "user_id", user_id, "group_id", group.getLongId(), "title", "Product title", "artcode", "22222");
        request().param("id", id).param("product_id", p.getLongId()).param("stock_amount", 100).post("changeStockAmount");
        a(redirected()).shouldBeTrue();
        a(WarehousesProducts.count()).shouldBeEqual(1);

        request().param("id", id).param("product_id", p.getLongId()).param("stock_amount", 50).post("decreaseStockAmount");
        a(redirected()).shouldBeTrue();
        a(WarehousesProducts.count()).shouldBeEqual(1);

        // request().param("id", id).get("show");
        // List<Map> items = (List<Map>) assigns().get("items");
        // a(items.size()).shouldBeEqual(1);
        // a(items.get(0).get("stock_amount")).shouldBeEqual(50);
        // a(items.get(0).get("title")).shouldBeEqual("Product title");
    }

    @Test
    public void shouldUpdate() {
        //create a fourth item
        String newtitle = "Warehouse One Updated";
        request().param("id", id).param("title", newtitle).param("artcode", "newartcode").post("update");
        
        a(redirected()).shouldBeTrue();
        
        a(flash("message")).shouldNotBeNull();

        request().param("id", id).get("show");
        Warehouse item = (Warehouse) assigns().get("item");
        a(item.get("title")).shouldBeEqual(newtitle);
        a(item.get("artcode")).shouldBeEqual("newartcode");
    }

    // @Test
    // public void shouldDelete() {
    //     Group u = (Group) Group.findAll().get(0);

    //     request().param("groupname",  u.get("groupname")).delete("delete");
        
    //     a(redirected()).shouldBeTrue();
    //     a(Group.count()).shouldBeEqual(2);
    //     a(flash("message")).shouldNotBeNull();
    // }

    // @Test
    // public void shouldShowHTML() {
    //     Group i = (Group) Group.findAll().get(0);

    //     request().integrateViews().param("groupname",  i.get("groupname")).get("show");

    //     Group item = (Group) assigns().get("item");
    //     a(item.get("groupname")).shouldBeEqual(i.get("groupname"));
    //     String html = responseContent();
    //     a(html.contains(i.getString("title"))).shouldBeTrue();
    // }
}
