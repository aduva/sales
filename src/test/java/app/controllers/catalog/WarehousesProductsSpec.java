package app.controllers;

import org.javalite.activeweb.DBIntegrationSpec;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import app.services.SecurityServiceMockModule;
import com.google.inject.Guice;
import org.javalite.activeweb.Cookie;
import app.services.Subject;

/**
 * @author Askhat Shakenov
 */
public class WarehousesProductsSpec extends DBIntegrationSpec {
    Group group = null;
    String groupname = "group1";
    Long user_id = 1l, id, id2, id3;
    String username = "askhat";

    public void bef() {
        ComboProducts.deleteAll();
        Attribute.deleteAll();
        AttributeValue.deleteAll();
        Composite.deleteAll();
        Product.deleteAll();
        Warehouse.deleteAll();
        Group.deleteAll();
        User.deleteAll();
        GroupsUsers.deleteAll();
        Role.deleteAll();
        group = Group.createIt("groupname", groupname, "title", "fake group");
        
        User u = User.newUser(username, "123456");
        u.saveIt();
        Role role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());

        GroupsUsers ur1 = new GroupsUsers();
        ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
        ur1.save();

        user_id = u.getLongId();
        
        Subject subject = new Subject(u, true, false);
        subject.setCurrentGroup(group);
        session("subject", subject);

    }

    @Test
    public void shouldShowManagement() {
        bef();
        addProducts();

        controller("products").get("index");
        List items = (List) vals().get("items");
        a(items.size()).shouldBeEqual(7);

        controller("warehouses").get("management");
        items = (List) vals().get("items");
        a(items.size()).shouldBeEqual(3);
    }

    @Test
    public void shouldManage() {
        bef();
        addProducts();

        controller("products").get("index");
        List items = (List) vals().get("items");
        a(items.size()).shouldBeEqual(7);

        controller("warehouses").get("management");
        items = (List) vals().get("items");
        a(items.size()).shouldBeEqual(3);

        Warehouse p = Warehouse.createIt("artcode", "Warehouse1", "user_id", user_id, "group_id", group.getLongId(), "title", "Some title");
        Long wid = p.getLongId();
        Warehouse p2 = Warehouse.createIt("artcode", "Warehouse2", "user_id", user_id, "group_id", group.getLongId(), "title", "Some another");
        Long wid2 = p2.getLongId();

        controller("warehouse")
            .param("manage_type_code", "INCREASE")
            .param("amount", 50)
            .param("source", wid)
            .param("id", id)
        .post("updateStock");

        controller("warehouses").param("pid", id).get("management");
        List<Map<String, Object>> maps = (List<Map<String, Object>>) vals().get("items");
        a(maps.size()).shouldBeEqual(3);
        a(maps.get(0).get("stock")).shouldBeEqual(50);
    }

    public void addProducts() {
        Attribute a1 = Attribute.createIt("title", "Color", "group_id", group.getLongId());
        Attribute a2 = Attribute.createIt("title", "Width", "group_id", group.getLongId());
        AttributeValue av1 = AttributeValue.createIt("title", "Red", "attribute_id", a1.getLongId());
        AttributeValue av2 = AttributeValue.createIt("title", "White", "attribute_id", a1.getLongId());
        AttributeValue av3 = AttributeValue.createIt("title", "Green", "attribute_id", a1.getLongId());

        AttributeValue av4 = AttributeValue.createIt("title", "120", "attribute_id", a2.getLongId());
        AttributeValue av5 = AttributeValue.createIt("title", "140", "attribute_id", a2.getLongId());
        AttributeValue av6 = AttributeValue.createIt("title", "160", "attribute_id", a2.getLongId());

        Product p = Product.createIt("pricewithtax", 10, "priceperunit", 12, "type_code", "DEFAULT", "user_id", 1l, "group_id", group.getLongId(), "title", "Some title", "artcode", "888888");
        id = p.getLongId();
        Product p2 = Product.createIt("pricewithtax", 10, "priceperunit", 12, "type_code", "PARTIAL", "user_id", 1l, "group_id", group.getLongId(), "title", "Some another", "artcode", "12346");
        id2 = p2.getLongId();
        Product p3 = Product.createIt("pricewithtax", 10, "priceperunit", 12, "type_code", "DEFAULT", "user_id", 1l, "group_id", group.getLongId(), "title", "Somethings", "artcode", "12347");
        id3 = p3.getLongId();

        addCombo("2222222", id, av1.getString("id"), av4.getString("id"));
        addCombo("2222223", id, av2.getString("id"), av4.getString("id"));
        addCombo("2222224", id, av3.getString("id"), av4.getString("id"));
        addCombo("2222225", id3, av1.getString("id"), av5.getString("id"));
        addCombo("2222225", id3, av1.getString("id"), av6.getString("id"));
        addCombo("2222225", id3, av1.getString("id"), av4.getString("id"));
    }

    public void addCombo(String artcode_, Long id, String id1, String id2) {
        List<String> list = new ArrayList<String>(2);
        list.add(id1);
        list.add(id2);
        controller("products")
            .param("artcode", artcode_)
            .param("username", username)
            .param("pricewithtax", "10.0")
            .param("priceperunit", "12.0")
            .params("attribute_value_id[]", list)
            .param("product_id", id)
        .post("addCombos");
    }
}