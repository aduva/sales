package app.controllers;

import org.javalite.activeweb.DBControllerSpec;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import app.services.SecurityServiceMockModule;
import com.google.inject.Guice;
import org.javalite.activeweb.Cookie;
import app.services.Subject;

/**
 * @author Askhat Shakenov
 */
public class ProductsControllerSpec extends DBControllerSpec {

    Group group = null;
    String groupname = "ys";
    String username = "user1";
    String artcode = "12345";
    String title = "Product One Hello";
    Long id = null, id2 = null, id3 = null;
    Attribute a1, a2, a3;
    AttributeValue av1, av2, av3, av4, av5, av6;
    Cookie userCookie = new Cookie("last_login", username);

    @Before
    public void before() {
        ComboProducts.deleteAll();
        Attribute.deleteAll();
        AttributeValue.deleteAll();
        Composite.deleteAll();
        Product.deleteAll();
        Group.deleteAll();
        Role.deleteAll();
        GroupsUsers.deleteAll();
        User.deleteAll();
        group = Group.createIt("groupname", groupname, "title", "Yerke Sylkym");

        Product p = Product.createIt("pricewithtax", 10, "priceperunit", 12, "type_code", "DEFAULT", "user_id", 1l, "group_id", group.getLongId(), "title", title, "artcode", artcode);
        id = p.getLongId();
        Product p2 = Product.createIt("pricewithtax", 10, "priceperunit", 12, "type_code", "PARTIAL", "user_id", 1l, "group_id", group.getLongId(), "title", "Some another", "artcode", "12346");
        id2 = p2.getLongId();
        Product p3 = Product.createIt("pricewithtax", 10, "priceperunit", 12, "type_code", "DEFAULT", "user_id", 2l, "group_id", 2l, "title", "Somethings", "artcode", "12347");
        id3 = p3.getLongId();

        a1 = Attribute.createIt("title", "Color", "group_id", group.getLongId());
        a2 = Attribute.createIt("title", "Width", "group_id", group.getLongId());
        av1 = AttributeValue.createIt("title", "Red", "attribute_id", a1.getLongId());
        av2 = AttributeValue.createIt("title", "White", "attribute_id", a1.getLongId());
        av3 = AttributeValue.createIt("title", "Green", "attribute_id", a1.getLongId());

        av3 = AttributeValue.createIt("title", "120", "attribute_id", a2.getLongId());
        av4 = AttributeValue.createIt("title", "140", "attribute_id", a2.getLongId());
        av5 = AttributeValue.createIt("title", "160", "attribute_id", a2.getLongId());
        
        User u = User.newUser(username, "123456");
        u.saveIt();
        Role role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());

        GroupsUsers ur1 = new GroupsUsers();
        ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
        ur1.save();

        Subject subject = new Subject(u, true, false);
        subject.setCurrentGroup(group);
        session("subject", subject);
    }

    @Test
    public void shouldListAll() {
        request().cookie(userCookie).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);
    }

    @Test
    public void shouldListByPageSize() {
        request().cookie(userCookie).param("pageSize", 1).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldListByPageSizeAndPage() {
        request().cookie(userCookie).param("pageSize", 2).param("page", 2).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(0);
    }

    @Test
    public void shouldListBySearchQueue() {
        request().cookie(userCookie).param("q", title.substring(0,3)).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);

        request().cookie(userCookie).param("q", "some").get("index");
        items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);

        request().cookie(userCookie).param("q", "1234").get("index");
        items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);
    }

    @Test
    public void shouldShow() {
        request().cookie(userCookie).param("id", id).get("show");
        Product item = (Product) assigns().get("item");
        a(item.get("title")).shouldBeEqual(title);
        a(item.get("artcode")).shouldBeEqual(artcode);
    }

    @Test
    public void shouldCreate() {
        //create a fourth item
        request()
            .param("title", "New Product")
            .param("artcode", "222222")
            .param("username", username)
            .param("type_code", "DEFAULT")
            .cookie(userCookie)
        .post("save");
        

        a(redirected()).shouldBeTrue();
        a(Product.count()).shouldBeEqual(4);
        a(flash("message")).shouldNotBeNull();

        request().cookie(userCookie).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(3);
    }

    @Test
    public void shouldBeDefaultCombo() {
        addCombo("222222", av1.getString("id"), av4.getString("id"));
        
        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(3);
        a(ComboProducts.count()).shouldBeEqual(1);
        a(ComboProductsValues.count()).shouldBeEqual(2);
        a(Composite.count()).shouldBeEqual(1);

        Map item = (Map) assigns().get("item");
        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item.get("type_code")).shouldBeEqual("DEFAULT");
        a(item.get("id")).shouldBeEqual(id);
    }

    @Test
    public void shouldAddCombo() {
        addCombo("22222", av1.getString("id"), av4.getString("id"));
        
        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(3);
        a(ComboProductsValues.count()).shouldBeEqual(2);
        a(ComboProducts.count()).shouldBeEqual(1);
        a(Composite.count()).shouldBeEqual(1);

        Map item = (Map) assigns().get("item");
        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item.get("type_code")).shouldBeEqual("DEFAULT");
        a(item.get("id")).shouldBeEqual(id);

        addCombo("222322", av2.getString("id"), av5.getString("id"));
        
        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(4);
        a(ComboProductsValues.count()).shouldBeEqual(4);
        a(ComboProducts.count()).shouldBeEqual(2);
        a(Composite.count()).shouldBeEqual(1);

        item = (Map) assigns().get("item");
        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av2.getString("title") + ", " + a2.getString("title") + ": " + av5.getString("title"));
        a(item.get("type_code")).shouldBeEqual("COMBO");
        a(item.get("id")).shouldNotBeEqual(id);
    }

    @Test
    public void shouldEditCombo() {
        addCombo("222222", av1.getString("id"), av4.getString("id"));
        
        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(3);
        a(ComboProducts.count()).shouldBeEqual(1);
        a(ComboProductsValues.count()).shouldBeEqual(2);
        a(Composite.count()).shouldBeEqual(1);

        Map item = (Map) assigns().get("item");

        System.out.println("........> " + item);

        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item.get("type_code")).shouldBeEqual("DEFAULT");
        a(item.get("id")).shouldBeEqual(id);
        a(item.get("pricewithtax")).shouldBeEqual(10.0);
        a(item.get("priceperunit")).shouldBeEqual(12.0);

        // List<String> list = new ArrayList<String>(2);
        // list.add(av1.getString("id"));
        // list.add(id2);
        request()
            .param("artcode", "5555")
            .param("username", username)
            .param("pricewithtax", 15)
            .param("priceperunit", 22)
            // .params("attribute_value_id[]", list)
            .param("id", id)
            .param("is_default", false)
        .put("editCombo");

        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(3);
        a(ComboProducts.count()).shouldBeEqual(1);
        a(ComboProductsValues.count()).shouldBeEqual(2);
        a(Composite.count()).shouldBeEqual(1);

        item = (Map) assigns().get("item");
        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item.get("id")).shouldBeEqual(id);
        a(item.get("pricewithtax")).shouldBeEqual(15);
        a(item.get("priceperunit")).shouldBeEqual(22);
    }

    @Test
    public void shouldDeleteAttributeValue() {
        addCombo("222222", av1.getString("id"), av4.getString("id"));
        
        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(3);
        a(ComboProducts.count()).shouldBeEqual(1);
        a(ComboProductsValues.count()).shouldBeEqual(2);
        a(Composite.count()).shouldBeEqual(1);

        Map item = (Map) assigns().get("item");

        System.out.println("........> " + item);

        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item.get("type_code")).shouldBeEqual("DEFAULT");
        a(item.get("id")).shouldBeEqual(id);
        a(item.get("pricewithtax")).shouldBeEqual(10.0);
        a(item.get("priceperunit")).shouldBeEqual(12.0);

        request()
            .param("id", ComboProductsValues.findFirst("id > 0").getLongId())
        .delete("deleteAttributeValue");

        a(ComboProducts.count()).shouldBeEqual(1);
        a(ComboProductsValues.count()).shouldBeEqual(1);
        a(Composite.count()).shouldBeEqual(1);
    }

    @Test
    public void shouldChangeDefaultCombo() {
        addCombo("222222", av1.getString("id"), av4.getString("id"));
        
        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        a(Product.count()).shouldBeEqual(3);
        a(ComboProducts.count()).shouldBeEqual(1);
        a(ComboProductsValues.count()).shouldBeEqual(2);
        a(Composite.count()).shouldBeEqual(1);

        Map item = (Map) assigns().get("item");

        System.out.println("........> " + item);

        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item.get("is_default")).shouldBeTrue();
        a(item.get("id")).shouldBeEqual(id);
       
       addCombo("222322", av2.getString("id"), av5.getString("id"));
       
       a(val("item")).shouldNotBeNull();

       a(redirected()).shouldBeFalse();
       a(Product.count()).shouldBeEqual(4);
       a(ComboProductsValues.count()).shouldBeEqual(4);
       a(ComboProducts.count()).shouldBeEqual(2);
       a(Composite.count()).shouldBeEqual(1);

       item = (Map) assigns().get("item");
       a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av2.getString("title") + ", " + a2.getString("title") + ": " + av5.getString("title"));
       a(item.get("is_default")).shouldBeFalse();
       a(item.get("id")).shouldNotBeEqual(id);


        request()
            .param("id", item.get("id"))
            .param("is_default", true)
        .put("editCombo");

        a(val("item")).shouldNotBeNull();

        a(redirected()).shouldBeFalse();
        
        item = (Map) assigns().get("item");
        a(item.get("title")).shouldBeEqual(a1.getString("title") + ": " + av2.getString("title") + ", " + a2.getString("title") + ": " + av5.getString("title"));
        a(item.get("is_default")).shouldBeTrue();
        a(item.get("id")).shouldNotBeEqual(id);
    }

    @Test
    public void shouldListCombos() {
        addCombo("22222", av1.getString("id"), av4.getString("id"));
        addCombo("222322", av2.getString("id"), av5.getString("id"));

        request().param("id", id).get("combos");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);
        Map item0 = (Map) items.get(0);
        Map item1 = (Map) items.get(1);
        a(item0.get("title")).shouldBeEqual(a1.getString("title") + ": " + av1.getString("title") + ", " + a2.getString("title") + ": " + av4.getString("title"));
        a(item0.get("artcode")).shouldBeEqual(artcode);
        a(item1.get("title")).shouldBeEqual(a1.getString("title") + ": " + av2.getString("title") + ", " + a2.getString("title") + ": " + av5.getString("title"));
        a(item1.get("artcode")).shouldBeEqual("222322");
        a(item0.get("is_default")).shouldBeTrue();
        a(item1.get("is_default")).shouldBeFalse();
    }

    @Test
    public void shouldAddSet() {
        addSet(id, id2, 4);

        a(Composite.count()).shouldBeEqual(1);
        a(SetProducts.count()).shouldBeEqual(1);
    }

    @Test
    public void shouldListSets() {
        addSet(id, id2, 4);
        addSet(id, id3, 6);

        request().param("id", id).get("sets");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);

        Map item0 = (Map) items.get(0);
        Map item1 = (Map) items.get(1);
       
        a(item0.get("amount")).shouldBeEqual(4);
        a(item1.get("amount")).shouldBeEqual(6);
    }

    public void addSet(Long id, Long id2, double amount) {
        request()
            .param("id", id)
            .param("product_id", id2)
            .param("amount", amount)
        .post("addSet");
    } 

    public void addCombo(String artcode_, String id1, String id2) {
        //create a fourth item
        List<String> list = new ArrayList<String>(2);
        list.add(id1);
        list.add(id2);
        request()
            .param("artcode", artcode_)
            .param("username", username)
            .param("pricewithtax", "10.0")
            .param("priceperunit", "12.0")
            .params("attribute_value_id[]", list)
            .param("product_id", id)
        .post("addCombos");
    }

    // @Test
    // public void shouldUpdate() {
    //     //create a fourth item
    //     String newtitle = "Group One Updated";
    //     request().param("title", newtitle).param("groupname", "group1").put("update");
    //     
    //     a(redirected()).shouldBeTrue();
        
    //     a(flash("message")).shouldNotBeNull();

    //     request().integrateViews().param("groupname",  "group1").get("show");

    //     Group item = (Group) assigns().get("item");
    //     a(item.get("title")).shouldBeEqual(newtitle);
    // }

    // @Test
    // public void shouldDelete() {
    //     Group u = (Group) Group.findAll().get(0);

    //     request().param("groupname",  u.get("groupname")).delete("delete");
        
    //     a(redirected()).shouldBeTrue();
    //     a(Group.count()).shouldBeEqual(2);
    //     a(flash("message")).shouldNotBeNull();
    // }

    // @Test
    // public void shouldShowHTML() {
    //     Group i = (Group) Group.findAll().get(0);

    //     request().integrateViews().param("groupname",  i.get("groupname")).get("show");

    //     Group item = (Group) assigns().get("item");
    //     a(item.get("groupname")).shouldBeEqual(i.get("groupname"));
    //     String html = responseContent();
    //     a(html.contains(i.getString("title"))).shouldBeTrue();
    // }
}
