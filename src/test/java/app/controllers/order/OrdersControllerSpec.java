package app.controllers;

import org.javalite.activeweb.DBControllerSpec;
import org.javalite.activeweb.Cookie;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.joda.time.DateTime;
import java.sql.Timestamp;
import app.services.SecurityServiceMockModule;
import app.services.Subject;
import com.google.inject.Guice;

/**
 * @author Askhat Shakenov
 */
public class OrdersControllerSpec extends DBControllerSpec {
    Group group = null;
    String groupname = "group1";
    String username = "user1";
    Long user_id = null;
    Long id = null, id2 = null;
    Cookie userCookie = new Cookie("last_login", username);

    @Before
    public void before() {
        Product.deleteAll();
        Order.deleteAll();
        Group.deleteAll();
        Role.deleteAll();
        User.deleteAll();
        GroupsUsers.deleteAll();
        group = Group.createIt("groupname", groupname, "title", "fake group");
        
        User u = (User) User.newUser(username, "123456");
        u.saveIt();
        user_id = u.getLongId();

        Order p = Order.createIt("user_id", user_id, "group_id", group.getLongId());
        id = p.getLongId();

        Role role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());

        GroupsUsers ur1 = new GroupsUsers();
        ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
        ur1.save();


        Subject subject = new Subject(u, true, false);
        subject.setCurrentGroup(group);
        session("subject", subject);
    }

    @Test
    public void shouldListAll() {
        request().cookie(userCookie).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldListByPageSize() {
        request().cookie(userCookie).param("pageSize", 1).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldListByPageSizeAndPage() {
        request().cookie(userCookie).param("pageSize", 2).param("page", 2).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(0);
    }

    @Test
    public void shouldShow() {
        request().cookie(userCookie).param("id", id).get("show");
        Order item = (Order) assigns().get("item");
        a(item.get("user_id")).shouldBeEqual(user_id);
    }

    @Test
    public void shouldCreate() {
        //create a fourth item
        request()
            // .param("user_id", "askhat")
            .cookie(userCookie)
        .post("save");
        
        a(redirected()).shouldBeTrue();
        a(Order.count()).shouldBeEqual(2);
        a(flash("message")).shouldNotBeNull();
        Order item = (Order)flash("item");
        a(item.get("due_date")).shouldBeEqual(new Timestamp(new DateTime().withTimeAtStartOfDay().plusDays(1).minusSeconds(1).toDate().getTime()));
    }

    @Test
    public void shouldChangeDueDate() {
        request().param("id", id).put("changeDueDate");
        a(redirected()).shouldBeTrue();
        request().cookie(userCookie).param("id", id).get("show");
        Order item = (Order) assigns().get("item");
        a(item.get("due_date")).shouldBeEqual(new Timestamp(new DateTime().withTimeAtStartOfDay().plusDays(2).minusSeconds(1).toDate().getTime()));   
    }

    @Test
    public void shouldAddProduct() {
        Product p = Product.createIt(
            "group_id", group.getLongId(),
            "user_id", 1l,
            "type_code", "DEFAULT",
            "title", "Product title",
            "artcode", "22222",
            "pricewithtax", 5
        );
        
        Warehouse w = Warehouse.createIt(
            "group_id", group.getLongId(),
            "user_id", 1l,
            "title", "Warehouse title"
        );
        
        WarehousesProducts wp = WarehousesProducts.createIt(
            "warehouse_id", w.getLongId(),
            "product_id", p.getLongId(),
            "stock", 100
        );

        request().param("id", id).param("product_id", p.getLongId()).param("amount", 100).post("addProduct");
        a(OrdersProducts.count()).shouldBeEqual(1);
        OrdersProducts op = (OrdersProducts) flash("item");
        a(op.getLong("order_id")).shouldBeEqual(id);
        a(op.get("pricewithtax")).shouldBeEqual(p.get("pricewithtax"));
        a(op.get("sum")).shouldBeEqual(500);
    }

    // @Test
    // public void shouldDelete() {
    //     Group u = (Group) Group.findAll().get(0);

    //     request().param("groupname",  u.get("groupname")).delete("delete");
        
    //     a(redirected()).shouldBeTrue();
    //     a(Group.count()).shouldBeEqual(2);
    //     a(flash("message")).shouldNotBeNull();
    // }

    // @Test
    // public void shouldShowHTML() {
    //     Group i = (Group) Group.findAll().get(0);

    //     request().integrateViews().param("groupname",  i.get("groupname")).get("show");

    //     Group item = (Group) assigns().get("item");
    //     a(item.get("groupname")).shouldBeEqual(i.get("groupname"));
    //     String html = responseContent();
    //     a(html.contains(i.getString("title"))).shouldBeTrue();
    // }
}
