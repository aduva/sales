package app.controllers;

import org.javalite.activeweb.DBControllerSpec;
import org.javalite.activeweb.Cookie;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;
import app.services.Subject;

import java.util.List;
import java.util.Map;

/**
 * @author Askhat Shakenov
 */
public class GroupsControllerSpec extends DBControllerSpec {

    String groupname = "group1";
    String title = "Group One Hello";
    String username = "askhat";
    Cookie userCookie = new Cookie("last_login", username);

    @Before
    public void before() {
        Group.deleteAll();
        Group group = (Group)Group.createIt("groupname", groupname, "title", title);
        Group.createIt("groupname", "group2", "title", "Some another");
        Group.createIt("groupname", "group3", "title", "Somethings");

        User u = User.newUser(username, "123456");
        u.saveIt();
        Role role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());

        GroupsUsers ur1 = new GroupsUsers();
        ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
        ur1.save();

        Subject subject = new Subject(u, true, false);
        subject.setCurrentGroup(group);
        session("subject", subject);
    }

    @Test
    public void shouldListAll() {
        request().cookie(userCookie).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(3);
    }

    @Test
    public void shouldListByPageSize() {
        request().cookie(userCookie).param("pageSize", 2).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);
    }

    @Test
    public void shouldListByPageSizeAndPage() {
        request().cookie(userCookie).param("pageSize", 2).param("page", 2).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldListBySearchQueue() {
        request().cookie(userCookie).param("q", groupname.substring(0,3)).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(3);

        request().cookie(userCookie).param("q", "some").get("index"); //<< this is where we execute the controller
        items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);

        request().cookie(userCookie).param("q", "hello").get("index"); //<< this is where we execute the controller
        items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldShow() {
        request().cookie(userCookie).param("groupname", groupname).get("show"); //<< this is where we execute the controller and pass a parameter
        Group item = (Group) assigns().get("item");
        a(item.get("groupname")).shouldBeEqual(groupname);
    }

    @Test
    public void shouldCreate() {
        //create a fourth item
        request().cookie(userCookie).param("title", "New Group").param("groupname", "newgroup").post("save");
        //get list of all items

        a(redirected()).shouldBeTrue();
        a(Group.count()).shouldBeEqual(4);
        a(flash("message")).shouldNotBeNull();

        request().cookie(userCookie).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(4);
    }

    @Test
    public void shouldUpdate() {
        //create a fourth item
        String newtitle = "Group One Updated";
        request().cookie(userCookie).param("title", newtitle).param("groupname", "group1").put("update");
        //get list of all items
        a(redirected()).shouldBeTrue();
        
        a(flash("message")).shouldNotBeNull();

        request().cookie(userCookie).integrateViews().param("groupname",  "group1").get("show");

        Group item = (Group) assigns().get("item");
        a(item.get("title")).shouldBeEqual(newtitle);
    }

    @Test
    public void shouldDelete() {
        Group u = (Group) Group.findAll().get(0);

        request().cookie(userCookie).param("groupname",  u.get("groupname")).delete("delete");
        
        a(redirected()).shouldBeTrue();
        a(Group.count()).shouldBeEqual(2);
        a(flash("message")).shouldNotBeNull();
    }

    @Test
    public void shouldShowHTML() {
        Group i = (Group) Group.findAll().get(0);

        request().cookie(userCookie).integrateViews().param("groupname",  i.get("groupname")).get("show");

        Group item = (Group) assigns().get("item");
        a(item.get("groupname")).shouldBeEqual(i.get("groupname"));
        String html = responseContent();
        a(html.contains(i.getString("title"))).shouldBeTrue();
    }
}
