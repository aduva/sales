package app.controllers;

import org.javalite.activeweb.DBControllerSpec;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Guice;
import java.util.List;
import java.util.Map;
import app.services.SecurityServiceMockModule;

/**
 * @author Askhat Shakenov
 */
public class AuthenticationControllerSpec extends DBControllerSpec {

    String username = "askhat";
    String password = "123456";
    User u = null;
    Role role1 = null, role5 = null;

    @Before
    public void before() {
        User.deleteAll();
        Group.deleteAll();
        Role.deleteAll();
        GroupsUsers.deleteAll();
        u = (User)User.newUser(username, password);
        u.saveIt();

        setInjector(Guice.createInjector(new SecurityServiceMockModule()));

        Group group = Group.createIt("groupname", "ys", "title", "Yerke Sylkym");
        role1 = Role.createIt("title", "Admin", "level", 1, "group_id", group.getLongId());
        Role role2 = Role.createIt("title", "Account", "level", 10, "group_id", group.getLongId());
        Role role3 = Role.createIt("title", "Cashier", "level", 20, "group_id", group.getLongId());
        Role role4 = Role.createIt("title", "Stockman", "level", 20, "group_id", group.getLongId());
        role5 = Role.createIt("title", "Salesman", "level", 20, "group_id", group.getLongId());

        GroupsUsers ur1 = new GroupsUsers();
        ur1.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role1.getLongId());
        ur1.save();
        GroupsUsers ur2 = new GroupsUsers();
        ur2.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role2.getLongId());
        ur2.save();
        GroupsUsers ur3 = new GroupsUsers();
        ur3.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role3.getLongId());
        ur3.save();
        GroupsUsers ur4 = new GroupsUsers();
        ur4.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role4.getLongId());
        ur4.save();
        // GroupsUsers ur5 = new GroupsUsers();
        // ur5.set("group_id", group.getLongId(), "user_id", u.getLongId(), "role_id", role5.getLongId());
        // ur5.save();
    }

    @Test
    public void shouldShowLoginGroupRoles() {
        request().param("g", "ys").get("login");
        List roles = (List) assigns().get("roles");
        a(roles).shouldNotBeNull();
        a(roles.size()).shouldBeEqual(5);
    }

    @Test
    public void shouldShowLoginRoleUsers() {
        request().param("g", "ys").param("rid", role1.getLongId()).get("login");
        List users = (List) assigns().get("users");
        a(users).shouldNotBeNull();
        a(users.size()).shouldBeEqual(1);

        request().param("g", "ys").param("rid", role5.getLongId()).get("login");
        users = (List) assigns().get("users");
        a(users).shouldNotBeNull();
        a(users.size()).shouldBeEqual(0);
    }
        

    @Test
    public void shouldNotLoginByUsername() {
        request().param("password", password).param("username", "user1").post("doLogin");

        a(redirected()).shouldBeTrue();
        a(flash("message")).shouldNotBeNull();
        a(flash("message")).shouldBeEqual("auth.username.error");
    }

    @Test
    public void shouldNotLoginByPassword() {
        request().param("password", "12345").param("username", username).post("doLogin");

        a(redirected()).shouldBeTrue();
        a(flash("message")).shouldNotBeNull();
        a(flash("message")).shouldBeEqual("auth.password.error");
    }

    @Test
    public void shouldLogin() {
        request().param("password", password).param("username", username).post("doLogin");

        a(redirected()).shouldBeTrue();
        a(flash("message")).shouldNotBeNull();
        a(flash("message")).shouldBeEqual("auth.login.success");
    }
}
