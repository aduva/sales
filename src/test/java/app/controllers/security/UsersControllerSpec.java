package app.controllers;

import org.javalite.activeweb.DBControllerSpec;
import org.javalite.activeweb.Cookie;
import app.models.*;
import org.javalite.test.XPathHelper;
import org.junit.Before;
import org.junit.Test;
import app.services.Subject;

import java.util.List;
import java.util.Map;

/**
 * @author Askhat Shakenov
 */
public class UsersControllerSpec extends DBControllerSpec {

    String groupname = "yerke";
    String username = "askhat";
    String password = "123456";
    Role role1 = null;
    Cookie userCookie = new Cookie("last_login", username);

    @Before
    public void before() {
  
        User.deleteAll();
        User u = (User)User.newUser(username, password);
        u.save();
        User u2 = User.newUser("azoda", "123456");
        u2.save();
        User u3 = User.newUser("roza", "123456");
        u3.save();

        Group g = Group.createIt("groupname", groupname, "title", "Yerke");
        role1 = Role.createIt("title", "Admin", "level", 1, "group_id", g.getLongId());
        Role role2 = Role.createIt("title", "Account", "level", 10, "group_id", g.getLongId());
        Role role3 = Role.createIt("title", "Cashier", "level", 20, "group_id", g.getLongId());
        GroupsUsers gu = GroupsUsers.createIt("user_id", u.getLongId(), "group_id", g.getLongId(), "role_id", role1.getLongId());
        GroupsUsers gu2 = GroupsUsers.createIt("user_id", u2.getLongId(), "group_id", g.getLongId(), "role_id", role2.getLongId());
        GroupsUsers gu3 = GroupsUsers.createIt("user_id", u3.getLongId(), "group_id", g.getLongId(), "role_id", role3.getLongId());

        Subject subject = new Subject(u, true, false);
        subject.setCurrentGroup(g);
        session("subject", subject);
    }

    @Test
    public void shouldListAll() {
        a(User.count()).shouldBeEqual(3);

        request().cookie(userCookie).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(3);
    }

    @Test
    public void shouldListByPageSize() {
        request().cookie(userCookie).param("pageSize", 2).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(2);
    }

    @Test
    public void shouldListByPageSizeAndPage() {
        request().cookie(userCookie).param("pageSize", 2).param("page", 2).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldListBySearchQueue() {
        request().cookie(userCookie).param("q", username.substring(0,3)).get("index"); //<< this is where we execute the controller
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(1);
    }

    @Test
    public void shouldShow() {
        request().cookie(userCookie).param("username", username).get("show"); //<< this is where we execute the controller and pass a parameter
        User item = (User) assigns().get("item");
        a(item.get("username")).shouldBeEqual(username);
    }

    @Test
    public void shouldCreate() {
        //create a fourth item
        request().cookie(userCookie).param("password", "12345").param("role_id",role1.getLongId()).param("username", "user1").post("save");
        //get list of all items

        a(redirected()).shouldBeTrue();
        a(User.count()).shouldBeEqual(4);
        a(flash("message")).shouldNotBeNull();

        request().cookie(userCookie).get("index");
        List items = (List) assigns().get("items");
        a(items.size()).shouldBeEqual(4);
    }

    @Test
    public void shouldUpdate() {
        //create a fourth item
        String newpassword = "222222";
        request().cookie(userCookie).param("password", newpassword).param("username", "askhat").put("update");
        //get list of all items
        a(redirected()).shouldBeTrue();
        
        a(flash("message")).shouldNotBeNull();

        request().integrateViews().cookie(userCookie).param("username",  "askhat").get("show");

        User item = (User) assigns().get("item");
        a(item.get("password")).shouldBeEqual(newpassword);
    }

    @Test
    public void shouldDelete() {
        User u = (User) User.findAll().get(0);

        request().cookie(userCookie).param("username",  u.get("username")).delete("delete");
        
        a(redirected()).shouldBeTrue();
        a(User.count()).shouldBeEqual(2);
        a(flash("message")).shouldNotBeNull();
    }

    @Test
    public void shouldShowHTML() {
        User u = (User) User.findAll().get(0);

        request().integrateViews().cookie(userCookie).param("username",  u.get("username")).get("show");

        User item = (User) assigns().get("item");
        a(item.get("username")).shouldBeEqual(u.get("username"));
        String html = responseContent();
        a(html.contains(u.getString("username"))).shouldBeTrue();
    }
}
