package app.models;

import org.javalite.activeweb.DBSpec;
import org.junit.Test;

import java.util.List;

/**
 * @author Askhat Shakenov
 */
public class ProductSpec extends DBSpec {

    @Test
    public void shouldValidate(){
        Product item = new Product();
        a(item).shouldNotBe("valid");

        item.set("group_id", 1l, "title", "Fake Product", "user_id", 1l);
        a(item).shouldBe("valid");
    }

}