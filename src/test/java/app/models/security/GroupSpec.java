package app.models;

import org.javalite.activeweb.DBSpec;
import org.junit.Test;

import java.util.List;

/**
 * @author Askhat Shakenov
 */
public class GroupSpec extends DBSpec {

    @Test
    public void shouldValidate(){
        Group item = new Group();
        a(item).shouldNotBe("valid");

        item.set("groupname", "fakegroup", "title", "Group is Fakegroup");
        a(item).shouldBe("valid");
    }

}