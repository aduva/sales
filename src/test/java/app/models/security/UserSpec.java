package app.models;

import org.javalite.activeweb.DBSpec;
import org.junit.Test;

import java.util.List;

/**
 * @author Askhat Shakenov
 */
public class UserSpec extends DBSpec {

    @Test
    public void shouldValidate(){
        User user = User.newUser("fakeuser", "123456");
        a(user).shouldBe("valid");
    }

}

