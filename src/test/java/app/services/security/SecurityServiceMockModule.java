package app.services;

import com.google.inject.AbstractModule;

public class SecurityServiceMockModule extends AbstractModule {
    protected void configure() {
        bind(SecurityService.class).to(SecurityServiceMock.class).asEagerSingleton();
    }
}