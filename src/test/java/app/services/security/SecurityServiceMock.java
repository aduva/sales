package app.services;

import org.javalite.activeweb.SessionFacade;
import app.models.User;
import app.utils.*;
import java.io.Serializable;

public class SecurityServiceMock implements SecurityService {    

    private static final String SESSION_KEY_SUBJECT = "subject";
    private static final String SESSION_KEY_GROUP = "user-current-group";
    private static final String SESSION_KEY_GROUPS = "user-groups";
    // private static String SESSION_KEY_LOGIN_TIME = "user-logged-in";

    public Subject subject() {
        return (Subject) session(SESSION_KEY_SUBJECT);
    }

    public Long userId() {
        return 1l;
    }

    public void login(String username, String password, boolean rememberMe) throws UsernameNotFoundException, PasswordNotMatchException {
        User user = (User) User.findFirst("username = ?", username);
        if(user == null)
            throw new UsernameNotFoundException();

        BCryptPasswordCryptor matcher = new BCryptPasswordCryptor();
        if(!matcher.match(password, user.getString("password")))
            throw new PasswordNotMatchException();

        session(SESSION_KEY_SUBJECT, new Subject(user, true, rememberMe));
    }

    public void logout() {
        subject().isAuthenticated(false);
        if(!subject().rememberMe())
            session().remove(SESSION_KEY_SUBJECT);
    }

    private SessionFacade session(){
        return new SessionFacade();
    }
    
    private void session(String name, Serializable value){
        session().put(name, value);
    }

    private Object session(String name){
        Object val = session().get(name);
        return val == null ? null : val;
    }

}